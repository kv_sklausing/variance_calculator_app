import functools
import time
import psutil
from queue import Queue
import threading
import multiprocessing as mp
import trimesh
import os
import traceback

def memProfile(func):
    """Print the percentage of CPU and RAM used by the decorated function"""

    @functools.wraps(func)
    def wrapper_profiler(*args, **kwargs):
        print(f"CPU: {psutil.cpu_percent()}%")
        print(f"Virtual Memory: {psutil.virtual_memory().percent}%")
        # memDict = {'CPU': psutil.cpu_percent(), 'VirtualMemory': psutil.virtual_memory()}
        value = func(*args, **kwargs)
        return value

    return wrapper_profiler


def timer(func):
    """Print the runtime of the decorated function"""

    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()  # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()  # 2
        run_time = end_time - start_time  # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value

    return wrapper_timer


def timer_and_pause(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()  # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()  # 2
        run_time = end_time - start_time  # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value

    return wrapper_timer


def get_cpu_info():
    numCPUs = mp.cpu_count()
    cpuPool = mp.Pool(processes=None)
    return


def multi_thread_action(inputDict, targetFun, maxThreads=4):
    """
        Create multiple threads based on inputDictionary
        """
    if len(inputDict) > maxThreads:
        return

    q = Queue()
    threads = list()
    results = dict()
    for inputKey, inputArg in inputDict.items():
        # if inputKey != 'Test Mesh File' or inputKey != 'Ref Mesh File':
        #     continue
        funArgsList = [inputArg, inputKey, q]
        t = threading.Thread(target=targetFun, args=funArgsList)
        threads.append(t)
        t.start()
    for t in threads:
        ret = q.get()  # will block until result is ready
        results[ret[0]] = ret[1]
    for t in threads:
        t.join()
    return results


def read_mesh_file(readMesh, info, queue, validate=False):
    try:
        # print("Reading in file: %s" % os.path.basename(readMesh))
        sampleInfo = {'file': info}
        meshLoaded = trimesh.load_mesh(readMesh)
        meshLoaded.metadata.update(sampleInfo)

        # Preprocess trimesh objects by:
        # 1. removes NaN and Inf values
        # 2. merges duplicate vertices
        # 3. removes triangles which have one edge of their rectangular 2D oriented bounding box shorter than tol.merge
        # 4. removes duplicated triangles
        # 5. ensures triangles are consistently wound and normals face outwards
        meshLoaded.process(validate=validate)
    except:
        errorTraceback = traceback.format_exc()
        print('\nError encountered during read mesh routine.')
        queue.put((info, errorTraceback))
        return
    else:
        queue.put((info, meshLoaded))
        return



def start_mesh_load_thread(meshPairDict):
    # Read ref and test mesh file as trimesh object in multiple threads
    multiThreadResults = multi_thread_action(meshPairDict, read_mesh_file)

    try:
        testTriMesh = multiThreadResults['Test Mesh File']
    except KeyError:
        testTriMesh = None

    try:
        refTriMesh = multiThreadResults['Ref Mesh File']
    except KeyError:
        refTriMesh = None

    return testTriMesh, refTriMesh

