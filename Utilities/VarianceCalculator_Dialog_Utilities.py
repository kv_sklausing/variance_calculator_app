
from PySide2.QtWidgets import QMessageBox
from PySide2.QtGui import QIcon, QPixmap
from UserInterface.VarianceCalculator_Widgets import *

def alert_user_dialog(message, parent=None, iconPath=None, alertIcon=None):
    print('\nWarning user via message box...')
    qMessageBox = QMessageBox(parent)
    qMessageBox.setWindowTitle('Warning...')
    if iconPath is not None:
        qMessageBox.setWindowIcon(QIcon(iconPath))
        # qMessageBox.setIcon(QIcon(iconPath))
        qMessageBox.setIconPixmap(QPixmap(iconPath))
    if alertIcon is not None:
        qMessageBox.setIconPixmap(QPixmap(alertIcon))
    qMessageBox.setText(message)
    if qMessageBox.exec_() == qMessageBox.Ok:
        return True


def notify_user_dialog(message, parent=None, iconPath=None, notifyIcon=None):
    print('\nAlerting user via message box...')
    qMessageBox = QMessageBox(parent)
    qMessageBox.setWindowTitle('Info...')
    if iconPath is not None:
        qMessageBox.setWindowIcon(QIcon(iconPath))
        # qMessageBox.setIcon(QIcon(iconPath))
        qMessageBox.setIconPixmap(QPixmap(iconPath))
    if notifyIcon is not None:
        qMessageBox.setIconPixmap(QPixmap(notifyIcon))
    qMessageBox.setText(message)
    if qMessageBox.exec_() == qMessageBox.Ok:
        return True


def confirm_user_dialog(message, parent=None, iconPath=None, notifyIcon=None):
    print('\nConfirming user via message box...')
    qMessageBox = QMessageBox(parent)
    qMessageBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
    qMessageBox.setWindowTitle('Confirm...')
    if iconPath is not None:
        qMessageBox.setWindowIcon(QIcon(iconPath))
        # qMessageBox.setIcon(QIcon(iconPath))
        qMessageBox.setIconPixmap(QPixmap(iconPath))
    if notifyIcon is not None:
        qMessageBox.setIconPixmap(QPixmap(notifyIcon))
    qMessageBox.setText(message)
    if qMessageBox.exec_() == qMessageBox.Yes:
        return True
    else:
        return False
