'''
Collection of Utilities used for gui development.
'''


import os
import pickle



class WindowSettings():
    # Create empty class for storing window settings.
    def __init__(self, name):
        self.name = name


def load_settings_file(fileFullPathSettings):
    print('\nLoading settings file...')
    # Check to see if it exists.
    print(fileFullPathSettings)
    if not os.path.exists(fileFullPathSettings):
        print('Settings file does not exist.')
        classSettings = None
        return classSettings
    # Load in the settings file.
    with open(fileFullPathSettings, 'rb') as fileObject:
        classSettings = pickle.load(fileObject)
    print('Settings file loaded.')
    return classSettings


def save_settings_file(fileFullPathSettings, classSettings):
    print('\nSaving settings file...')
    # Save settings to file.
    print(fileFullPathSettings)
    with open(fileFullPathSettings, 'wb') as fileObject:
        pickle.dump(classSettings, fileObject)
    print('Settings file saved.')
    return


def get_settings_filepath(fileFullPathScript):
    # Create the path for the settings file.
    fileNameScript = os.path.basename(fileFullPathScript)
    fileBaseNameScript = os.path.splitext(fileNameScript)[0]
    fileNameSettings = '%s.config' % (fileBaseNameScript)
    fileBasePathScript = os.path.dirname(fileFullPathScript)
    fileFullPathSettings = os.path.join(fileBasePathScript, fileNameSettings)
    return fileFullPathSettings


