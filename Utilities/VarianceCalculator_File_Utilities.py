import os
import traceback
import pandas
from PySide2.QtWidgets import QFileDialog
from PySide2.QtGui import QIcon
from UserInterface.VarianceCalculator_Widgets import *


def open_directory(directoryPath):
    print('\nOpening directory...')
    if os.path.isfile(directoryPath):
        directoryPath = os.path.dirname(directoryPath)
    else:
        pass
    try:
        # raise NameError('TEST')
        os.startfile(directoryPath)
    except:
        print('Could not open project directory:\n%s' % directoryPath)
        currentException = traceback.format_exc()
        print('Unexpected error:\n%s' % currentException)
    else:
        print('Opened project directory:\n%s' % directoryPath)


def select_directory_dialog(directoryPath, iconPath=None):
    print('\nInteractively selecting directory...')
    try:
        # raise NameError('TEST')
        qFileDialog = QFileDialog()
        qFileDialog.setWindowTitle('Select existing directory...')
        if not iconPath == None:
            qFileDialog.setWindowIcon(QIcon(iconPath))
        qFileDialog.setViewMode(QFileDialog.Detail)
        qFileDialog.setFileMode(QFileDialog.Directory)
        if os.path.isfile(directoryPath):
            qFileDialog.setDirectory(os.path.dirname(directoryPath))
        if os.path.isdir(directoryPath):
            qFileDialog.setDirectory(directoryPath)
        if qFileDialog.exec_():  # == QFileDialog.Accepted:
            # Update project directory.
            directoryPath = qFileDialog.selectedFiles()[0]
            print('Directory selected:\n%s' % (directoryPath))
            return os.path.abspath(directoryPath)
        else:
            print('Directory selection cancelled.')
            return None
    except:
        print('Issue selecting directory.')
        currentException = traceback.format_exc()
        print('Unexpected error:\n%s' % (currentException))
    return


def select_file_dialog(filePathStart, iconPath=None, meshOnly=True):
    print('\nInteractively selecting file...')
    print(filePathStart)
    try:
        # raise NameError('TEST')
        qFileDialog = QFileDialog()
        qFileDialog.setWindowTitle('Select existing file...')
        if not iconPath == None:
            qFileDialog.setWindowIcon(QIcon(iconPath))
        qFileDialog.setViewMode(QFileDialog.Detail)
        qFileDialog.setFileMode(QFileDialog.ExistingFile)
        if os.path.isfile(filePathStart):
            qFileDialog.setDirectory(os.path.dirname(filePathStart))
        if os.path.isdir(filePathStart):
            qFileDialog.setDirectory(filePathStart)
        if meshOnly:
            qFileDialog.setNameFilters(['Test Files (*.ply *.stl *.obj)'])
        else:
            qFileDialog.setNameFilters(['Ref Files (*.stp *.step *.igs \n'
                                        '*.ply *.stl *.obj \n'
                                        '*.txt *.csv *.asc *.var *.dev)'])
        if qFileDialog.exec_():  # == QFileDialog.Accepted:
            # Update project file.
            filePath = qFileDialog.selectedFiles()[0]
            print('File selected:\n%s' % (filePath))
            return os.path.abspath(filePath)
        else:
            filePath = filePathStart
            print('File selection cancelled.')
            return None
    except:
        print('Issue selecting file.')
        currentException = traceback.format_exc()
        print('Unexpected error:\n%s' % (currentException))
    return


def get_files(directory, dataTypeList, contains=None, location='any'): #location = 'any' or 'end'
    """Summary of function.

    Returns a list of files found based on the the search directory, type of file given and a specific end string (if any).

    Args:
        directory: search directory where files should be found
        dataTypeList: type = list, file extensions for valid files desired
        contains: type = string, a string that is in the files you want, identifier
        locatoin: type = string, 'any' used for the contain string to be anywhere in the file, 'end' to have the file specifically end with the string

    Returns:
        A list of files found

    Raises:
        No raises, returns None if no files found based on input arguments

    Example Usage:
        get_files(input_directory, ['.stl'], endsWithStr='Baseline')
    """

    print("\nSearching for %s files in directory:\n%s" % (str(dataTypeList), directory))

    # Filter for finding valid file
    def data_filter(file):
        fileNameNoExt, fileExt = os.path.splitext(file)
        if fileExt in dataTypeList or fileExt.split('.')[-1] in dataTypeList:
            if contains != None:
                if location == 'end':
                    goodFile = fileNameNoExt.endswith(contains)
                elif location == 'any':
                    if contains in fileNameNoExt:
                        goodFile = True
                    else:
                        goodFile = False
            else:
                goodFile = True
        else:
            goodFile = False
        return goodFile

    # Create list of all files in directory
    if (directory is None) or not os.path.isdir(directory):  # check input
        print('Directory does not exist.\n%s' % (directory))
        return None
    else:
        all_files = os.listdir(directory)  # get all files in directory
        data_files = list(filter(data_filter, all_files))  # filter out everything that isnt a valid data file
        data_files = [os.path.join(directory, validData) for validData in data_files]  # create full file path

        if len(data_files) == 0:  # error if no valid data types
            print('No valid files %s in directory.\n%s' % (str(dataTypeList), directory))
            return None

    print("Found %i %s files in directory." % (len(data_files), str(dataTypeList)))

    return data_files


def valid_directory(directoryPath):
    """Summary of function.

    Function that checks the input for validity

    Args:
        directoryPath: Path to a directory

    Returns:
        Path to a directory if the input is a valid drectory

    Raises:
        Returns empty and prints error message if input is invalid

    Example Usage:
        (optional, but recommended; write an example line calling the function)
    """

    if not os.path.isdir(directoryPath):
        print("Input directory is not a valid directory!")
        return
    else:
        directoryPath = directoryPath.strip('\"') # remove leading/trailing quotes around string
        if os.path.isdir(directoryPath):
            directoryPath = os.path.abspath(directoryPath)
    return (directoryPath)


def valid_dirs_in_directory(directory):
    """Summary of function.

    Function that finds valid folders within a given directory

    Args:
        directory: Path to a directory

    Returns:
        list of file paths to a valid directories found

    Raises:
        Returns empty if input direcotry is None

    Example Usage:
        (optional, but recommended; write an example line calling the function)
    """
    dirs_in_directory = []
    if directory == None:
        return directory
    else:
        directoryitems = os.listdir(directory)
        for item in directoryitems:
            tempdir = os.path.join(directory, item)
            if os.path.isdir(tempdir):
                dirs_in_directory.append(tempdir)
            else:
                pass
    return (dirs_in_directory)


def valid_file(filePath):
    """Summary of function.

    Function that checks the input file for validity

    Args:
        filePath: Path to a file

    Returns:
        filepath if exists and is file.

    Raises:
        Returns empty and prints error message if input is not a file

    Example Usage:
        (optional, but recommended; write an example line calling the function)
    """

    if not os.path.isfile(filePath):
        print("Input file does not exist!")
        return
    else:
        filePath = filePath.strip('\"') # remove leading/trailing quotes around string
        if os.path.isfile(filePath):
            filePath = os.path.abspath(filePath)
    return (filePath)


def read_asc_pt_file(geoPointFile):
    sampledPoints = pandas.read_csv(geoPointFile, delim_whitespace=True, comment='#',
                                    names=['X', 'Y', 'Z', 'X Normal', 'Y Normal', 'Z Normal'],
                                    usecols=[0, 1, 2, 3, 4, 5])
    sampledPointsFinal = sampledPoints.filter(['X', 'Y', 'Z']).values
    sampledPointsFinalNorms = sampledPoints.filter(['X Normal', 'Y Normal', 'Z Normal']).values
    return sampledPointsFinal, sampledPointsFinalNorms


def read_txt_pt_file(geoPointFile):
    sampledPoints = pandas.read_csv(geoPointFile,
                                    names=['X', 'Y', 'Z', 'X Normal', 'Y Normal', 'Z Normal'],
                                    usecols=[0, 1, 2, 4, 5, 6])  # skips deviation column (keeps normals)
    sampledPointsFinal = sampledPoints.filter(['X', 'Y', 'Z']).values
    sampledPointsFinalNorms = sampledPoints.filter(['X Normal', 'Y Normal', 'Z Normal']).values
    return sampledPointsFinal, sampledPointsFinalNorms


def read_csv_pt_file(geoPointFile):
    sampledPoints = pandas.read_csv(geoPointFile,
                                    names=['X', 'Y', 'Z', 'X Normal', 'Y Normal', 'Z Normal'],
                                    usecols=[0, 1, 2, 4, 5, 6])  # skips deviation column (keeps normals)
    sampledPointsFinal = sampledPoints.filter(['X', 'Y', 'Z']).values
    sampledPointsFinalNorms = sampledPoints.filter(['X Normal', 'Y Normal', 'Z Normal']).values
    return sampledPointsFinal, sampledPointsFinalNorms


def read_var_pt_file(geoPointFile):
    sampledPoints = pandas.read_csv(geoPointFile, delim_whitespace=True, comment='#',
                                    names=['X', 'Y', 'Z'],
                                    usecols=[0, 1, 2])  # only reads x, y, z information (contains no normals)
    sampledPointsFinal = sampledPoints.values
    return sampledPointsFinal
