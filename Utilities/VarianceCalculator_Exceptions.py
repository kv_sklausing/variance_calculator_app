
class CustomReadFileError(Exception):
    pass

class CustomKillThreadError(Exception):
    pass
