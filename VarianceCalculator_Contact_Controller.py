'''
Contact UI
'''


from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon, QPixmap


from UserInterface.Windows.VarianceCalculator_Contact_Window import Ui_VarianceCalculator_Contact

class VarianceCalculatorContactController(Ui_VarianceCalculator_Contact):
    def __init__(self, parent=None, appIcon256=None, kvIcon=None, appIcon32=None):

        # Initialize the parent classes.
        super().__init__()

        self.parent = parent
        self.appIcon32 = appIcon32
        self.appIcon256 = appIcon256
        self.kvIcon = kvIcon

        self.init_window()

        self.set_contact_window_logo()

        # Initialize widget actions.
        self.init_widget_actions()

        return

    def init_window(self):
        # Set some options for the window.
        self.setWindowIcon(QIcon(self.appIcon32))
        self.setWindowModality(Qt.ApplicationModal)
        return

    def init_widget_actions(self):
        # Connect the widgets to their actions.
        self.pushButtonOk.clicked.connect(
            self.accept
        )

    def set_contact_window_logo(self):

        if self.appIcon32 is not None:
            self.setWindowIcon(QIcon(self.appIcon32))

        if self.appIcon256 is not None:
            self.appLogoPixmap = QPixmap(self.appIcon256)
            self.appLogoPixmap = self.appLogoPixmap.scaled(self.labelAppLogo.width(), self.labelAppLogo.height(),
                                                           Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.labelAppLogo.setPixmap(self.appLogoPixmap)

        if self.kvIcon is not None:
            self.labelKVPixmap = QPixmap(self.kvIcon)
            self.labelKVPixmap = self.labelKVPixmap.scaled(self.labelKVLogo.width(), self.labelKVLogo.height() / 2,
                                                           Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.labelKVLogo.setPixmap(self.labelKVPixmap)
        return

