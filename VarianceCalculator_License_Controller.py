'''
Licensing UI and functions
'''

import sys
import os

from PySide2.QtWidgets import QMessageBox
from PySide2.QtCore import Qt, Slot
from PySide2.QtGui import QPixmap, QIcon

from UserInterface.Windows.VarianceCalculator_License_Window import Ui_VarianceCalculator_License

from cryptlex.lexactivator import LexActivator, LexStatusCodes, LexActivatorException, PermissionFlags
from Utilities import VarianceCalculator_Main


class VarianceCalculatorLicenseController(Ui_VarianceCalculator_License):

    VarianceMAPPERLICENSECONTROLLER_PAGE_LICENSE_STATUS = 0
    VarianceMAPPERLICENSECONTROLLER_PAGE_ACTIVATE_LICENSE = 1

    def __init__(self, appIcon256=None, appIcon32=None, kvIcon=None, releaseDate=None, version=None):
        # Initialize the parent classes.
        super().__init__()

        self.appIcon32 = appIcon32
        self.appIcon256 = appIcon256
        self.kvIcon = kvIcon

        self.version = version
        self.releaseDate = releaseDate

        self.init_window()

        self.set_license_window_logo()

        self.__licenseIsActive = False
        self.readProductEnvironment()

        self.pushButtonClose.clicked.connect(
            self.on_pushButtonClose_clicked
        )

        self.pushButtonActivate.clicked.connect(
            self.on_pushButtonActivate_clicked
        )


    def init_window(self):
        # Set some options for the window.
        self.setWindowIcon(QIcon(self.appIcon32))
        self.setWindowModality(Qt.ApplicationModal)

        self.labelReleaseDate.setText(self.releaseDate)
        self.labelVersionNumber.setText(self.version)
        return

    def readProductEnvironment(self):
        self.PRODUCT_ID = VarianceCalculator_Main.PRODUCT_ID
        self.PRODUCT_DAT = VarianceCalculator_Main.PRODUCT_DAT
        self.PRODUCT_ENV = VarianceCalculator_Main.PRODUCT_ENV
        print(self.PRODUCT_ENV)
        # if os.path.isfile(ENV_FILENAME):
        #     with open(ENV_FILENAME, 'r') as f:
        #         for line in f:
        #             if line.startswith('PRODUCT_ID'):
        #                 self.PRODUCT_ID = line.split(',')[1].strip()
        #             elif line.startswith('PRODUCT_DAT'):
        #                 self.PRODUCT_DAT = line.split(',')[1].strip()
        #             elif line.startswith('PRODUCT_ENV'):
        #                 self.PRODUCT_ENV = line.split(',')[1].strip()
        # else:
        #     errorMessage = 'Error: could not locate %s' % ENV_FILENAME
        #     print(errorMessage)
        #     QMessageBox.critical(self, 'Error', errorMessage)
        #     sys.exit(0)

        if self.PRODUCT_ENV != 'DEV' and (not self.PRODUCT_ID or not self.PRODUCT_DAT):
            errorMessage = 'Error: Product Info is corrupted.'
            print(errorMessage)
            QMessageBox.critical(self, 'Error', errorMessage)
            sys.exit(0)

    def licenseIsActive(self):
        statusString = ''
        licStatusColor = 'black'
        if self.__licenseIsActive or self.PRODUCT_ENV == 'DEV':
            return True, ''
        try:
            LexActivator.SetProductData(self.PRODUCT_DAT)
            LexActivator.SetProductId(self.PRODUCT_ID, PermissionFlags.LA_USER)
            status = LexActivator.IsLicenseGenuine()
            if LexStatusCodes.LA_OK == status:
                statusString = 'License is genuinely activated!'
                licStatusColor = 'green'
                self.__licenseIsActive = True
            elif LexStatusCodes.LA_EXPIRED == status:
                statusString = 'License is genuinely activated but has expired!'
                licStatusColor = 'orange'
                self.__licenseIsActive = True
            elif LexStatusCodes.LA_SUSPENDED == status:
                statusString = 'License is genuinely activated but has been suspended!'
                licStatusColor = 'orange'
                self.__licenseIsActive = True
            elif LexStatusCodes.LA_GRACE_PERIOD_OVER == status:
                statusString = 'License is genuinely activated but grace period is over!'
                licStatusColor = 'red'
                self.__licenseIsActive = True
            else:
                statusString = 'License is not activated: (status: %s)' % status
                licStatusColor = 'red'
                self.__licenseIsActive = False
        except LexActivatorException as exception:
            print('Error:', exception.code, exception.message)
            if exception.code == 54:
                statusString = 'Please enter a valid license key.'
            else:
                statusString = 'Error: %s' % exception.message
        print(statusString)

        self.set_label_license_status(licStatus=statusString, color=licStatusColor)

        return self.__licenseIsActive, statusString

    def activateLicense(self, licenseKey):
        self.__licenseIsActive = False
        statusString = ''
        licStatusColor = 'black'
        try:
            LexActivator.SetProductData(self.PRODUCT_DAT)
            LexActivator.SetProductId(self.PRODUCT_ID, PermissionFlags.LA_USER)
            LexActivator.SetLicenseKey(licenseKey)
            status = LexActivator.ActivateLicense()
            if LexStatusCodes.LA_OK == status:  # or LexStatusCodes.LA_EXPIRED == status or LexStatusCodes.LA_SUSPENDED == status:
                self.__licenseIsActive = True
                statusString = 'License activated successfully: (status: %s)' % status
                licStatusColor = 'green'
            else:
                statusString = 'License activation failed: (status: %s)' % status
                licStatusColor = 'red'
        except LexActivatorException as exception:
            print('Error:', exception.code, exception.message)
            if exception.code == 54:
                statusString = 'Please enter a valid license key.'
                licStatusColor = 'orange'
            else:
                statusString = 'Error: %s' % exception.message
                licStatusColor = 'red'

        self.set_label_license_status(licStatus=statusString, color=licStatusColor)

        return self.__licenseIsActive, statusString

    def setPage(self, index):
        self.stackedWidget.setCurrentIndex(index)

    @Slot()
    def on_pushButtonActivate_clicked(self):
        licenseKey = self.lineEditLicenseKey.text()
        if licenseKey:
            licenseActive, statusString = self.activateLicense(licenseKey)
            if licenseActive:
                print(statusString)
                self.accept()
            else:
                QMessageBox.critical(self, 'Error Activating License', statusString)
        else:
            QMessageBox.critical(self, 'Error Activating License', 'Please enter a valid license key.')

    @Slot()
    def on_pushButtonClose_clicked(self):
        self.accept()

    def set_license_window_logo(self):

        if self.appIcon32 is not None:
            self.setWindowIcon(QIcon(self.appIcon32))

        if self.appIcon256 is not None:
            self.appLogoPixmap = QPixmap(self.appIcon256)
            self.appLogoPixmap = self.appLogoPixmap.scaled(self.labelAppLogo.width(), self.labelAppLogo.height(),
                                                           Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.labelAppLogo.setPixmap(self.appLogoPixmap)

        if self.kvIcon is not None:
            self.labelKVPixmap = QPixmap(self.kvIcon)
            self.labelKVPixmap = self.labelKVPixmap.scaled(self.labelKVLogo.width(), self.labelKVLogo.height()/2,
                                                           Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.labelKVLogo.setPixmap(self.labelKVPixmap)
        return

