import logging
import Utilities as VarianceUtilities

from UserInterface.Windows.VarianceCalculator_LogWindow import VarianceVisionLogGUI

"""
info(msg, *args, **kwargs)
Logs a message with level INFO on this logger. The arguments are interpreted as for debug().

warning(msg, *args, **kwargs)
Logs a message with level WARNING on this logger. The arguments are interpreted as for debug().

critical(msg, *args, **kwargs)
Logs a message with level CRITICAL on this logger. The arguments are interpreted as for debug().

exception(msg, *args, **kwargs)
Logs a message with level ERROR on this logger. The arguments are interpreted as for debug(). Exception info is added to the logging message. This method should only be called from an exception handler.
"""


class VarianceCalculatorLogging:
    """
    Class that handles processing the analysis routine in its own thread.
    """

    def __init__(self, logFileOutput=None):
        # Initialize the parent class.
        super().__init__()

        self.logFileOutput = logFileOutput

        formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s')
        logLevel = logging.INFO

        self.mainControllerLogger = VarianceUtilities.init_logger(loggerName='Main Controller Logger', toFile=True,
                                                                   toConsole=True,
                                                                   fileLevel=logLevel,
                                                                   logPath=self.logFileOutput)

        self.mainControllerLogger.handlers[0].setFormatter(formatter)
        self.mainControllerLogger.handlers[1].setFormatter(formatter)

        return
