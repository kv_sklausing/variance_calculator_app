import os
import sys

from PySide2 import QtGui
from PySide2.QtGui import QImage, QBrush, QPainter
from PySide2.QtCore import Qt

# Define module constants.
# Get info about this file.

if getattr(sys, 'frozen', False):
    # Set path for UI Assets
    SCRIPT_INTERPRETER_DIR = os.path.dirname(sys.executable)
    USER_INTERFACE_DIR = os.path.join(SCRIPT_INTERPRETER_DIR, 'UserInterface')
else:
    # Set path for UI Assets
    USER_INTERFACE_DIR = os.path.dirname(__file__)

KV_FONT_PATH = os.path.join(USER_INTERFACE_DIR, 'Assets', 'Fonts', 'Helvetica_Neue', 'HelveticaNeue.otf')
KV_FONT_NAME = 'Helvetica Neue'

KV_FONT_PATH_BOLD = os.path.join(USER_INTERFACE_DIR, 'Assets', 'Fonts', 'Helvetica_Neue', 'HelveticaNeue-Bold.otf')
KV_FONT_NAME_BOLD = 'Helvetica Neue Bold'

# function to alter image
def mask_image(imgPath, pad=0):
    # Load image
    # image = QImage.fromData(imgdata, imgtype)
    image = QtGui.QImage(imgPath)

    # convert image to 32-bit ARGB (adds an alpha
    # channel ie transparency factor):
    image.convertToFormat(QImage.Format_ARGB32)

    # Crop image to a square:
    imgsize = min(image.width(), image.height())
    # rect = QRect(
    #     (image.width() - imgsize) / 2,
    #     (image.height() - imgsize) / 2,
    #     imgsize,
    #     imgsize,
    # )
    #
    # image = image.copy(rect)

    # Create the output image with the same dimensions
    # and an alpha channel and make it completely transparent:
    out_img = QImage(imgsize+pad, imgsize+pad, QImage.Format_ARGB32)
    out_img.fill(Qt.transparent)

    # Create a texture brush and paint a circle
    # with the original image onto the output image:
    brush = QBrush(image)

    # Paint the output image
    painter = QPainter(out_img)
    painter.setBrush(brush)

    # Don't draw an outline
    painter.setPen(Qt.NoPen)

    # drawing circle
    painter.drawEllipse(0, 0, imgsize+pad, imgsize+pad)

    # closing painter event
    painter.end()

    # Convert the image to a pixmap and rescale it.
    # pr = QWindow().devicePixelRatio()
    imgPathNEw = os.path.join(os.path.dirname(imgPath), '%s_Cropped.png' % os.path.splitext(os.path.basename(imgPath))[0])
    out_img.save(imgPathNEw, "PNG", -1)

    # pm = QtGui.QPixmap.convertFromImage(out_img)
    # pm.setDevicePixelRatio(pr)
    # size *= pr
    # pm = pm.scaled(size, size, Qt.KeepAspectRatio,
    #                Qt.SmoothTransformation)

    # return back the pixmap data
    return imgPathNEw

KV_ADD = os.path.join(USER_INTERFACE_DIR, 'Assets', 'icons', 'add', 'Icon.png')
KV_ADD_HELP = os.path.join(USER_INTERFACE_DIR, 'Assets', 'icons', 'add', 'add_help.png')

KV_REMOVE = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'delete', 'Icon.png')
KV_REMOVE_HELP = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'delete', 'delete_help.png')

KV_PROCESS = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'process', 'Icon.png')
KV_PROCESS_HELP = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'process', 'process_help.png')

KV_LOGO_ICON = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'kv_logo', 'kinetic-vision-logo.png')

KV_SELECT_DIR = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'select_directory', 'Icon.png')
KV_SELECT_DIR_HELP = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'select_directory', 'select_dir_help.png')

ACTION_EDIT = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'action_edit', 'Icon.png')
ACTION_EDIT_HELP = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'action_edit', 'action_edit_help.png')

ACTION_DELETE = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'action_delete', 'Icon.png')
ACTION_DELETE_HELP = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'action_delete', 'action_delete_help.png')

ACTION_OPEN_OUTPUT_DIR = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'action_open_output_dir', 'Combined Shape.png')
ACTION_OPEN_OUTPUT_DIR_HELP = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'action_open_output_dir', 'action_open_output_dir_help.png')

ACTION_DISPLAY_MESH = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'display', 'display_var_applied_small.png')
ACTION_DISPLAY_MESH_HELP = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'display', 'display_var_applied_small.png')

DROP_DOWN_ARROW = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'down_arrow', 'down_arrow.png')
ACTION_DRAG_ICON = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'drag', 'drag.png')

TC_APP_ICON_32 = mask_image(os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'app_logo', 'VarianceCalculator_App_Icon-32.png'))
TC_APP_ICON_48 = mask_image(os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'app_logo', 'VarianceCalculator_App_Icon-48.png'))
TC_APP_ICON_100 = mask_image(os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'app_logo', 'VarianceCalculator_App_Icon-100.png'))
TC_APP_ICON_256 = mask_image(os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'app_logo', 'VarianceCalculator_App_Icon-256.png'))
TC_APP_ICON_1024 = mask_image(os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'app_logo', 'VarianceCalculator_App_Icon-1024.png'))

TC_APP_SPLASH = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'splash', 'VarianceCalculator_Launch_Image-White-720.png')

KV_ALERT = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'alert', 'kv_alert.png')
KV_ALERT_HELP = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'alert', 'kv_alert_help.png')
KV_ALERT_LARGE = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'alert', 'kv_alert_large.png')

KV_OKAY = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'ok', 'kv_ok.png')
KV_OKAY_HELP = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'ok', 'kv_ok_help.png')

KV_ERROR = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'error', 'kv_error.png')
KV_ERROR_HELP = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'error', 'kv_error_help.png')
KV_ERROR_LARGE = os.path.join(USER_INTERFACE_DIR, 'Assets',  'icons', 'error', 'kv_error_large.png')

DASHBOARD_EMPTY_HOWTO = os.path.join(USER_INTERFACE_DIR, 'HowTo', '01_Dashboard_Empty.png')
SELECT_FILES_SINGLE_HOWTO = os.path.join(USER_INTERFACE_DIR, 'HowTo', '02_Select_Files_Single.png')
FILES_ADDED_SINGLE_HOWTO = os.path.join(USER_INTERFACE_DIR, 'HowTo', '03_Files_Added_Single.png')
SELECT_FILES_MULTI_HOWTO = os.path.join(USER_INTERFACE_DIR, 'HowTo', '04_Select_Files_Multi.png')
FILES_ADDED_MULTI_HOWTO = os.path.join(USER_INTERFACE_DIR, 'HowTo', '05_Files_Added_Multi.png')
CONFIRM_FILES_HOWTO = os.path.join(USER_INTERFACE_DIR, 'HowTo', '06_Confirm_Files.png')
DASHBOARD_PROCESSING_HOWTO = os.path.join(USER_INTERFACE_DIR, 'HowTo', '07_Dashboard_Running.png')

