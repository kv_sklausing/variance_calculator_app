
from UserInterface.VarianceCalculator_Widgets import *


class Ui_VarianceCalculator_Contact(TVDialogWindow):

    def __init__(self, parent=None):
        # Initialize the parent classes.
        super().__init__()

        self.parent = parent

        self.setupUi()

    def setupUi(self):

        self.setObjectName(u"VarianceCalculator_Contact")
        self.resize(600, 155)
        self.setMinimumSize(600, 155)
        self.setMaximumSize(600, 155)

        self.setWindowTitle("Contact Information")

        self.gridLayoutMain = QGridLayout(self)
        self.gridLayoutMain.setObjectName(u"gridLayoutMain")

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayoutScrollContents")

        self.label_for_website()
        self.gridLayout.addWidget(self.labelWebsite, 0, 1, 1, 1)

        self.label_for_email()
        self.gridLayout.addWidget(self.labelEmailAddress, 1, 1, 1, 1)

        self.label_for_website_header()
        self.gridLayout.addWidget(self.labelWebsiteHeader, 0, 0, 1, 1)

        self.label_for_contact()
        self.gridLayout.addWidget(self.labelContact, 1, 0, 1, 1)

        self.label_for_app_logo()
        self.gridLayout.addWidget(self.labelAppLogo, 0, 2, 2, 1)

        self.gridLayoutMain.addLayout(self.gridLayout, 0, 1, 1, 2)

        self.push_button_okay()
        self.gridLayoutMain.addWidget(self.pushButtonOk, 1, 2, 1, 1)

        self.label_for_kv_logo()
        self.gridLayoutMain.addWidget(self.labelKVLogo, 1, 1, 1, 1)

    def label_for_website_header(self):
        self.labelWebsiteHeader = TVLabel()
        self.labelWebsiteHeader.setObjectName(u"labelWebsiteHeader")
        self.labelWebsiteHeader.setMinimumSize(100, 0)
        self.labelWebsiteHeader.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)

        self.labelWebsiteHeader.setText("Website:")
        return

    def label_for_website(self):
        self.labelWebsite = TVLabel()
        self.labelWebsite.setStyleSheet(KV_Stylist.TVLabelNoBold)
        self.labelWebsite.setObjectName(u"labelWebsite")
        self.labelWebsite.setAlignment(Qt.AlignBottom)

        # self.labelWebsite.setText("<a href=www.kinetic-vision.com>")

        self.labelWebsite.setText("<a href=\"www.kinetic-vision.com\">www.kinetic-vision.com</a>")
        # self.labelWebsite.setTextFormat(Qt.RichText)
        self.labelWebsite.setTextInteractionFlags(Qt.TextBrowserInteraction)
        self.labelWebsite.setOpenExternalLinks(True)

        return

    def label_for_contact(self):
        self.labelContact = TVLabel()
        self.labelContact.setObjectName(u"labelContact")
        self.labelContact.setAlignment(Qt.AlignCenter)

        self.labelContact.setText("Contact:")
        return

    def label_for_email(self):
        self.labelEmailAddress = TVLabel()
        self.labelEmailAddress.setStyleSheet(KV_Stylist.TVLabelNoBold)
        self.labelEmailAddress.setObjectName(u"labelEmailAddress")
        self.labelEmailAddress.setAlignment(Qt.AlignCenter)

        # self.labelEmailAddress.setText("varcalculator@saec-kv.com")
        self.labelEmailAddress.setText("<a href='mailto:varcalculator@kinetic-vision.com'>varcalculator@kinetic-vision.com</a>")
        # self.labelEmailAddress.setTextFormat(Qt.RichText)
        self.labelEmailAddress.setTextInteractionFlags(Qt.TextBrowserInteraction)
        self.labelEmailAddress.setOpenExternalLinks(True)
        return

    def label_for_app_logo(self):
        self.labelAppLogo = TVLabel()
        self.labelAppLogo.setObjectName(u"labelAppLogo")
        self.labelAppLogo.setMinimumSize(180, 90)
        self.labelAppLogo.setMaximumSize(180, 90)
        self.labelAppLogo.setAlignment(Qt.AlignCenter)
        # self.labelAppLogo.setScaledContents(True)

        # self.labelAppLogo.setText(u"App Logo")
        return

    def label_for_kv_logo(self):
        self.labelKVLogo = TVLabel()
        self.labelKVLogo.setObjectName(u"labelKVLogo")
        self.labelKVLogo.setMinimumSize(395, 30)
        self.labelKVLogo.setMaximumSize(395, 30)
        self.labelKVLogo.setAlignment(Qt.AlignLeft | Qt.AlignBottom)
        # self.labelAppLogo.setScaledContents(True)

        # self.labelAppLogo.setText(u"App Logo")
        return

    def push_button_okay(self):
        self.pushButtonOk = TVPushButton()
        self.pushButtonOk.setObjectName(u"pushButtonOk")
        self.pushButtonOk.setEnabled(True)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButtonOk.sizePolicy().hasHeightForWidth())
        self.pushButtonOk.setSizePolicy(sizePolicy)
        self.pushButtonOk.setMinimumSize(180, 30)
        self.pushButtonOk.setBaseSize(180, 30)

        self.pushButtonOk.setText("Close")
        self.pushButtonOk.setFont(QFont('Helvetica Neue'))
        return
