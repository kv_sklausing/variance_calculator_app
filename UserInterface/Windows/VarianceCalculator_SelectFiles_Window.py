'''
GUI frontend for VarianceCalculatorProcessor.py.
'''

from PySide2.QtWidgets import (QGridLayout)

from UserInterface.VarianceCalculator_Widgets import *
from UserInterface.VarianceCalculator_Assets import *

class VarianceVisionSelectFilesGUI(TVDialogWindow):
    '''
    Class for the VAR Distribution main application window.
    '''

    def __init__(self):
        # Initialize the parent classes.
        super().__init__()

        self.setupUI()

    def setupUI(self):

        self.vBoxLayout = QVBoxLayout()
        self.vBoxLayout.setContentsMargins(10, 10, 10, 10)
        self.setLayout(self.vBoxLayout)
        # self.vBoxLayout.addWidget(self.testButton)

        centralwidgetSizePolicy = QSizePolicy()
        centralwidgetSizePolicy.setVerticalPolicy(QSizePolicy.MinimumExpanding)
        centralwidgetSizePolicy.setHorizontalPolicy(QSizePolicy.MinimumExpanding)
        centralwidgetSizePolicy.setHorizontalStretch(100)
        centralwidgetSizePolicy.setVerticalStretch(100)
        centralwidgetSizePolicy.setHeightForWidth(True)
        self.setSizePolicy(centralwidgetSizePolicy)

        # Add Add/Delete Groupbox
        self.frame_select_input_type()

        self.frame_select_files()
        self.vBoxLayout.addWidget(self.frameSelectFiles, stretch=100, alignment=Qt.AlignCenter)

        self.frame_select_dirs()
        # self.vBoxLayout.addWidget(self.frameSelectDirs, stretch=100, alignment=Qt.AlignCenter)

        self.frame_input_params()

        self.frame_confirm_files()
        self.vBoxLayout.addWidget(self.frameConfirmFiles, stretch=100, alignment=Qt.AlignCenter)

        self.frame_confirm_dirs()
        # self.vBoxLayout.addWidget(self.frameConfirmDirs, stretch=100, alignment=Qt.AlignCenter)

        # Set initial state
        self.pushButtonSelectSingleFile.setEnabled(False)
        self.pushButtonSelectMultiFile.setEnabled(True)

        self.addPairType = 'file'

        return

    def frame_select_input_type(self):

        self.frameSelectInputType = TVFrame(name='frameSelectInputType')
        self.frameSelectInputType.setFixedSize(QSize(660, 100))
        frameSelectFilesSizePolicy = QSizePolicy()
        frameSelectFilesSizePolicy.setHorizontalPolicy(QSizePolicy.Fixed)
        frameSelectFilesSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        frameSelectFilesSizePolicy.setHeightForWidth(True)
        self.frameSelectInputType.setSizePolicy(frameSelectFilesSizePolicy)

        # Groupbox Layout
        frameSelectFilesLayout = QGridLayout()
        frameSelectFilesLayout.setHorizontalSpacing(0)
        frameSelectFilesLayout.setVerticalSpacing(10)
        frameSelectFilesLayout.setContentsMargins(15, 0, 15, 10)
        self.frameSelectInputType.setLayout(frameSelectFilesLayout)

        # Create Widgets for Groupbox
        self.pushButtonSelectSingleFile = TVPushButton(name='pushButtonSelectSingleFile', text='Single File')
        self.pushButtonSelectSingleFile.setFixedSize(QSize(320, 30))
        self.pushButtonSelectSingleFile.setStyleSheet(KV_Stylist.TVPushButtonBlue)

        self.pushButtonSelectMultiFile = TVPushButton(name='pushButtonSelectMultiFile', text='Multiple Files')
        self.pushButtonSelectMultiFile.setFixedSize(QSize(320, 30))
        self.pushButtonSelectSingleFile.setStyleSheet(KV_Stylist.TVPushButtonGray)

        self.labelSelectFiles = TVLabel(name='labelSelectFiles', text='Select Files')
        self.labelSelectFiles.setAlignment(Qt.AlignCenter)

        # Add both buttons
        frameSelectFilesLayout.addWidget(self.labelSelectFiles, 0, 0, 1, 2)
        frameSelectFilesLayout.addWidget(self.pushButtonSelectSingleFile, 1, 0, 1, 1)
        frameSelectFilesLayout.addWidget(self.pushButtonSelectMultiFile, 1, 1, 1, 1)

        self.selectButtonGroup = QButtonGroup()
        self.selectButtonGroup.addButton(self.pushButtonSelectSingleFile, 1)
        self.selectButtonGroup.addButton(self.pushButtonSelectMultiFile, 2)

        self.vBoxLayout.addWidget(self.frameSelectInputType, stretch=100, alignment=Qt.AlignCenter)  # row, column, row span, column span

        return

    def frame_select_files(self):

        self.frameSelectFiles = TVFrame(name='frameSelectFiles')

        # Size Groupbox
        self.frameSelectFiles.setFixedSize(QSize(660, 100))
        frameSelectFilesSizePolicy = QSizePolicy()
        frameSelectFilesSizePolicy.setHorizontalPolicy(QSizePolicy.Fixed)
        frameSelectFilesSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        frameSelectFilesSizePolicy.setHeightForWidth(True)
        self.frameSelectFiles.setSizePolicy(frameSelectFilesSizePolicy)

        # Groupbox Layout
        frameSelectFilesLayout = QHBoxLayout()
        self.frameSelectFiles.setLayout(frameSelectFilesLayout)

        self.groupboxTestFile = TVGroupBox(name='groupboxTestFile', text='Test Mesh')
        self.groupBoxLayoutTestFile = QVBoxLayout()
        self.groupboxTestFile.setLayout(self.groupBoxLayoutTestFile)

        self.pushButtonSelectTestFile = TVPushButton(name='pushButtonSelectTestFile', text='Select')
        self.pushButtonSelectTestFile.setStyleSheet(KV_Stylist.TVPushButtonWhite)

        self.pushButtonSelectTestFile.setFixedSize(QSize(300, 30))
        self.groupBoxLayoutTestFile.addWidget(self.pushButtonSelectTestFile)

        self.lineEdittestFile = TVLineEdit(name='lineEdittestFile', icon=ACTION_EDIT)
        self.lineEdittestFile.setFixedSize(QSize(300, 30))
        self.lineEdittestFile.setReadOnly(True)

        self.groupboxRefFile = TVGroupBox(name='groupboxRefFile', text='Ref File')
        self.groupBoxLayoutRefFile = QVBoxLayout()
        self.groupboxRefFile.setLayout(self.groupBoxLayoutRefFile)

        self.pushButtonSelectRefFile = TVPushButton(name='pushButtonSelectRefFile', text='Select')
        self.pushButtonSelectRefFile.setStyleSheet(KV_Stylist.TVPushButtonWhite)


        self.pushButtonSelectRefFile.setFixedSize(QSize(300, 30))
        self.groupBoxLayoutRefFile.addWidget(self.pushButtonSelectRefFile)

        self.lineEditRefFile = TVLineEdit(name='lineEditRefFile', icon=ACTION_EDIT)
        self.lineEditRefFile.setFixedSize(QSize(300, 30))
        self.lineEditRefFile.setReadOnly(True)

        # Add both buttons
        frameSelectFilesLayout.addWidget(self.groupboxTestFile, stretch=100, alignment=Qt.AlignCenter)
        frameSelectFilesLayout.addWidget(self.groupboxRefFile, stretch=100, alignment=Qt.AlignCenter)

        return

    def frame_select_dirs(self):
        self.frameSelectDirs = TVFrame(name='frameSelectDirs')

        # Size Groupbox
        self.frameSelectDirs.setFixedSize(QSize(660, 100))
        frameSelectDirsSizePolicy = QSizePolicy()
        frameSelectDirsSizePolicy.setHorizontalPolicy(QSizePolicy.Fixed)
        frameSelectDirsSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        frameSelectDirsSizePolicy.setHeightForWidth(True)
        self.frameSelectDirs.setSizePolicy(frameSelectDirsSizePolicy)

        # Groupbox Layout
        frameSelectDirsLayout = QHBoxLayout()
        self.frameSelectDirs.setLayout(frameSelectDirsLayout)

        self.groupboxTestDir = TVGroupBox(name='groupboxTestDir', text='Test Mesh Folder')
        self.groupBoxLayoutTestDir = QVBoxLayout()
        self.groupboxTestDir.setLayout(self.groupBoxLayoutTestDir)

        self.pushButtonSelectTestDir = TVPushButton(name='pushButtonSelectTestDir', text='Select')
        self.pushButtonSelectTestDir.setStyleSheet(KV_Stylist.TVPushButtonWhite)


        self.pushButtonSelectTestDir.setFixedSize(QSize(300, 30))
        self.groupBoxLayoutTestDir.addWidget(self.pushButtonSelectTestDir)

        self.lineEdittestDir = TVLineEdit(name='lineEdittestDir', icon=KV_SELECT_DIR)
        self.lineEdittestDir.setFixedSize(QSize(300, 30))
        self.lineEdittestDir.setReadOnly(True)

        self.groupboxRefDir = TVGroupBox(name='groupboxRefDir', text='Ref File Folder')
        self.groupBoxLayoutRefDir = QVBoxLayout()
        self.groupboxRefDir.setLayout(self.groupBoxLayoutRefDir)

        self.pushButtonSelectRefDir = TVPushButton(name='pushButtonSelectRefDir', text='Select')
        self.pushButtonSelectRefDir.setStyleSheet(KV_Stylist.TVPushButtonWhite)

        self.pushButtonSelectRefDir.setFixedSize(QSize(300, 30))
        self.groupBoxLayoutRefDir.addWidget(self.pushButtonSelectRefDir)

        self.lineEditRefDir = TVLineEdit(name='lineEditRefDir', icon=KV_SELECT_DIR)
        self.lineEditRefDir.setFixedSize(QSize(300, 30))
        self.lineEditRefDir.setReadOnly(True)

        # Add both buttons
        frameSelectDirsLayout.addWidget(self.groupboxTestDir, stretch=100, alignment=Qt.AlignCenter)
        frameSelectDirsLayout.addWidget(self.groupboxRefDir, stretch=100, alignment=Qt.AlignCenter)

        return

    def frame_input_params(self):

        desiredPtDensityGB = TVGroupBox(name='desiredPtDensityGB', text='Desired Point Count')
        desiredPtDensityGBLayout = QVBoxLayout()
        desiredPtDensityGB.setLayout(desiredPtDensityGBLayout)
        desiredPtDensityGB.setFixedSize(QSize(200, 80))
        self.spinBoxPointDensity = TVSpinBox(name='pushButtonOpenPDF', value=60000)
        self.spinBoxPointDensity.setMinimum(0)
        self.spinBoxPointDensity.setMaximum(10000000)
        self.spinBoxPointDensity.setFixedSize(QSize(180, 30))
        self.spinBoxPointDensity.setGroupSeparatorShown(True)
        desiredPtDensityGBLayout.addWidget(self.spinBoxPointDensity)

        outputDirGB = TVGroupBox(name='outputDirGB', text='Output Directory')
        outputDirGBLayout = QVBoxLayout()
        outputDirGB.setLayout(outputDirGBLayout)
        outputDirGB.setFixedSize(QSize(200, 80))
        self.lineEditOutputDir = TVLineEdit(name='pushButtonClose', icon=KV_SELECT_DIR)
        self.lineEditOutputDir.setFixedSize(QSize(180, 30))
        self.lineEditOutputDir.setReadOnly(True)
        outputDirGBLayout.addWidget(self.lineEditOutputDir)

        outputFileTypeGB = TVGroupBox(name='outputFileTypeGB', text='Output File Type')
        outputFileTypeGBLayout = QVBoxLayout()
        outputFileTypeGB.setLayout(outputFileTypeGBLayout)
        outputFileTypeGB.setFixedSize(QSize(200, 80))
        self.comboBoxOutputType = TVComboBox(name='comboBoxOutputType', items=('CSV', 'VAR', 'CSV / VAR'))
        self.comboBoxOutputType.setFixedSize(QSize(180, 30))
        outputFileTypeGBLayout.addWidget(self.comboBoxOutputType)

        self.geomagicSaveUnitsGB = TVGroupBox(name='geomagicSaveUnitsGB', text='Mesh Units (CAD Pts Sampled)')
        geomagicSaveUnitsGBLayout = QVBoxLayout()
        self.geomagicSaveUnitsGB.setLayout(geomagicSaveUnitsGBLayout)
        self.geomagicSaveUnitsGB.setFixedSize(QSize(200, 80))
        self.comboBoxGeoUnits = TVComboBox(name='comboBoxGeoUnits', items=('Inches', 'Millimeters', 'Meters'))
        self.comboBoxGeoUnits.setFixedSize(QSize(180, 30))
        geomagicSaveUnitsGBLayout.addWidget(self.comboBoxGeoUnits)

        self.frameInputParams = TVFrame(name='framePushButtons')
        self.frameInputParamLayout = QHBoxLayout()
        self.frameInputParams.setLayout(self.frameInputParamLayout)

        self.frameInputParams.setFixedSize(QSize(660, 100))
        frameInputParamSizePolicy = QSizePolicy()
        frameInputParamSizePolicy.setHorizontalPolicy(QSizePolicy.Fixed)
        frameInputParamSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        frameInputParamSizePolicy.setHeightForWidth(True)
        self.frameInputParams.setSizePolicy(frameInputParamSizePolicy)

        # Add both buttons
        self.frameInputParamLayout.addWidget(desiredPtDensityGB, stretch=100, alignment=Qt.AlignCenter)
        self.frameInputParamLayout.addWidget(outputDirGB, stretch=100, alignment=Qt.AlignCenter)
        self.frameInputParamLayout.addWidget(outputFileTypeGB, stretch=100, alignment=Qt.AlignCenter)

        self.vBoxLayout.addWidget(self.frameInputParams, stretch=100, alignment=Qt.AlignCenter)

        return

    def frame_confirm_files(self):

        # Frame Layout for OK Button Only
        self.frameConfirmFiles = TVFrame(name='frameConfirmFiles')
        self.frameConfirmFilesLayout = QHBoxLayout()
        self.frameConfirmFilesLayout.setSpacing(20)
        self.frameConfirmFiles.setLayout(self.frameConfirmFilesLayout)
        # Size Frame
        self.frameConfirmFiles.setFixedSize(QSize(660, 100))
        frameConfirmFilesSizePolicy = QSizePolicy()
        frameConfirmFilesSizePolicy.setHorizontalPolicy(QSizePolicy.Fixed)
        frameConfirmFilesSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        frameConfirmFilesSizePolicy.setHeightForWidth(True)
        self.frameConfirmFiles.setSizePolicy(frameConfirmFilesSizePolicy)
        # OK Button
        self.pushButtonSaveFiles = TVPushButton(name='pushButtonSaveFiles', text='OK')
        self.pushButtonSaveFiles.setFixedSize(QSize(180, 30))
        self.pushButtonSaveFiles.setEnabled(False)
        self.pushButtonSaveFiles.setStyleSheet(KV_Stylist.TVPushButtonBlueDisabled)
        # Add OK Button only to frame
        self.frameConfirmFilesLayout.addWidget(self.pushButtonSaveFiles, stretch=100, alignment=Qt.AlignCenter)

    def frame_confirm_dirs(self):
        # Frame Layout
        self.frameConfirmDirs = TVFrame(name='frameConfirmDirs')
        self.frameConfirmDirsLayout = QHBoxLayout()
        self.frameConfirmDirsLayout.setSpacing(20)
        self.frameConfirmDirs.setLayout(self.frameConfirmDirsLayout)
        # Size Frame
        self.frameConfirmDirs.setFixedSize(QSize(660, 100))
        frameConfirmDirsSizePolicy = QSizePolicy()
        frameConfirmDirsSizePolicy.setHorizontalPolicy(QSizePolicy.Fixed)
        frameConfirmDirsSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        frameConfirmDirsSizePolicy.setHeightForWidth(True)
        self.frameConfirmDirs.setSizePolicy(frameConfirmDirsSizePolicy)
        # Confirm Button
        self.pushButtonSortFiles = TVPushButton(name='pushButtonSortFiles', text='Confirm Files')
        self.pushButtonSortFiles.setFixedSize(QSize(180, 30))
        self.pushButtonSortFiles.setEnabled(False)
        self.pushButtonSortFiles.setStyleSheet(KV_Stylist.TVPushButtonBlueDisabled)
        # OK Button
        self.pushButtonSaveDirs = TVPushButton(name='pushButtonSaveDirs', text='OK')
        self.pushButtonSaveDirs.setFixedSize(QSize(180, 30))
        self.pushButtonSaveDirs.setEnabled(False)
        self.pushButtonSaveDirs.setStyleSheet(KV_Stylist.TVPushButtonBlueDisabled)
        # Add buttons to layout
        self.frameConfirmDirsLayout.addWidget(self.pushButtonSortFiles, stretch=100, alignment=Qt.AlignCenter)
        self.frameConfirmDirsLayout.addWidget(self.pushButtonSaveDirs, stretch=100, alignment=Qt.AlignCenter)

        return

    def reset_single_file_selection_ui(self):
        self.update_single_file_selection()

        self.groupBoxLayoutTestFile.replaceWidget(self.lineEdittestFile, self.pushButtonSelectTestFile)
        self.pushButtonSelectTestFile.setVisible(True)
        self.lineEdittestFile.setVisible(False)

        self.groupBoxLayoutRefFile.replaceWidget(self.lineEditRefFile, self.pushButtonSelectRefFile)
        self.pushButtonSelectRefFile.setVisible(True)
        self.lineEditRefFile.setVisible(False)

        self.pushButtonSaveFiles.setEnabled(False)
        self.pushButtonSaveFiles.setStyleSheet(KV_Stylist.TVPushButtonBlueDisabled)

        self.remove_geomagic_units_combobox()
        return

    def update_single_file_selection(self):

        # Sets input switchs
        self.pushButtonSelectSingleFile.setEnabled(False)
        self.pushButtonSelectSingleFile.setStyleSheet(KV_Stylist.TVPushButtonBlue)
        self.pushButtonSelectMultiFile.setEnabled(True)
        self.pushButtonSelectMultiFile.setStyleSheet(KV_Stylist.TVPushButtonGray)

        # Changes the Ref/Test widgets
        self.vBoxLayout.replaceWidget(self.frameSelectDirs, self.frameSelectFiles)
        self.frameSelectFiles.setVisible(True)
        self.frameSelectDirs.setVisible(False)

        # Changes the confirm/save widgets
        self.vBoxLayout.replaceWidget(self.frameConfirmDirs, self.frameConfirmFiles)
        self.frameConfirmFiles.setVisible(True)
        self.frameConfirmDirs.setVisible(False)

        self.addPairType = 'file'
        return

    def reset_multi_file_selection_ui(self):
        self.update_multi_file_selection()

        self.groupBoxLayoutTestDir.replaceWidget(self.lineEdittestDir, self.pushButtonSelectTestDir)
        self.pushButtonSelectTestDir.setVisible(True)
        self.lineEdittestDir.setVisible(False)

        self.groupBoxLayoutRefDir.replaceWidget(self.lineEditRefDir, self.pushButtonSelectRefDir)
        self.pushButtonSelectRefDir.setVisible(True)
        self.lineEditRefDir.setVisible(False)

        self.pushButtonSortFiles.setEnabled(False)
        self.pushButtonSortFiles.setStyleSheet(KV_Stylist.TVPushButtonBlueDisabled)
        self.pushButtonSaveDirs.setEnabled(False)
        self.pushButtonSaveDirs.setStyleSheet(KV_Stylist.TVPushButtonBlueDisabled)

        self.remove_geomagic_units_combobox()
        return

    def update_multi_file_selection(self):

        # Sets input switchs
        self.pushButtonSelectSingleFile.setEnabled(True)
        self.pushButtonSelectSingleFile.setStyleSheet(KV_Stylist.TVPushButtonGray)
        self.pushButtonSelectMultiFile.setEnabled(False)
        self.pushButtonSelectMultiFile.setStyleSheet(KV_Stylist.TVPushButtonBlue)

        # Changes the Ref/Test widgets
        self.vBoxLayout.replaceWidget(self.frameSelectFiles, self.frameSelectDirs)
        self.frameSelectFiles.setVisible(False)
        self.frameSelectDirs.setVisible(True)

        # Changes the confirm/save widgets
        self.vBoxLayout.replaceWidget(self.frameConfirmFiles, self.frameConfirmDirs)
        self.frameConfirmFiles.setVisible(False)
        self.frameConfirmDirs.setVisible(True)

        self.addPairType = 'dir'
        return

    def update_test_file_widget(self, TestFilePath=None):
        # Changes the Test file widget
        if TestFilePath:
            self.groupBoxLayoutTestFile.replaceWidget(self.pushButtonSelectTestFile, self.lineEdittestFile)
            self.pushButtonSelectTestFile.setVisible(False)
            self.lineEdittestFile.setVisible(True)
            self.lineEdittestFile.setText(TestFilePath)
        return

    def reset_test_file_widget(self):
        # Changes the Test file widget
        if self.lineEdittestFile.isVisible():
            self.groupBoxLayoutTestFile.replaceWidget(self.lineEdittestFile, self.pushButtonSelectTestFile)
            self.pushButtonSelectTestFile.setVisible(True)
            self.lineEdittestFile.setVisible(False)
        return

    def update_test_dir_widget(self, TestDirPath=None):
        # Changes the Test dir widget
        if TestDirPath:
            self.groupBoxLayoutTestDir.replaceWidget(self.pushButtonSelectTestDir, self.lineEdittestDir)
            self.pushButtonSelectTestDir.setVisible(False)
            self.lineEdittestDir.setVisible(True)
            self.lineEdittestDir.setText(TestDirPath)
        return

    def reset_test_dir_widget(self):
        # Changes the Test file widget
        if self.lineEdittestDir.isVisible():
            self.groupBoxLayoutTestDir.replaceWidget(self.lineEdittestDir, self.pushButtonSelectTestDir)
            self.pushButtonSelectTestDir.setVisible(True)
            self.lineEdittestDir.setVisible(False)
        return

    def update_ref_file_widget(self, RefFilePath=None):
        # Changes the Ref file widget
        if RefFilePath:
            self.groupBoxLayoutRefFile.replaceWidget(self.pushButtonSelectRefFile, self.lineEditRefFile)
            self.pushButtonSelectRefFile.setVisible(False)
            self.lineEditRefFile.setVisible(True)
            self.lineEditRefFile.setText(RefFilePath)
        return

    def reset_ref_file_widget(self):
        # Changes the Test file widget
        if self.lineEditRefFile.isVisible():
            self.groupBoxLayoutRefFile.replaceWidget(self.lineEditRefFile, self.pushButtonSelectRefFile)
            self.pushButtonSelectRefFile.setVisible(True)
            self.lineEditRefFile.setVisible(False)
        return

    def update_ref_dir_widget(self, RefDirPath=None):
        # Changes the Ref dir widget
        if RefDirPath:
            self.groupBoxLayoutRefDir.replaceWidget(self.pushButtonSelectRefDir, self.lineEditRefDir)
            self.pushButtonSelectRefDir.setVisible(False)
            self.lineEditRefDir.setVisible(True)
            self.lineEditRefDir.setText(RefDirPath)
        return

    def reset_ref_dir_widget(self):
        # Changes the Test file widget
        if self.lineEditRefDir.isVisible():
            self.groupBoxLayoutRefDir.replaceWidget(self.lineEditRefDir, self.pushButtonSelectRefDir)
            self.pushButtonSelectRefDir.setVisible(True)
            self.lineEditRefDir.setVisible(False)
        return

    def add_geomagic_units_combobox(self):
        if not self.comboBoxGeoUnits.isVisible():
            if self.addPairType == 'dir':
                self.frameConfirmDirsLayout.addWidget(self.geomagicSaveUnitsGB, stretch=100, alignment=Qt.AlignCenter)
            else:
                self.frameConfirmFilesLayout.addWidget(self.geomagicSaveUnitsGB, stretch=100, alignment=Qt.AlignCenter)
            self.geomagicSaveUnitsGB.setVisible(True)
        return

    def remove_geomagic_units_combobox(self):
        if self.addPairType == 'dir':
            self.frameConfirmDirsLayout.removeWidget(self.geomagicSaveUnitsGB)
        else:
            self.frameConfirmFilesLayout.removeWidget(self.geomagicSaveUnitsGB)
        self.geomagicSaveUnitsGB.setVisible(False)
        return