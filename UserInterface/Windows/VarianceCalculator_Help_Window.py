from UserInterface.VarianceCalculator_Widgets import *

class Ui_VarianceCalculator_Help(TVMainWindow):

    def __init__(self, parent=None):
        # Initialize the parent classes.
        super().__init__()

        self.parent = parent

        self.setupUi()

    def setupUi(self):
        self.setObjectName("Help Window")
        self.setWindowTitle("Variance Calculator Help")

        self.resize(1160, 800)
        self.setMinimumSize(self.size())

        centralwidgetSizePolicy = QSizePolicy()
        centralwidgetSizePolicy.setVerticalPolicy(QSizePolicy.MinimumExpanding)
        centralwidgetSizePolicy.setHorizontalPolicy(QSizePolicy.MinimumExpanding)
        centralwidgetSizePolicy.setHorizontalStretch(100)
        centralwidgetSizePolicy.setVerticalStretch(100)
        centralwidgetSizePolicy.setHeightForWidth(True)
        self.setSizePolicy(centralwidgetSizePolicy)

        # Use for MainWindow
        self.centralwidget = TVWidget(name='centralWidget')
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setMinimumSize(QSize(self.width(), self.height() - 30))
        self.gridLayoutMain = QGridLayout()
        self.gridLayoutMain.setObjectName(u"gridLayoutMain")
        self.centralwidget.setLayout(self.gridLayoutMain)

        self.setCentralWidget(self.centralwidget)

        self.scrollArea = TVScrollArea(self)
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setMinimumHeight(self.height() - 200)

        self.label_header()
        self.gridLayoutMain.addWidget(self.labelHeader, 0, 0, 1, 1)

        self.gridLayoutMain.addWidget(self.scrollArea, 1, 0, 1, 1)

        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.verticalScrollBar = self.scrollArea.verticalScrollBar()
        self.verticalScrollBar.setStyleSheet(KV_Stylist.TVScrollBarVertical)
        self.horizontalScrollBar = self.scrollArea.horizontalScrollBar()
        self.horizontalScrollBar.setStyleSheet(KV_Stylist.TVScrollBarHorizontal)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.gridLayoutScrollContents = QGridLayout(self.scrollAreaWidgetContents)
        self.gridLayoutScrollContents.setObjectName(u"gridLayoutScrollContents")
        self.gridLayoutScrollContents.setVerticalSpacing(25)
        self.gridLayoutScrollContents.setContentsMargins(50, 10, 50, 10)

        self.content_01()
        self.gridLayoutScrollContents.addWidget(self.groupBox_01, 0, 0, 1, 1)
        self.gridLayoutScrollContents.addWidget(self.label_img_01, 0, 2, 1, 1)

        self.content_02()
        self.gridLayoutScrollContents.addWidget(self.groupBox_02, 1, 0, 1, 1)
        self.gridLayoutScrollContents.addWidget(self.label_img_02, 1, 2, 1, 1)

        self.content_03()
        self.gridLayoutScrollContents.addWidget(self.groupBox_03, 2, 0, 1, 1)
        self.gridLayoutScrollContents.addWidget(self.label_img_03, 2, 2, 1, 1)

        self.content_04()
        self.gridLayoutScrollContents.addWidget(self.groupBox_04, 3, 0, 1, 1)
        self.gridLayoutScrollContents.addWidget(self.label_img_04, 3, 2, 1, 1)

        self.content_05()
        self.gridLayoutScrollContents.addWidget(self.groupBox_05, 4, 0, 1, 1)
        self.gridLayoutScrollContents.addWidget(self.label_img_05, 4, 2, 1, 1)

        self.content_06()
        self.gridLayoutScrollContents.addWidget(self.groupBox_06, 5, 0, 1, 1)
        self.gridLayoutScrollContents.addWidget(self.label_img_06, 5, 2, 1, 1)

        self.content_07()
        self.gridLayoutScrollContents.addWidget(self.groupBox_07, 6, 0, 1, 1)
        self.gridLayoutScrollContents.addWidget(self.label_img_07, 6, 2, 1, 1)

        self.horizontalSpacerMain = QSpacerItem(50, 0, QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        self.gridLayoutScrollContents.addItem(self.horizontalSpacerMain, 0, 0, 1, 1)

        self.frame_push_buttons()
        self.gridLayoutMain.addWidget(self.framePushButtons, 2, 0, 1, 1)

    def content_01(self):

        self.groupBox_01 = TVGroupBox(self.scrollAreaWidgetContents)
        self.groupBox_01.setObjectName(u"groupBox_01")
        self.groupBox_01.setMinimumSize(400, 300)
        self.groupBox_01.setTitle("Header 01")
        self.groupBox_01.setStyleSheet(KV_Stylist.TVHelpGroupBox)

        self.gridLayoutGroupBox01 = QGridLayout(self.groupBox_01)
        self.gridLayoutGroupBox01.setObjectName(u"gridLayoutGroupBox01")

        # self.gridLayoutGroupBox01.setS

        self.label_text_01 = TVLabel(self.groupBox_01)
        self.label_text_01.setObjectName(u"label_text_01")
        self.label_text_01.setText('Text 01')
        self.label_text_01.setStyleSheet(KV_Stylist.TVLabelNoBold)
        self.label_text_01.setAlignment(Qt.AlignLeft)
        self.label_text_01.setWordWrap(True)
        # self.label_text_01.setMinimumWidth(self.groupBox_01.width())

        self.label_img_01 = TVLabel(self.scrollAreaWidgetContents)
        self.label_img_01.setObjectName(u"label_img_01")
        self.label_img_01.setText('Image 01')

        self.gridLayoutGroupBox01.addWidget(self.label_text_01, 0, 0, 1, 1)

        return

    def content_02(self):

        self.groupBox_02 = TVGroupBox(self.scrollAreaWidgetContents)
        self.groupBox_02.setObjectName(u"groupBox_02")
        self.groupBox_02.setMinimumSize(400, 300)
        self.groupBox_02.setTitle("Header 02")
        self.groupBox_02.setStyleSheet(KV_Stylist.TVHelpGroupBox)

        self.gridLayoutGroupBox02 = QGridLayout(self.groupBox_02)
        self.gridLayoutGroupBox02.setObjectName(u"gridLayoutGroupBox02")

        self.label_text_02 = TVLabel(self.groupBox_02)
        self.label_text_02.setObjectName(u"label_text_02")
        self.label_text_02.setText('Text 02')
        self.label_text_02.setStyleSheet(KV_Stylist.TVLabelNoBold)
        self.label_text_02.setAlignment(Qt.AlignLeft)
        self.label_text_02.setWordWrap(True)

        self.label_img_02 = TVLabel(self.scrollAreaWidgetContents)
        self.label_img_02.setObjectName(u"label_img_02")
        self.label_img_02.setText('Image 02')

        self.gridLayoutGroupBox02.addWidget(self.label_text_02, 0, 0, 1, 1)

        return

    def content_03(self):

        self.groupBox_03 = TVGroupBox(self.scrollAreaWidgetContents)
        self.groupBox_03.setObjectName(u"groupBox_03")
        self.groupBox_03.setMinimumSize(400, 300)
        self.groupBox_03.setTitle("Header 03")
        self.groupBox_03.setStyleSheet(KV_Stylist.TVHelpGroupBox)

        self.gridLayoutGroupBox03 = QGridLayout(self.groupBox_03)
        self.gridLayoutGroupBox03.setObjectName(u"gridLayoutGroupBox03")

        self.label_text_03 = TVLabel(self.groupBox_03)
        self.label_text_03.setObjectName(u"label_text_03")
        self.label_text_03.setText('Text 03')
        self.label_text_03.setStyleSheet(KV_Stylist.TVLabelNoBold)
        self.label_text_03.setAlignment(Qt.AlignLeft)
        self.label_text_03.setWordWrap(True)


        self.label_img_03 = TVLabel(self.scrollAreaWidgetContents)
        self.label_img_03.setObjectName(u"label_img_03")
        self.label_img_03.setText('Image 03')

        self.gridLayoutGroupBox03.addWidget(self.label_text_03, 0, 0, 1, 1)

        return

    def content_04(self):

        self.groupBox_04 = TVGroupBox(self.scrollAreaWidgetContents)
        self.groupBox_04.setObjectName(u"groupBox_04")
        self.groupBox_04.setMinimumSize(400, 300)
        self.groupBox_04.setTitle("Header 03")
        self.groupBox_04.setStyleSheet(KV_Stylist.TVHelpGroupBox)

        self.gridLayoutGroupBox04 = QGridLayout(self.groupBox_04)
        self.gridLayoutGroupBox04.setObjectName(u"gridLayoutGroupBox04")

        self.label_text_04 = TVLabel(self.groupBox_04)
        self.label_text_04.setObjectName(u"label_text_04")
        self.label_text_04.setText('Text 03')
        self.label_text_04.setStyleSheet(KV_Stylist.TVLabelNoBold)
        self.label_text_04.setAlignment(Qt.AlignLeft)
        self.label_text_04.setWordWrap(True)


        self.label_img_04 = TVLabel(self.scrollAreaWidgetContents)
        self.label_img_04.setObjectName(u"label_img_04")
        self.label_img_04.setText('Image 03')

        self.gridLayoutGroupBox04.addWidget(self.label_text_04, 0, 0, 1, 1)

        return

    def content_05(self):

        self.groupBox_05 = TVGroupBox(self.scrollAreaWidgetContents)
        self.groupBox_05.setObjectName(u"groupBox_05")
        self.groupBox_05.setMinimumSize(400, 300)
        self.groupBox_05.setTitle("Header 03")
        self.groupBox_05.setStyleSheet(KV_Stylist.TVHelpGroupBox)

        self.gridLayoutGroupBox05 = QGridLayout(self.groupBox_05)
        self.gridLayoutGroupBox05.setObjectName(u"gridLayoutGroupBox05")

        self.label_text_05 = TVLabel(self.groupBox_05)
        self.label_text_05.setObjectName(u"label_text_05")
        self.label_text_05.setText('Text 03')
        self.label_text_05.setStyleSheet(KV_Stylist.TVLabelNoBold)
        self.label_text_05.setAlignment(Qt.AlignLeft)
        self.label_text_05.setWordWrap(True)


        self.label_img_05 = TVLabel(self.scrollAreaWidgetContents)
        self.label_img_05.setObjectName(u"label_img_05")
        self.label_img_05.setText('Image 03')

        self.gridLayoutGroupBox05.addWidget(self.label_text_05, 0, 0, 1, 1)

        return

    def content_06(self):

        self.groupBox_06 = TVGroupBox(self.scrollAreaWidgetContents)
        self.groupBox_06.setObjectName(u"groupBox_06")
        self.groupBox_06.setMinimumSize(400, 300)
        self.groupBox_06.setTitle("Header 03")
        self.groupBox_06.setStyleSheet(KV_Stylist.TVHelpGroupBox)

        self.gridLayoutGroupBox06 = QGridLayout(self.groupBox_06)
        self.gridLayoutGroupBox06.setObjectName(u"gridLayoutGroupBox06")

        self.label_text_06 = TVLabel(self.groupBox_06)
        self.label_text_06.setObjectName(u"label_text_06")
        self.label_text_06.setText('Text 03')
        self.label_text_06.setStyleSheet(KV_Stylist.TVLabelNoBold)
        self.label_text_06.setAlignment(Qt.AlignLeft)
        self.label_text_06.setWordWrap(True)


        self.label_img_06 = TVLabel(self.scrollAreaWidgetContents)
        self.label_img_06.setObjectName(u"label_img_06")
        self.label_img_06.setText('Image 03')

        self.gridLayoutGroupBox06.addWidget(self.label_text_06, 0, 0, 1, 1)

        return

    def content_07(self):

        self.groupBox_07 = TVGroupBox(self.scrollAreaWidgetContents)
        self.groupBox_07.setObjectName(u"groupBox_07")
        self.groupBox_07.setMinimumSize(400, 300)
        self.groupBox_07.setTitle("Header 03")
        self.groupBox_07.setStyleSheet(KV_Stylist.TVHelpGroupBox)

        self.gridLayoutGroupBox07 = QGridLayout(self.groupBox_07)
        self.gridLayoutGroupBox07.setObjectName(u"gridLayoutGroupBox07")

        self.label_text_07 = TVLabel(self.groupBox_07)
        self.label_text_07.setObjectName(u"label_text_07")
        self.label_text_07.setText('Text 03')
        self.label_text_07.setStyleSheet(KV_Stylist.TVLabelNoBold)
        self.label_text_07.setAlignment(Qt.AlignLeft)
        self.label_text_07.setWordWrap(True)


        self.label_img_07 = TVLabel(self.scrollAreaWidgetContents)
        self.label_img_07.setObjectName(u"label_img_07")
        self.label_img_07.setText('Image 03')

        self.gridLayoutGroupBox07.addWidget(self.label_text_07, 0, 0, 1, 1)

        return

    def frame_push_buttons(self):

        self.pushButtonOpenPDF = TVPushButton(name='pushButtonOpenPDF', text='Open PDF Document')
        self.pushButtonOpenPDF.setFixedSize(180, 30)

        self.pushButtonClose = TVPushButton(name='pushButtonClose', text='Close')
        self.pushButtonClose.setFixedSize(180, 30)

        self.framePushButtons = TVFrame(name='framePushButtons')
        framePushButtonsLayout = QGridLayout()
        self.framePushButtons.setLayout(framePushButtonsLayout)

        frameInputParamSizePolicy = QSizePolicy()
        frameInputParamSizePolicy.setHorizontalPolicy(QSizePolicy.MinimumExpanding)
        frameInputParamSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        frameInputParamSizePolicy.setHeightForWidth(True)
        self.framePushButtons.setSizePolicy(frameInputParamSizePolicy)

        self.horizontalSpacer = QSpacerItem(50, 0, QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        framePushButtonsLayout.addItem(self.horizontalSpacer, 0, 0, 1, 1)

        # Add both buttons
        # framePushButtonsLayout.addWidget(self.pushButtonOpenPDF, 0, 1, 1, 1)
        framePushButtonsLayout.addWidget(self.pushButtonClose, 0, 2, 1, 1)

        return

    def label_header(self):
        self.labelHeader = TVLabel(name='labelHeader')
        self.setMinimumHeight(60)
        self.labelHeader.setText('How to Use the Variance Calculator')
        self.labelHeader.setAlignment(Qt.AlignCenter)
        self.labelHeader.setStyleSheet(KV_Stylist.TVHelpLabelHeader)

        return
