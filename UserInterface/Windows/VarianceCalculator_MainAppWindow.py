'''
Main Dashboard frontend for VarianceCalculator.py.
'''

# from multiprocessing import freeze_support
from PySide2.QtWidgets import (QTableWidgetItem, QHeaderView,
                               QAbstractScrollArea, QGridLayout)
from PySide2.QtCore import Qt

import UserInterface.VarianceCalculator_Assets as kvAssets
from UserInterface.VarianceCalculator_Widgets import *


class VarianceVisionMainGUI(TVMainWindow):
    '''
    Class for the VAR Distribution main application window.
    '''

    def __init__(self, font=None):
        # Initialize the parent classes.
        super().__init__()

        self.font = QFont(kvAssets.KV_FONT_NAME)

        self.setupUI()

    def setupUI(self):
        self.setMinimumSize(QSize(1080, 720))

        self.menu_help_actions()
        self.menu_preprocess_actions()

        self.centralwidget = TVWidget(name='centralWidget')
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setMinimumSize(QSize(self.width(), self.height() - 60))

        self.gridlayout = QGridLayout()
        self.gridlayout.setHorizontalSpacing(50)
        self.gridlayout.setVerticalSpacing(15)
        self.gridlayout.setContentsMargins(10, 10, 10, 10)

        self.centralwidget.setLayout(self.gridlayout)
        centralwidgetSizePolicy = QSizePolicy()
        centralwidgetSizePolicy.setVerticalPolicy(QSizePolicy.MinimumExpanding)
        centralwidgetSizePolicy.setHorizontalPolicy(QSizePolicy.MinimumExpanding)

        centralwidgetSizePolicy.setHorizontalStretch(100)
        centralwidgetSizePolicy.setVerticalStretch(100)
        centralwidgetSizePolicy.setHeightForWidth(True)
        self.centralwidget.setSizePolicy(centralwidgetSizePolicy)

        self.setCentralWidget(self.centralwidget)

        self.stacked_widget()
        self.gridLayout.addWidget(self.stackedWidget, 0, 0, 1, 1)
        # self.stackedWidget.addWidget()  # Main Variance Page
        # self.stackedWidget.addWidget()  # CAD Pt Cover Page
        # self.stackedWidget.addWidget()  # Mesh Close Manifold Page
        # self.stackedWidget.addWidget()  # Reshift Mesh / CAD Page

        return

    def stacked_widget(self):
        self.stackedWidget = QStackedWidget(self)
        self.stackedWidget.setObjectName(u"stackedWidget")
        return

    def groupbox_source_files(self):

        self.groupBoxSourceFiles = TVGroupBox(name='groupBoxSourceFiles')

        # Size Groupbox
        # self.groupBoxSourceFiles.setFixedSize(QSize(485, 75))
        groupBoxAddDelSizePolicy = QSizePolicy()
        groupBoxAddDelSizePolicy.setHorizontalPolicy(QSizePolicy.Fixed)
        groupBoxAddDelSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        groupBoxAddDelSizePolicy.setHeightForWidth(True)
        self.groupBoxSourceFiles.setSizePolicy(groupBoxAddDelSizePolicy)

        self.groupBoxSourceFiles.setTitle('Source Files')
        # self.groupBoxSourceFiles.setAlignment(3)

        # Groupbox Layout
        groupBoxAddDelLayout = QHBoxLayout()
        self.groupBoxSourceFiles.setLayout(groupBoxAddDelLayout)

        # Create Widgets for Groupbox
        self.pushButtonAddPair = TVPushButton(name='pushButtonAddPair', text='Add')
        self.pushButtonAddPair.setFixedSize(QSize(180, 30))
        self.pushButtonAddPair.setIcon(QIcon(kvAssets.KV_ADD))
        self.pushButtonAddPair.setIconSize(QSize(25, 25))


        self.horizontalSpacerInputs = QSpacerItem(30, 20, hData=QSizePolicy.Fixed, vData=QSizePolicy.Fixed)

        self.pushButtonRemovePair = TVPushButton(name='pushButtonRemovePair', text='')
        self.pushButtonRemovePair.setFixedSize(QSize(60, 30))
        self.pushButtonRemovePair.setIcon(QIcon(kvAssets.KV_REMOVE))
        self.pushButtonRemovePair.setIconSize(QSize(25, 25))
        self.pushButtonRemovePair.setStyleSheet(KV_Stylist.TVPushButtonGray)

        self.pushButtonClearComp = TVPushButton(name='pushButtonAddPair', text='Clear Completed Jobs')
        self.pushButtonClearComp.setFixedSize(QSize(240, 30))
        self.pushButtonClearComp.setStyleSheet(KV_Stylist.TVPushButtonGray)

        # Add both buttons
        groupBoxAddDelLayout.addWidget(self.pushButtonAddPair)
        groupBoxAddDelLayout.addItem(self.horizontalSpacerInputs)
        groupBoxAddDelLayout.addWidget(self.pushButtonRemovePair)
        groupBoxAddDelLayout.addWidget(self.pushButtonClearComp)
        return

    def frame_processing(self):

        self.frameProcess = TVFrame(name='frameProcess')
        self.frameProcess.setFixedSize(QSize(220, 75))
        groupBoxProcessSizePolicy = QSizePolicy()
        groupBoxProcessSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        groupBoxProcessSizePolicy.setHorizontalPolicy(QSizePolicy.Fixed)
        groupBoxProcessSizePolicy.setHeightForWidth(self.frameProcess.sizePolicy().hasHeightForWidth())
        self.frameProcess.setSizePolicy(groupBoxProcessSizePolicy)

        # Groupbox Layout
        self.frameProcessLayout = QVBoxLayout()
        self.frameProcess.setLayout(self.frameProcessLayout)

        self.verticalSpacerProcess = QSpacerItem(20, 0, hData=QSizePolicy.Fixed, vData=QSizePolicy.Fixed)

        self.pushButtonStartProcessing = TVPushButton(name='pushButtonStartProcessing', text='Process Files')
        self.pushButtonStartProcessing.setFixedSize(QSize(180, 30))
        self.pushButtonStartProcessing.setIcon(QIcon(kvAssets.KV_PROCESS))
        self.pushButtonStartProcessing.setIconSize(QSize(25, 25))
        self.pushButtonStartProcessing.setEnabled(False)
        self.pushButtonStartProcessing.setStyleSheet(KV_Stylist.TVPushButtonBlueDisabled)

        self.pushButtonResumeProcessing = TVPushButton(name='pushButtonResumeProcessing', text='Resume Processing')
        self.pushButtonResumeProcessing.setFixedSize(self.pushButtonStartProcessing.size())

        self.pushButtonPauseProcessing = TVPushButton(name='pushButtonPauseProcessing', text='Pause Processing')
        self.pushButtonPauseProcessing.setFixedSize(self.pushButtonStartProcessing.size())

        # Add button
        self.frameProcessLayout.addItem(self.verticalSpacerProcess)
        self.frameProcessLayout.addWidget(self.pushButtonStartProcessing)

    def table_view(self):
        self.tableWidgetMeshQueue = TVTableWidget(name='tableWidgetMeshQueue')
        # self.tableWidgetMeshQueue.setMinimumSize(self.size())

        self.tableWidgetMeshQueue.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.tableWidgetMeshQueue.setAlternatingRowColors(True)
        self.tableWidgetMeshQueue.setSelectionBehavior(QAbstractItemView.SelectRows)

        tableWidgetMeshQueueSizePolicy = QSizePolicy()
        tableWidgetMeshQueueSizePolicy.setHorizontalPolicy(QSizePolicy.MinimumExpanding)
        tableWidgetMeshQueueSizePolicy.setVerticalPolicy(QSizePolicy.MinimumExpanding)
        tableWidgetMeshQueueSizePolicy.setHorizontalStretch(100)
        tableWidgetMeshQueueSizePolicy.setVerticalStretch(100)
        self.tableWidgetMeshQueue.setSizePolicy(tableWidgetMeshQueueSizePolicy)

        self.tableWidgetMeshQueue.setColumnCount(8)
        headerItem1 = QTableWidgetItem()
        headerItem2 = QTableWidgetItem()
        headerItem3 = QTableWidgetItem()
        headerItem4 = QTableWidgetItem()
        headerItem5 = QTableWidgetItem()
        headerItem6 = QTableWidgetItem()
        headerItem7 = QTableWidgetItem()
        headerItem8 = QTableWidgetItem()

        headerItem1.setText('Test')
        headerItem2.setText('Ref')
        headerItem3.setText('Output Directory')
        headerItem4.setText('Output File Type')
        headerItem5.setText('Mesh Analysis')
        headerItem6.setText('Desired Point Count')
        headerItem7.setText('Status')
        headerItem8.setText('Actions')

        self.tableWidgetMeshQueue.setHorizontalHeaderItem(0, headerItem1)
        self.tableWidgetMeshQueue.setHorizontalHeaderItem(1, headerItem2)
        self.tableWidgetMeshQueue.setHorizontalHeaderItem(2, headerItem3)
        self.tableWidgetMeshQueue.setHorizontalHeaderItem(3, headerItem4)
        self.tableWidgetMeshQueue.setHorizontalHeaderItem(4, headerItem5)
        self.tableWidgetMeshQueue.setHorizontalHeaderItem(5, headerItem6)
        self.tableWidgetMeshQueue.setHorizontalHeaderItem(6, headerItem7)
        self.tableWidgetMeshQueue.setHorizontalHeaderItem(7, headerItem8)

        # Get Header Info
        tableWidgetMeshQueueHeader = self.tableWidgetMeshQueue.horizontalHeader()
        tableWidgetMeshQueueHeader.setDefaultAlignment(Qt.AlignCenter)
        tableWidgetMeshQueueHeader.setSectionResizeMode(QHeaderView.Stretch)
        tableWidgetMeshQueueHeader.setStretchLastSection(False)

        return

    def label_kv_logo(self):
        self.labelKVLogo = TVLabel(name='labelKVLogo')
        kvLogoPixmap = QPixmap(kvAssets.KV_LOGO_ICON)
        self.labelKVLogo.setPixmap(kvLogoPixmap)
        # self.labelKVLogo.setFixedSize(self.size())
        self.labelKVLogo.setStyleSheet(KV_Stylist.TVLabelKVLogo)

        return

    def label_kv_txt(self):
        self.labelKVText = TVLabel(name='labelKVText')
        self.labelKVText.setText('© 2021 SAEC/Kinetic Vision, Inc. | All rights reserved')
        # self.labelKVText.setFixedSize(self.size())
        self.labelKVText.setStyleSheet(KV_Stylist.TVLabelKVLogo)

        return

    def menu_help_actions(self):

        self.menubar = self.menuBar()
        self.menubar.setFont(QFont('Helvetica Neue'))
        self.menubar.setStyleSheet(KV_Stylist.TVMenuBar)

        menuFileHelp = TVMenu(name='menuFileHelp', text='Help')

        self.actionFileAbout = TVAction(name='actionFileAbout', text='About')
        self.actionFileHowTo = TVAction(name='actionFileHowTo', text='How To')
        self.actionFileContact = TVAction(name='actionFileContact', text='Contact')
        self.menuFileCadApp = TVMenu(name='actionCadApp', text='CAD Application')

        self.actionCadGroup = QActionGroup(self)
        self.actionCadGroup.setExclusive(True)

        self.actionGeoApp = TVAction(name='actionGeoApp', text='Geomagic Wrap')
        self.actionGeoApp.setCheckable(True)
        self.actionGeoApp.setChecked(True)
        self.menuFileCadApp.addAction(self.actionGeoApp)
        self.actionCadGroup.addAction(self.actionGeoApp)

        # self.actionGmeshApp = TVAction(name='actionGmeshApp', text='GMesh')
        # self.actionGmeshApp.setCheckable(True)
        # self.menuFileCadApp.addAction(self.actionGmeshApp)
        # self.actionCadGroup.addAction(self.actionGmeshApp)

        menuFileHelp.addAction(self.actionFileAbout)
        menuFileHelp.addSeparator()

        menuFileHelp.addAction(self.actionFileHowTo)
        menuFileHelp.addSeparator()

        menuFileHelp.addAction(self.actionFileContact)
        menuFileHelp.addSeparator()

        menuFileHelp.addMenu(self.menuFileCadApp)

        self.menubar.addMenu(menuFileHelp)

        return

    def menu_preprocess_actions(self):

        menuFilePreProc = TVMenu(name='menuFilePreProc', text='Preprocessing')

        self.actionReshiftCADandMesh = TVAction(name='actionReshiftCADandMesh', text='Reshift CAD / Mesh')
        self.actionCloseManifold = TVAction(name='actionCloseManifold', text='Close Meshes')
        self.actionCADPtCover = TVAction(name='actionCADPtCover', text='CAD Pt Creation')

        menuFilePreProc.addAction(self.actionReshiftCADandMesh)
        menuFilePreProc.addSeparator()

        menuFilePreProc.addAction(self.actionCloseManifold)
        menuFilePreProc.addSeparator()

        menuFilePreProc.addAction(self.actionCADPtCover)
        menuFilePreProc.addSeparator()

        self.menubar.addMenu(menuFilePreProc)

        return

    def show_pause_processing_widget(self):
        if self.pushButtonStartProcessing.isVisible():
            # Changes the Ref file widget
            self.frameProcessLayout.replaceWidget(self.pushButtonStartProcessing, self.pushButtonPauseProcessing)
            self.pushButtonStartProcessing.setVisible(False)
            self.pushButtonPauseProcessing.setVisible(True)

        if self.pushButtonResumeProcessing.isVisible():
            # Changes the Ref file widget
            self.frameProcessLayout.replaceWidget(self.pushButtonResumeProcessing, self.pushButtonPauseProcessing)
            self.pushButtonResumeProcessing.setVisible(False)
            self.pushButtonPauseProcessing.setVisible(True)
        return

    def show_resume_processing_widget(self):
        # Changes the Ref file widget
        self.frameProcessLayout.replaceWidget(self.pushButtonPauseProcessing, self.pushButtonResumeProcessing)
        self.pushButtonPauseProcessing.setVisible(False)
        self.pushButtonResumeProcessing.setVisible(True)
        return

    def show_start_processing_widget(self):
        # Changes the Ref file widget
        if self.pushButtonPauseProcessing.isVisible():
            self.frameProcessLayout.replaceWidget(self.pushButtonPauseProcessing, self.pushButtonStartProcessing)
            self.pushButtonPauseProcessing.setVisible(False)
            self.pushButtonStartProcessing.setVisible(True)

        if self.pushButtonResumeProcessing.isVisible():
            self.frameProcessLayout.replaceWidget(self.pushButtonResumeProcessing, self.pushButtonStartProcessing)
            self.pushButtonResumeProcessing.setVisible(False)
            self.pushButtonStartProcessing.setVisible(True)
        return
