'''
Sort Files Control for ThicknesCalculator.py.
'''

from PySide2.QtWidgets import (QGridLayout)
from PySide2.QtCore import Qt

from UserInterface.VarianceCalculator_Widgets import *


class VarianceVisionSortFilesGUI(TVDialogWindow):
    '''
    Class for the VAR Distribution main application window.
    '''

    def __init__(self, parent=None):
        # Initialize the parent classes.
        super().__init__()

        self.parent = parent

        self.setupUI()


    def setupUI(self):
        self.setFixedSize(self.size())

        self.gridLayout = QGridLayout()
        self.gridLayout.setContentsMargins(25, 25, 25, 25)
        self.gridLayout.setHorizontalSpacing(5)
        self.gridLayout.setVerticalSpacing(25)

        self.setLayout(self.gridLayout)

        centralwidgetSizePolicy = QSizePolicy()
        centralwidgetSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        centralwidgetSizePolicy.setHorizontalPolicy(QSizePolicy.Fixed)
        centralwidgetSizePolicy.setHorizontalStretch(100)
        centralwidgetSizePolicy.setVerticalStretch(100)
        centralwidgetSizePolicy.setHeightForWidth(True)
        self.setSizePolicy(centralwidgetSizePolicy)

        self.label_for_table()
        self.gridLayout.addWidget(self.labelSortTableHeader, 0, 0, 1, 2)  # row, column, row span, column span

        self.table_view_Test()
        self.gridLayout.addWidget(self.tableTestSort, 1, 0, 1, 1)  # row, column, row span, column span

        self.table_view_Ref()
        self.gridLayout.addWidget(self.tableRefSort, 1, 1, 1, 1)  # row, column, row span, column span

        self.frame_push_button_confirm()
        self.gridLayout.addWidget(self.frameConfirm, 2, 0, 1, 2)  # row, column, row span, column span

        return


    def label_for_table(self):
        self.labelSortTableHeader = TVLabel(name='labelSortTableHeader', text='Drag to Sort Mesh Files')
        self.labelSortTableHeader.setAlignment(Qt.AlignCenter)
        return


    def table_view_Test(self):
        self.tableTestSort = TVSortableTable(name='tableTestSort')
        self.tableTestSort.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.TestScrollBarVert = self.tableTestSort.scrollBarVert
        self.TestScrollBarHor = self.tableTestSort.scrollBarHorz
        # self.tableTestSort.setMinimumSize(self.size())

        # self.tableTestSort.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)

        tableWidgetMeshQueueSizePolicy = QSizePolicy()
        tableWidgetMeshQueueSizePolicy.setHorizontalPolicy(QSizePolicy.MinimumExpanding)
        tableWidgetMeshQueueSizePolicy.setVerticalPolicy(QSizePolicy.MinimumExpanding)
        tableWidgetMeshQueueSizePolicy.setHorizontalStretch(100)
        tableWidgetMeshQueueSizePolicy.setVerticalStretch(100)
        self.tableTestSort.setSizePolicy(tableWidgetMeshQueueSizePolicy)

        # QTableView
        self.TestModel = self.tableTestSort.model
        self.TestModel.setColumnCount(1)
        headerItem1 = QStandardItem()
        headerItem1.setText('Test Files')
        self.TestModel.setHorizontalHeaderItem(0, headerItem1)

        # # QTableWidget
        # # self.TestModel = self.tableTestSort.model
        # self.tableTestSort.setColumnCount(2)
        # headerItem1 = QTableWidgetItem()
        # headerItem1.setText('Test Files')
        # self.tableTestSort.setHorizontalHeaderItem(0, headerItem1)

        return


    def table_view_Ref(self):
        self.tableRefSort = TVSortableTable(name='tableRefSort')
        self.RefScrollBarVert = self.tableRefSort.scrollBarVert
        self.RefScrollBarHorz = self.tableRefSort.scrollBarHorz
        # self.tableRefSort.setMinimumSize(self.size())

        # self.tableRefSort.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)

        tableWidgetMeshQueueSizePolicy = QSizePolicy()
        tableWidgetMeshQueueSizePolicy.setHorizontalPolicy(QSizePolicy.MinimumExpanding)
        tableWidgetMeshQueueSizePolicy.setVerticalPolicy(QSizePolicy.MinimumExpanding)
        tableWidgetMeshQueueSizePolicy.setHorizontalStretch(100)
        tableWidgetMeshQueueSizePolicy.setVerticalStretch(100)
        self.tableRefSort.setSizePolicy(tableWidgetMeshQueueSizePolicy)

        # QTableView
        self.RefModel = self.tableRefSort.model
        self.RefModel.setColumnCount(1)
        standardItem2 = QStandardItem()
        standardItem2.setText('Ref Files')
        self.RefModel.setHorizontalHeaderItem(0, standardItem2)

        # # QTableWidget
        # # self.RefModel = self.tableRefSort.model
        # self.tableRefSort.setColumnCount(2)
        # standardItem2 = QTableWidgetItem()
        # standardItem2.setText('Ref Files')
        # self.tableRefSort.setHorizontalHeaderItem(0, standardItem2)

        return


    def frame_push_button_confirm(self):

        self.frameConfirm = TVFrame(name='frameConfirm')
        # self.frameConfirm.setFixedSize(QSize(220, 75))
        groupBoxProcessSizePolicy = QSizePolicy()
        groupBoxProcessSizePolicy.setVerticalPolicy(QSizePolicy.Preferred)
        groupBoxProcessSizePolicy.setHorizontalPolicy(QSizePolicy.Preferred)
        groupBoxProcessSizePolicy.setHeightForWidth(self.frameConfirm.sizePolicy().hasHeightForWidth())
        self.frameConfirm.setSizePolicy(groupBoxProcessSizePolicy)

        # Groupbox Layout
        frameProcessLayout = QHBoxLayout()
        self.frameConfirm.setLayout(frameProcessLayout)

        self.pushButtonConfirm = TVPushButton(name='pushButtonConfirm', text='Confirm')
        self.pushButtonConfirm.setFixedSize(QSize(180, 30))

        # Add button
        frameProcessLayout.addWidget(self.pushButtonConfirm)
        return

