
from UserInterface.VarianceCalculator_Widgets import *


class Ui_VarianceCalculator_License(TVDialogWindow):

    def __init__(self, parent=None):
        # Initialize the parent classes.
        super().__init__()

        self.parent = parent

        self.setupUi()

    def setupUi(self):
        self.setWindowFlag(Qt.WindowContextHelpButtonHint, False)

        self.setObjectName(u"self")
        self.resize(600, 155)
        self.setMinimumSize(600, 155)
        self.setMaximumSize(600, 155)

        self.setWindowTitle("Variance Calculator License")

        self.gridLayout = QGridLayout(self)
        self.gridLayout.setObjectName(u"gridLayoutScrollContents")

        self.gridForButton = QGridLayout()
        self.gridForButton.setObjectName(u"gridForButton")

        self.gridLayoutActivate = QGridLayout()
        self.gridLayoutActivate.setObjectName(u"gridLayoutActivate")

        self.horizontalSpacerButtonActivate = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.gridLayoutActivate.addItem(self.horizontalSpacerButtonActivate, 0, 0, 1, 1)

        self.gridLayout.addLayout(self.gridForButton, 1, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.gridLayout.addItem(self.verticalSpacer, 2, 0, 1, 1)

        self.stacked_widget()
        self.gridLayout.addWidget(self.stackedWidget, 0, 0, 1, 1)

        self.page_lic_status()
        self.stackedWidget.addWidget(self.gridPageLicenseStatus)

        self.page_lic_activate()
        self.stackedWidget.addWidget(self.pageActivateLicense)

        self.label_license()
        self.gridLayoutLicStatusPage.addWidget(self.labelLicense, 0, 0, 1, 1)

        self.label_license_status()
        self.gridLayoutLicStatusPage.addWidget(self.labelLicenseStatus, 0, 1, 1, 1)

        self.label_version()
        self.gridLayoutLicStatusPage.addWidget(self.labelVersion, 1, 0, 1, 1)

        self.label_version_number()
        self.gridLayoutLicStatusPage.addWidget(self.labelVersionNumber, 1, 1, 1, 1)

        self.label_release()
        self.gridLayoutLicStatusPage.addWidget(self.labelRelease, 2, 0, 1, 1)

        self.label_release_date()
        self.gridLayoutLicStatusPage.addWidget(self.labelReleaseDate, 2, 1, 1, 1)

        self.label_app_logo()
        self.gridLayoutLicStatusPage.addWidget(self.labelAppLogo, 0, 3, 3, 1)

        self.horizontalSpacerLicStatus = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.gridLayoutLicStatusPage.addItem(self.horizontalSpacerLicStatus, 0, 2, 1, 1)

        self.label_lic_instruction()
        self.gridLayoutLicActivatePage.addWidget(self.labelActivateInstruct, 0, 0, 1, 2)

        self.label_enter_lic_key()
        self.gridLayoutLicActivatePage.addWidget(self.labelEnterLicKey, 1, 0, 1, 1)

        self.line_edit_lic_key()
        self.gridLayoutLicActivatePage.addWidget(self.lineEditLicenseKey, 1, 1, 1, 1)

        self.push_button_activate()
        self.gridLayoutActivate.addWidget(self.pushButtonActivate, 0, 1, 1, 1)

        self.push_button_close()
        self.gridForButton.addWidget(self.pushButtonClose, 0, 1, 1, 1)

        self.gridLayoutLicActivatePage.addLayout(self.gridLayoutActivate, 2, 0, 1, 2)
        self.stackedWidget.addWidget(self.pageActivateLicense)

        self.label_for_kv_logo()
        self.gridForButton.addWidget(self.labelKVLogo, 0, 0, 1, 1)

        self.stackedWidget.setCurrentIndex(1)
        self.pushButtonActivate.setDefault(True)
        QWidget.setTabOrder(self.lineEditLicenseKey, self.pushButtonActivate)
        QWidget.setTabOrder(self.pushButtonActivate, self.pushButtonClose)

    def stacked_widget(self):
        self.stackedWidget = QStackedWidget(self)
        self.stackedWidget.setObjectName(u"stackedWidget")
        return

    def page_lic_status(self):
        self.gridPageLicenseStatus = QWidget()
        self.gridPageLicenseStatus.setObjectName(u"gridPageLicenseStatus")
        self.gridLayoutLicStatusPage = QGridLayout(self.gridPageLicenseStatus)
        self.gridLayoutLicStatusPage.setObjectName(u"gridLayoutLicStatusPage")
        self.gridLayoutLicStatusPage.setContentsMargins(0, 0, 0, 0)


        return

    def page_lic_activate(self):
        self.pageActivateLicense = QWidget()
        self.pageActivateLicense.setObjectName(u"pageActivateLicense")
        self.gridLayoutLicActivatePage = QGridLayout(self.pageActivateLicense)
        self.gridLayoutLicActivatePage.setObjectName(u"gridLayoutLicActivatePage")
        self.gridLayoutLicActivatePage.setContentsMargins(0, 0, 0, 0)
        return

    def label_license(self):
        self.labelLicense = TVLabel(self.gridPageLicenseStatus)
        self.labelLicense.setObjectName(u"labelLicense")
        self.labelLicense.setText("License Status:")
        # font = QFont()
        # font.setPointSize(12)
        # self.labelLicense.setFont(font)
        return

    def label_license_status(self):
        self.labelLicenseStatus = TVLabel(self.gridPageLicenseStatus)
        self.labelLicenseStatus.setObjectName(u"labelLicenseStatus")
        self.labelLicenseStatus.setWordWrap(True)
        return

    def set_label_license_status(self, licStatus='ACTIVE', color='green'):
        self.labelLicenseStatus.setStyleSheet(KV_Stylist.TVLabelInfo)
        styleSheet = self.labelLicenseStatus.styleSheet()
        updatedStyleSheet = ' '.join([styleSheet, "QLabel{ color: %s;}" % color])
        self.labelLicenseStatus.setStyleSheet(updatedStyleSheet)
        self.labelLicenseStatus.setText(licStatus)
        return

    def label_version(self):
        self.labelVersion = TVLabel(self.gridPageLicenseStatus)
        self.labelVersion.setObjectName(u"labelVersion")
        self.labelVersion.setText("Version #:")
        # self.labelVersion.setFont(font)
        return

    def label_version_number(self):
        self.labelVersionNumber = TVLabel(self.gridPageLicenseStatus)
        self.labelVersionNumber.setObjectName(u"labelVersionNumber")
        self.labelVersionNumber.setText("1.0.0")
        self.labelVersionNumber.setStyleSheet(KV_Stylist.TVLabelInfo)
        # self.labelVersionNumber.setFont(font)
        return

    def label_release(self):
        self.labelRelease = TVLabel(self.gridPageLicenseStatus)
        self.labelRelease.setObjectName(u"labelRelease")
        self.labelRelease.setText("Release Date:")
        # self.labelRelease.setFont(font)
        return

    def label_release_date(self):
        self.labelReleaseDate = TVLabel(self.gridPageLicenseStatus)
        self.labelReleaseDate.setObjectName(u"labelReleaseDate")
        self.labelReleaseDate.setText("2/8/2021")
        self.labelReleaseDate.setStyleSheet(KV_Stylist.TVLabelInfo)
        # self.labelReleaseDate.setFont(font)
        return

    def label_app_logo(self):
        self.labelAppLogo = TVLabel(self.gridPageLicenseStatus)
        self.labelAppLogo.setObjectName(u"labelAppLogo")
        self.labelAppLogo.setMinimumSize(180, 90)
        self.labelAppLogo.setMaximumSize(180, 90)
        self.labelAppLogo.setFrameShape(QFrame.Box)
        self.labelAppLogo.setAlignment(Qt.AlignCenter)
        return

    def label_lic_instruction(self):
        self.labelActivateInstruct = TVLabel(self.pageActivateLicense)
        self.labelActivateInstruct.setObjectName(u"labelActivateInstruct")
        self.labelActivateInstruct.setText("To activate a new license, enter it below and click the \"Activate\" button.")
        self.labelActivateInstruct.setStyleSheet(KV_Stylist.TVLabelNoBold)
        # self.labelActivateInstruct.setFont(font2)
        return

    def label_enter_lic_key(self):
        self.labelEnterLicKey = TVLabel(self.pageActivateLicense)
        self.labelEnterLicKey.setObjectName(u"labelEnterLicKey")
        self.labelEnterLicKey.setText("Enter License Key:")
        # font2 = QFont()
        # font2.setPointSize(10)
        # self.labelEnterLicKey.setFont(font2)
        return

    def line_edit_lic_key(self):
        self.lineEditLicenseKey = TVLineEdit(self.pageActivateLicense)
        self.lineEditLicenseKey.setObjectName(u"lineEditLicenseKey")
        self.lineEditLicenseKey.setMinimumSize(QSize(0, 30))
        return

    def push_button_activate(self):

        self.pushButtonActivate = TVPushButton(self.pageActivateLicense)
        self.pushButtonActivate.setObjectName(u"pushButtonActivate")
        self.pushButtonActivate.setFixedSize(QSize(180, 30))
        self.pushButtonActivate.setText("Activate")
        return

    def push_button_close(self):

        self.pushButtonClose = TVPushButton(self)
        self.pushButtonClose.setObjectName(u"pushButtonClose")
        self.pushButtonClose.setFixedSize(QSize(180, 30))
        self.pushButtonClose.setText("Close")

        return

    def label_for_kv_logo(self):
        self.labelKVLogo = TVLabel()
        self.labelKVLogo.setObjectName(u"labelKVLogo")
        self.labelKVLogo.setMinimumSize(395, 30)
        self.labelKVLogo.setMaximumSize(395, 30)
        self.labelKVLogo.setAlignment(Qt.AlignLeft | Qt.AlignBottom)
        # self.labelAppLogo.setScaledContents(True)

        # self.labelAppLogo.setText(u"App Logo")
        return






