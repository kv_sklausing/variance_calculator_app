'''
GUI frontend for VarianceCalculator_Logger_Controller.py.
'''

from multiprocessing import freeze_support
from PySide2.QtWidgets import (QGridLayout)

from UserInterface.VarianceCalculator_Widgets import *


class VarianceVisionLogGUI(TVDialogWindow):
    '''
    Class for the VAR Distribution main application window.
    '''

    def __init__(self):
        # Initialize the parent classes.
        super().__init__()

        self.setupUI()

    def setupUI(self):

        # self.centralwidget = TVWidget(name='centralWidget')
        # self.centralwidget.setObjectName(u"centralwidget")
        # self.centralwidget.setMinimumSize(QSize(self.width(), self.height() - 30))

        self.setWindowTitle("My Form")

        self.gridlayout = QGridLayout()
        self.gridlayout.setHorizontalSpacing(50)
        self.gridlayout.setVerticalSpacing(15)
        self.gridlayout.setContentsMargins(10, 10, 10, 10)
        self.setLayout(self.gridlayout)

        # Add Add/Delete Groupbox
        self.log_window()
        self.gridlayout.addWidget(self.logWindow, 0, 0)  # row, column, row span, column span


        return

    def log_window(self):

        self.logWindow = TVPlainTextEdit(name='logWindow')

        # Size logWindow
        logWindowSizePolicy = QSizePolicy()
        logWindowSizePolicy.setHorizontalPolicy(QSizePolicy.MinimumExpanding)
        logWindowSizePolicy.setVerticalPolicy(QSizePolicy.MinimumExpanding)
        logWindowSizePolicy.setHorizontalStretch(100)
        logWindowSizePolicy.setVerticalStretch(100)
        self.logWindow.setSizePolicy(logWindowSizePolicy)

        return


