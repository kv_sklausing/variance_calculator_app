'''
The Kinetic Vision Imaging Group library (KVIMG)

This code is developed, maintained, and owned by Kinetic Vision with the intent
of aiding imaging engineers in typical project work.
'''

__package__ = 'UserInterface'
__version__ = '1.0.0'
__author__ = 'Kinetic Vision'

# The contents of __all__ are imported when the user calls - import from "folder" *
__all__ = [
    'VarianceCalculator_Assets.py',
    'VarianceCalculator_HelpRichText.py',
    'VarianceCalculator_Styles.py',
    'VarianceCalculator_Widgets.py'
]

from .VarianceCalculator_Assets import *
from .VarianceCalculator_HelpRichText import *
from .VarianceCalculator_Styles import *
from .VarianceCalculator_Widgets import *

