import os

from UserInterface.VarianceCalculator_Assets import DROP_DOWN_ARROW

darkBlue = "#0070C0"
lightBlue = "#00B0F0"
red = "#FF0000"
green = "#00B050"
orange = "#FFC000"
black = "#000000"
purple = "#7030A0"
white = "#ffffff"
kvBlue = "rgb(0, 124, 185)"  # #007cb9

TVTableWidget = (
    "QTableWidget { "
    "font: 12px;"
    # "font-weight: bold;"
    "color: #7f7f7f; "
    "background-color: #ffffff; "
    "alternate-background-color: #f9f9fa; "
    "border: 1px solid #7f7f7f; "
    "margin-left:10px; "
    "margin-right:10px; "
    "}"
    "QHeaderView::section { "
    "background-color: #7f7f7f; "
    "font: 12px;"
    "font-weight: bold;"
    "border-style: none;"
    "color:#ffffff;"
    "} "
)

TVScrollBarVertical = (
    "QScrollBar:vertical {"
    "            border: 0px solid #999999;"
    "            background: #c7c8ca;"
    "            margin: 0px 0px 0px 0px;"
    "            width: 9px;"
    "        }"
    "QScrollBar::handle:vertical {"
    "           border: 0px solid red;"
    "           border-radius: 4px;"
    "           background-color: #7f7f7f;"
    "        }"
    "QScrollBar::add-line:vertical {"
    "            height: 1px;"
    "            subcontrol-position: bottom;"
    "            subcontrol-origin: margin;"
    "        }"
    "QScrollBar::sub-line:vertical {"
    "            height: 0 px;"
    "            subcontrol-position: top;"
    "            subcontrol-origin: margin;"
    "        }"
)

TVScrollBarHorizontal = (
    "QScrollBar:horizontal {"
    "            border: 0px solid #999999;"
    "            background: #c7c8ca;"
    "            margin: 0px 0px 0px 0px;"
    "            height: 9px;"
    "        }"
    "QScrollBar::handle:horizontal {"
    "           border: 0px solid red;"
    "           border-radius: 4px;"
    "           background-color: #7f7f7f;"
    "        }"
    "QScrollBar::add-line:horizontal {"
    "            height: 1px;"
    "            subcontrol-position: bottom;"
    "            subcontrol-origin: margin;"
    "        }"
    "QScrollBar::sub-line:horizontal {"
    "            height: 0 px;"
    "            subcontrol-position: top;"
    "            subcontrol-origin: margin;"
    "        }"
)

TVProgressBarEnabled = (
    "QProgressBar {"
    "font: 14px;"
    "color: #ffffff;"
    "text-align: center;"
    "background-color: #c7c8ca;"
    "border: 1px #c7c8ca;"
    "border-radius: 8px;"
    "text-align: center;"
    "margin: 5px 5px 5px 5px;"
    "}"
    "QProgressBar::chunk {"
    "background-color: rgba(49, 155, 66, 1);"
    "border-radius: 8px"
    "}"
)

TVProgressBarDisabled = (
    "QProgressBar {"
    "font: 14px;"
    "color: #ffffff;"
    "text-align: center;"
    "background-color: #c7c8ca;"
    "border: 1px #c7c8ca;"
    "border-radius: 8px;"
    "text-align: center;"
    "margin: 5px 5px 5px 5px;"
    "}"
    "QProgressBar::chunk {"
    "background-color: rgba(49, 155, 66, 0.25);"
    "border-radius: 8px"
    "}"
)

TVPushButtonBlue = (
    "QPushButton { "
    # "    background-color: #007cb9; "
    "    background-color: rgb(0, 124, 185);"
    "    border: 1px solid rgb(0, 124, 185); "
    "    color: #ffffff; "
    "    text-align: center; "
    "    font: 14px; "
    "    font-weight: bold;"
    "}"
    "QPushButton::MouseMove { "
    "    background-color: #007cb9;} "
)

TVPushButtonBlueDisabled = (
    "QPushButton { "
    # "    background-color: #007cb9; "
    "    background-color: rgba(0, 124, 185, 75);"
    "    border: 1px solid rgba(0, 124, 185, 75); "
    "    color: #ffffff; "
    "    text-align: center; "
    "    font: 14px; "
    "    font-weight: bold;"
    "}"
    "QPushButton::MouseMove { "
    "    background-color: #007cb9;} "
)

TVPushButtonGray = (
    "QPushButton { "
    "    background-color: #c7c8ca; "
    "    border: 1px solid #c7c8ca; "
    "    color: #ffffff; "
    "    text-align: center; "
    "    font: 14px; "
    "    font-weight: bold;"
    "}"
    "QPushButton::MouseMove { "
    "    background-color: #c7c8ca;} "
    "QPushButton::hover {"
    "     color: rgb(0, 124, 185);"
    "}"
)

TVPushButtonWhite = (
    "QPushButton { "
    "    background-color: #ffffff; "
    "    border: 1px solid #c7c8ca; "
    "    color: #007cb9; "
    "    text-align: center; "
    "    font: 14px; "
    "    font-weight: bold;"
    "}"
    "QPushButton::MouseMove { "
    "    background-color: #c7c8ca;} "
)

TVGroupBox = (
    "QGroupBox {"
    "    border: none;"
    "    font: 16px; "
    "    font-weight: bold;"
    "    padding-top: 16px;"
    "}"
)

TVHelpGroupBox = (
    "QGroupBox {"
    "    border: none;"
    "    font: 20px; "
    "    font-weight: bold;"
    "    padding-top: 16px;"
    "    padding-bottom: 16px;"
    "}"
)

TVLabelHeader = (
    "QLabel { "
    "    border: none;"
    "    font: 16px; "
    "    font-weight: bold;"
    "} "
)

TVHelpLabelHeader = (
    "QLabel { "
    "    border: none;"
    "    font: 24px; "
    "    font-weight: bold;"
    "    color: rgb(0, 124, 185);"
    "    padding-bottom: 35px;"
    "    padding-top: 25px;"
    "} "
)

TVLabelNoBold = (
    "QLabel { "
    "    border: none;"
    "    font: 16px; "
    # "    font-weight: bold;"
    "} "
)

TVLabelInfo = (
    "QLabel { "
    "    border: none;"
    "    font: 16px; "
    # "    font-weight: bold;"
    "} "
)

TVLabelKVLogo = (
    "QLabel { "
    "    border: none;"
    "    font: 12px; "
    "} "
)


TVMainWindow = (
    "QMainWindow { "
    "    border: 1px solid #ffffff; "
    "    background-color: #ffffff; "
    "    } "
)

TVDialogWindow = (
    "QDialog { "
    "    border: 1px solid #ffffff; "
    "    background-color: #ffffff; "
    "    } "
)

TVScrollArea = (
    "QScrollArea { "
    "    border: 1px solid #ffffff;"
    "}"
    "QWidget { "
    "    background-color: #ffffff; "
    "}"

)

TVMenuBar = (
    "QMenuBar { "
    "    border: 1px solid #000000; "
    "    background-color: #000000; "
    "    color: #ffffff; "
    "    font: 14px; "
    # "    font-weight: bold;"
    "    }"
    "QMenuBar::item:pressed {"
    # "    background: #007cb9;"
    "    color: #007cb9; "
    "}"
    "QMenuBar::item:selected  {"
    # "    background: #007cb9;"
    "    color: #007cb9; "
    "}"
    "QMenuBar::item {"
    "    padding: 4px;"
    "    width: 125px;"
    "    margin-left: 20px;"
    "}"
)

TVMenu = (
    "QMenu { "
    "    border: 1px solid #000000; "
    "    background-color: #000000; "
    "    color: #ffffff; "
    "    font: 14px; "
    # "    font-weight: bold;"
    "    }"
    "QMenu::item:pressed {"
    # "    background: #007cb9;"
    "    color: #007cb9; "
    "}"
    "QMenu::item:selected {"
    # "    background: #007cb9;"
    "    color: #007cb9; "
    "}"
    "QMenu::item {"
    "    padding: 10px;"
    "    width: 125px;"
    "    margin-left: 20px;"
    "}"
    "QMenu::separator { "
    "   background-color: #ffffff; "
    "   height: 1px; "
    "}"
)

TVSpinBox = (
    "    QSpinBox {"
    "border: 1px solid #c7c8ca;"
    "font: 14px;"
    "}"
    "   QSpinBox::up-button {"
    "subcontrol-origin: border;"
    "subcontrol-position: top right;"
    "border-width: 0px;"
    "}"
    "   QSpinBox::down-button {"
    "subcontrol-origin: border;"
    "subcontrol-position: bottom right;"
    "border-width: 0px;"
    "}"
    "   QSpinBox::up-arrow {"
    "width: 0px;"
    "height: 0px;"
    "}"
    "   QSpinBox::down-arrow {"
    "width: 0px;"
    "height: 0px;"
    "}"
    "   QSpinBox::up-button:hover {"
    "}"
    "   QSpinBox::up-button:pressed {"
    "}"
    "   QSpinBox::up-arrow:disabled, QSpinBox::up-arrow:off {"
    "}"
    "   QSpinBox::down-button:hover {"
    "}"
    "   QSpinBox::down-button:pressed {"
    "}"
    "   QSpinBox::down-arrow:disabled, QSpinBox::down-arrow:off {"
    "}"
)

TVLineEdit = (
    "   QLineEdit { "
    "border: 1px solid #c7c8ca;"
    "font: 14px;"
    "padding-right: 30px;"
    "}"
)

TVComboBox = (
        "QComboBox {"
        "    border: 1px solid #c7c8ca;"
        "    padding: 1px 18px 1px 3px;"
        "    font: 14px;"
        "}"
        "QComboBox:editable {"
        "    background:  #007cb9;"
        "}"
        "QComboBox:!editable, QComboBox::drop-down:editable {"
        "    background: #ffffff;"
        "}"
        "QComboBox:!editable:on, QComboBox::drop-down:editable:on {"
        "    background: #007cb9;"
        "    color: #ffffff;"
        "}"
        "QComboBox:on { /* shift the text when the popup opens */"
        "    padding-top: 3px;"
        "    padding-left: 4px;"
        "}"
        "QComboBox::drop-down {"
        "    subcontrol-origin: padding;"
        "    subcontrol-position: top right;"
        "    width: 30px;"
        "    border-left-width: 1px;"
        "    border-left-color: #c7c8ca;"
        "    border-left-style: solid; /* just a single line */"
        "}"
        "QComboBox::down-arrow:on { /* shift the arrow when popup is open */"
        "    top: 1px;"
        "    left: 1px;"
        "}"
        "QComboBox::down-arrow "
        "{image: url(%s);"
        "}" % DROP_DOWN_ARROW.replace(os.sep, "/")
)

TVSortableTable = (
    "QTableView { "
    "font: 12px;"
    # "font-weight: bold;"
    "color: #7f7f7f; "
    "background-color: #ffffff; "
    "alternate-background-color: #f9f9fa; "
    "border: 1px solid #7f7f7f; "
    "margin-left:10px; "
    "margin-right:10px;"
    # "padding-right: -7px;"
    "} "
    "QHeaderView::section { "
    "background-color: #7f7f7f; "
    "font: 16px;"
    "font-weight: bold;"
    "border-style: none;"
    "color:#ffffff;"
    "padding-left: 20px;"
    "padding-top: 5px;"
    "} "
)


# QTableView::item{
#     border-top : 1px solid black
#     border-bottom : 1px solid black

TVModel = (
    "QStandardItemModel { "
    "    border: 1px solid gray; "
    "}"
)


TVWidget = " \
QSpinBox { \
    border: 4px solid gray; \
    border-radius: 10px; \
"

TVFrame = " \
QFrame { \
    border: none; \
"

TVSpacer = " \
QSpacerItem { \
    border: 1px solid gray; \
    border-radius: 10px; \
"

TVPlainTextEdit = " \
QLineEdit { \
    border: 1px solid gray; \
    border-radius: 10px; \
"

TVAppStyles = (
    "QMessageBox { "
    "    border: 1px solid #ffffff; "
    "    background-color: #ffffff; "
    "    font: 14px; "
    # "    font-weight: bold;"
    "    } "
    "QPushButton { "
    "    background-color: #007cb9; "
    "    border: 1px solid #007cb9; "
    "    color: #ffffff; "
    "    text-align: center; "
    "    font: 14px; "
    "    font-weight: bold;"
    "    width: 180px;"
    "    height: 30px;"
    "   }"
    "QToolTip { "
    "   color: #ffffff; "
    "   font: 12px;"
    "   background-color: #7f7f7f; "
    "   border: 0px;"
    "   padding: 2px;"
    "}"
)
TVActionToolTip = (
    "QToolTip { "
    "   color: #ffffff; "
    "   font: 12px;"
    "   background-color: #7f7f7f; "
    "   border: 0px;"
    "   padding: 2px;"
    "}"
    "QToolButton {"
    "   background: transparent; "
    "   border: none;"
    "}"
)

TVStatusBar = (
    "QStatusBar {"
    "   font: 12px;"
    "   border: 1px #7f7f7f;"
    "   border-radius: 3px;"
    "}"
)
