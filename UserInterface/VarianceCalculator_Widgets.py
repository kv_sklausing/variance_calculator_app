from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

from UserInterface import VarianceCalculator_Styles as KV_Stylist, VarianceCalculator_Assets


class TVMainWindow(QMainWindow):
    def __init__(self, name='mainWindow'):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)

        maindWindowSizePolicy = QSizePolicy()
        maindWindowSizePolicy.setVerticalPolicy(QSizePolicy.MinimumExpanding)
        maindWindowSizePolicy.setHorizontalPolicy(QSizePolicy.MinimumExpanding)
        maindWindowSizePolicy.setHorizontalStretch(100)
        maindWindowSizePolicy.setVerticalStretch(100)
        maindWindowSizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(maindWindowSizePolicy)


        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVMainWindow)


class TVDialogWindow(QDialog):
    def __init__(self, parent=None, name='dialogWindow'):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)
        self.parent = parent

        self.resize(QSize(680, 480))

        maindWindowSizePolicy = QSizePolicy()
        maindWindowSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        maindWindowSizePolicy.setHorizontalPolicy(QSizePolicy.Fixed)
        maindWindowSizePolicy.setHorizontalStretch(100)
        maindWindowSizePolicy.setVerticalStretch(100)
        maindWindowSizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(maindWindowSizePolicy)

        self.setMinimumSize(QSize(680, 480))

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVDialogWindow)


class TVMessageBox(QMessageBox):
    def __init__(self, parent=None, name='messageBox'):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)
        self.parent = parent

        self.resize(600, 250)

        maindWindowSizePolicy = QSizePolicy()
        maindWindowSizePolicy.setVerticalPolicy(QSizePolicy.Fixed)
        maindWindowSizePolicy.setHorizontalPolicy(QSizePolicy.Fixed)
        maindWindowSizePolicy.setHorizontalStretch(100)
        maindWindowSizePolicy.setVerticalStretch(100)
        maindWindowSizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(maindWindowSizePolicy)

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVAppStyles)


class TVPushButton(QPushButton):
    def __init__(self, parent=None, name='pushButton', text='ButtonGridTest'):
        # Initialize the parent classes.
        super().__init__()

        self.parent = parent
        self.name = name
        self.pushButtonText = text

        self.setObjectName(self.name)
        self.setText(self.pushButtonText)

        self.groupBoxSizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.groupBoxSizePolicy.setHorizontalStretch(100)
        self.groupBoxSizePolicy.setVerticalStretch(100)
        self.groupBoxSizePolicy.setHeightForWidth(True)

        self.setSizePolicy(self.groupBoxSizePolicy)

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVPushButtonBlue)


class TVSpinBox(QSpinBox):
    def __init__(self, parent=None, name='spinBox', value=0, maxVal=1000000, minVal=0):
        # Initialize the parent classes.
        super().__init__()

        self.parent = parent
        self.name = name

        self.setObjectName(self.name)

        self.setMaximum(maxVal)
        self.setMinimum(minVal)
        self.setValue(value)

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVSpinBox)

        return


class TVLineEdit(QLineEdit):
    def __init__(self, parent=None, name='lineEdit', icon=None):
        # Initialize the parent classes.
        super().__init__()

        self.parent = parent
        self.name = name
        self.setObjectName(self.name)

        self.lineEditIcon = None
        if icon:
            self.add_icon(icon)

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVLineEdit)

    def add_icon(self, iconPath):
        self.lineEditIcon = QToolButton(self)
        self.lineEditIcon.setCursor(Qt.PointingHandCursor)

        self.lineEditIcon.setFocusPolicy(Qt.NoFocus)
        self.lineEditIcon.setIcon(QIcon(iconPath))
        self.lineEditIcon.setStyleSheet("background: transparent; border: none;")

        # https://stackoverflow.com/questions/12462562/how-to-insert-a-button-inside-a-qlineedit
        frameWidth = self.style().pixelMetric(QStyle.PM_DefaultFrameWidth)
        buttonSize = self.lineEditIcon.sizeHint()
        # self.setStyleSheet('QLineEdit {padding-right: %dpx; }' % (buttonSize.width() + frameWidth + 1))

        layout = QHBoxLayout(self)
        layout.addWidget(self.lineEditIcon, 0, Qt.AlignRight)

        # layout.setSpacing(200)
        # layout.setMargin(0)
        # self.lineEditIcon.setToolTip('Reset Selection')
        return


class TVComboBox(QComboBox):
    def __init__(self, name='comboBox', items=None):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)
        # for item in items:
        #     self.addItem(item)

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVComboBox)


class TVAction(QAction):
    def __init__(self, name='action', text=None):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)

        if text is not None:
            self.setText(text)

        self.setFont(QFont('Helvetica Neue'))


class TVWidget(QWidget):
    def __init__(self, parent=None, name='widget', actionItems=False, sortItems=None):
        # Initialize the parent classes.
        super().__init__()

        self.parent = parent
        self.name = name
        self.setObjectName(self.name)
        # self.setStyleSheet(KV_Stylist.TVWidget)

        if actionItems:

            self.actionEditButton = self.create_action_items(VarianceCalculator_Assets.ACTION_EDIT, 'Edit Parameters')
            self.actionOpenButton = self.create_action_items(VarianceCalculator_Assets.ACTION_OPEN_OUTPUT_DIR, 'Open Output Directory')
            self.actionDeleteButton = self.create_action_items(VarianceCalculator_Assets.ACTION_DELETE, 'Remove From Queue')
            self.actionDisplayButton = self.create_action_items(VarianceCalculator_Assets.ACTION_OPEN_OUTPUT_DIR, 'Display Mesh')

            layout = QHBoxLayout(self)
            layout.addWidget(self.actionEditButton, 0)  # , Qt.AlignLeft)
            layout.addWidget(self.actionOpenButton, 0)  # , Qt.AlignCenter)
            layout.addWidget(self.actionDeleteButton, 0)  # , Qt.AlignRight)
            layout.addWidget(self.actionDisplayButton, 0)
            layout.setSpacing(0)
            layout.setMargin(0)

        if sortItems:

            self.sortDragButton = self.create_action_items(VarianceCalculator_Assets.ACTION_DRAG_ICON, 'Drag')
            self.sortDeleteButton = self.create_action_items(VarianceCalculator_Assets.ACTION_DELETE, 'Remove')

            layout = QHBoxLayout(self)
            layout.addWidget(self.sortDragButton, 0)  # , Qt.AlignLeft)
            layout.addWidget(self.sortDeleteButton, 0)  # , Qt.AlignRight)
            layout.setSpacing(0)
            layout.setMargin(0)

        self.setFont(QFont('Helvetica Neue'))

    def create_action_items(self, iconPath, toolTip):
        # frameWidth = cellItem.style().pixelMetric(QStyle.PM_DefaultFrameWidth)
        # buttonSize = cellItem.sizeHint()
        actionItemsButton = QToolButton()
        actionItemsButton.setCursor(Qt.PointingHandCursor)
        actionItemsButton.setFocusPolicy(Qt.NoFocus)
        actionItemsButton.setIcon(QIcon(iconPath))
        actionItemsButton.setStyleSheet(KV_Stylist.TVActionToolTip)
        # https://stackoverflow.com/questions/12462562/how-to-insert-a-button-inside-a-qlineedit
        # actionItemsButtonEdit.setStyleSheet('QLineEdit {padding-right: %dpx; }' % (buttonSize.width() + frameWidth + 1))
        actionItemsButton.setToolTip(toolTip)
        return actionItemsButton


class TVGroupBox(QGroupBox):
    def __init__(self, parent=None, name='groupBox_01', text='None'):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)

        self.setAlignment(Qt.AlignLeft)
        # self.setFlat(True)
        self.setTitle(text)

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVGroupBox)


class TVFrame(QFrame):
    def __init__(self, name='groupBox_01'):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)

        self.setFont(QFont('Helvetica Neue'))
        # self.setStyleSheet(KV_Stylist.TVFrame)



class TVTableWidget(QTableWidget):
    def __init__(self, name='table'):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)

        self.setAlternatingRowColors(True)
        self.setShowGrid(False)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.verticalHeader().setVisible(False)
        self.setFont(QFont('Helvetica Neue'))

        self.scrollBarVert = self.verticalScrollBar()
        # self.scrollBarVert.setMaximumWidth(10)
        self.scrollBarVert.setStyleSheet(KV_Stylist.TVScrollBarVertical)
        self.setStyleSheet(KV_Stylist.TVTableWidget)


class TVMenuBar(QMenuBar):
    def __init__(self, name='menuBar'):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVMenuBar)



class TVMenu(QMenu):
    def __init__(self, name='menu', text=None):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)

        if text is not None:
            self.setTitle(text)

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVMenu)

class TVSpacer(QSpacerItem):
    def __init__(self, name='spacer'):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)
        # self.setStyleSheet(KV_Stylist.TVSpacer)


class TVPlainTextEdit(QPlainTextEdit):
    def __init__(self, name='plainText', enableBool=False):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)
        self.setEnabled(enableBool)
        # self.setStyleSheet(KV_Stylist.TVPlainTextEdit)


class TVLabel(QLabel):
    def __init__(self, parent=None, name='label_text_01', text='None'):
        # Initialize the parent classes.
        super().__init__()

        self.parent = parent
        self.name = name
        self.setObjectName(self.name)
        self.setText(text)

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVLabelHeader)


class TVModel(QStandardItemModel):

    def dropMimeData(self, data, action, row, col, parent):
        """
        Always move the entire row, and don't allow column "shifting"
        """

        response = super().dropMimeData(data, action, row, 0, parent)

        # TESTING
        # if row == -1:   #Drop after last row
        #     row = self.rowCount()-1
        # item = self.item(row, 0)
        # item.setEnabled(self.was_enabled) # Hack// Fixes copying instead of moving when style column is disabled
        return response

    def mimeData(self, indices):
        """
        Move all data, including hidden/disabled columns
        """
        index = indices[0]
        new_data = []

        for col in range(self.columnCount()):
            new_data.append(index.sibling(index.row(), col))

        # item = self.item(index.row(), 0)
        # self.was_enabled = item.isEnabled()
        # item.setEnabled(True)  # Hack// Fixes copying instead of moving when item is disabled

        return super().mimeData(new_data)


class TVProxyStyle(QProxyStyle):

    def drawPrimitive(self, element, option, painter, widget=None):
        """
        Draw a line across the entire row rather than just the column
        we're hovering over.  This may not always work depending on global
        style - for instance I think it won't work on OSX.
        """
        if element == self.PE_IndicatorItemViewItemDrop and not option.rect.isNull():
            option_new = QStyleOption(option)
            option_new.rect.setLeft(0)
            if widget:
                option_new.rect.setRight(widget.width())
            option = option_new
        super().drawPrimitive(element, option, painter, widget)


class TVSortableTable(QTableView):

    def __init__(self, name):
        super().__init__()

        self.name = name

        # Get Header Info
        self.verticalHeader().hide()
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)
        self.horizontalHeader().setStretchLastSection(False)

        self.setSelectionBehavior(self.SelectRows)
        self.setSelectionMode(self.SingleSelection)
        self.setDragDropMode(self.InternalMove)
        self.setDragDropOverwriteMode(False)

        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        self.viewport().setAcceptDrops(True)
        self.setDragDropOverwriteMode(False)
        self.setDropIndicatorShown(True)

        # self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        # self.setSelectionBehavior(QAbstractItemView.SelectRows)
        # self.setDragDropMode(QAbstractItemView.InternalMove)

        self.setAlternatingRowColors(True)
        self.setShowGrid(False)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.verticalHeader().setVisible(False)
        self.setFont(QFont('Helvetica Neue'))

        self.scrollBarVert = self.verticalScrollBar()
        self.scrollBarVert.setStyleSheet(KV_Stylist.TVScrollBarVertical)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        self.scrollBarHorz = self.horizontalScrollBar()
        self.scrollBarHorz.setStyleSheet(KV_Stylist.TVScrollBarHorizontal)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        self.setStyleSheet(KV_Stylist.TVSortableTable)

        # Set our custom style - this draws the drop indicator across the whole row
        self.setStyle(TVProxyStyle())

        # Set our custom model - this prevents row "shifting"
        self.model = TVModel()
        self.setModel(self.model)

class TVProgressBar(QProgressBar):
    def __init__(self, name='progressBar', value=0):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)

        self.setAlignment(Qt.AlignCenter)
        self.setValue(value)

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVProgressBarEnabled)

class TVScrollArea(QScrollArea):
    def __init__(self, parent=None, name='progressBar', value=0):
        # Initialize the parent classes.
        super().__init__()

        self.name = name
        self.setObjectName(self.name)

        self.setAlignment(Qt.AlignCenter)

        self.setFont(QFont('Helvetica Neue'))
        self.setStyleSheet(KV_Stylist.TVScrollArea)


