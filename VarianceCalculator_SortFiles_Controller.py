'''
Main Sort Files Control for VarianceCalculator.py.
'''

import os
from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon, QStandardItem

import Utilities as VarianceUtilities

from UserInterface.Windows.VarianceCalculator_SortFiles_Window import VarianceVisionSortFilesGUI
from UserInterface.VarianceCalculator_Assets import ACTION_DRAG_ICON


class SortFilesController(VarianceVisionSortFilesGUI):
    '''
    Class for the VAR App Sort window.
    '''

    def __init__(self, parent=None, confirmedPairsSignal=None, iconPath32=None, mainLogger=None):
        # Initialize the parent classes.
        super().__init__()

        self.parent = parent
        self.icon_path = iconPath32

        # Initialize window properties.
        self.init_window()

        # Initialize widget actions.
        self.init_widget_actions()

        # Variables assigned out of controller
        self.TestFiles = None
        self.RefFiles = None

        self.confirmedPairsSignal = confirmedPairsSignal

        self.mainLogger = mainLogger

        return

    def init_window(self):
        # Set some options for the window.
        self.setWindowTitle('Sort Mesh Files')
        self.setWindowIcon(QIcon(self.icon_path))
        self.setFixedSize(self.size())
        self.setWindowModality(Qt.ApplicationModal)

        return

    def init_widget_actions(self):
        # Connect the widgets to their actions.
        self.pushButtonConfirm.clicked.connect(
            self.close_confirmed
        )

        self.RefScrollBarVert.valueChanged.connect(
            self.synchronize_scrollbars
        )

    def synchronize_scrollbars(self):
        RefValueVert = self.RefScrollBarVert.value()
        self.TestScrollBarVert.setValue(RefValueVert)

        return

    def fill_sortable_table(self):

        colNumTest = 0
        self.TestFileDict = dict()
        for rowNum, TestMeshFile in enumerate(self.TestFiles):
            TestFileName = os.path.basename(TestMeshFile)
            self.TestFileDict[TestFileName] = TestMeshFile

            # Get Action Items
            # actionItemsWidget = VarianceCalculator_Widgets.TVWidget(name='actionItemsWidget', sortItems=True)
            # actionItemsEdit = actionItemsWidget.sortDragButton
            # actionItemsDelete = actionItemsWidget.sortDeleteButton
            # TestModelIdx = self.TestModel.index(rowNum, 0)  # QTableView  # QTableView (not keeping on drag)
            # self.tableTestSort.setIndexWidget(TestModelIdx, actionItemsWidget)  # QTableView (not keeping on drag)
            # self.tableTestSort.setCellWidget(rowNum, 0, actionItemsWidget)  # QTableWidget

            # Get Row with Filenames
            rowItem = QStandardItem()  # QTableView
            rowItem.setEditable(False)  # QTableView
            rowItem.setDropEnabled(False)  # QTableView
            # rowItem = QTableWidgetItem()  # QTableWidget
            rowItem.setText(TestFileName)
            rowItem.setToolTip(TestMeshFile)
            rowItem.setIcon(QIcon(ACTION_DRAG_ICON))
            rowItem.setTextAlignment(Qt.AlignRight)
            self.TestModel.setItem(rowNum, 0, rowItem)  # QTableView
            # self.tableTestSort.setItem(rowNum, 1, rowItem)  # QTableWidget

        colNumRef = 0
        self.RefFileDict = dict()
        for rowNum, RefMeshFile in enumerate(self.RefFiles):
            RefFileName = os.path.basename(RefMeshFile)
            self.RefFileDict[RefFileName] = RefMeshFile

            # Get Action Items
            # actionItemsWidget = VarianceCalculator_Widgets.TVWidget(name='actionItemsWidget', sortItems=True)
            # actionItemsEdit = actionItemsWidget.sortDragButton
            # actionItemsDelete = actionItemsWidget.sortDeleteButton
            # RefModelIdx = self.RefModel.index(rowNum, 0) # QTableView (not keeping on drag)
            # self.tableRefSort.setIndexWidget(RefModelIdx, actionItemsWidget)  # QTableView (not keeping on drag)
            # self.tableRefSort.setCellWidget(rowNum, 0, actionItemsWidget)  # QTableWidget

            # Get Row with Filenames
            rowItem = QStandardItem()  # QTableView
            rowItem.setEditable(False)  # QTableView
            rowItem.setDropEnabled(False)  # QTableView
            # rowItem = QTableWidgetItem()  # QTableWidget
            rowItem.setText(RefFileName)
            rowItem.setToolTip(RefMeshFile)
            rowItem.setIcon(QIcon(ACTION_DRAG_ICON))
            rowItem.setTextAlignment(Qt.AlignRight)
            self.RefModel.setItem(rowNum, 0, rowItem)  # QTableView
            # self.tableRefSort.setItem(rowNum, 1, rowItem)  # QTableWidget

        # self.tableTestSort.resizeColumnsToContents()
        # self.tableRefSort.resizeColumnsToContents()

        return

    def close_confirmed(self):
        # self.store_window_settings()
        self.confirmedPairsSignal.emit()
        self.close()
        return

