'''
Help UI
'''

from PySide2.QtGui import QIcon, QPixmap

from UserInterface.VarianceCalculator_HelpRichText import *
from UserInterface.Windows.VarianceCalculator_Help_Window import Ui_VarianceCalculator_Help

from UserInterface.VarianceCalculator_Assets import *

APP_ICONS_LIST = (KV_FONT_PATH, KV_FONT_PATH_BOLD)


class VarianceCalculatorHelpController(Ui_VarianceCalculator_Help):
    def __init__(self, parent, appIcon32=None,
                 dashboardEmpty=None,
                 selectFilesSingle=None, filesAddedSingle=None,
                 selectFilesMulti=None, filesAddedMulti=None,
                 confirmFiles=None,
                 dashboardProcessing=None,
                 helpWindowSize=None):

        # Initialize the parent classes.
        super().__init__()

        self.parent = parent

        self.appIcon32 = appIcon32
        self.dashboardEmpty = dashboardEmpty
        self.selectFilesSingle = selectFilesSingle
        self.filesAddedSingle = filesAddedSingle
        self.selectFilesMulti = selectFilesMulti
        self.filesAddedMulti = filesAddedMulti
        self.confirmFiles = confirmFiles
        self.dashboardProcessing = dashboardProcessing
        self.helpWindowSize = helpWindowSize

        self.init_window()

        # Set Window Images
        self.set_help_window_images()

        # Set Window Headers
        self.set_help_window_headers()

        # Set Window Descriptions
        self.set_help_window_descriptions()

        # Initialize widget actions.
        self.init_widget_actions()

        return

    def init_window(self):
        # Set some options for the window.
        self.setWindowIcon(QIcon(self.appIcon32))
        self.setWindowModality(Qt.NonModal)
        if self.helpWindowSize is not None:
            self.resize(self.helpWindowSize)
        return

    def init_widget_actions(self):
        # Connect the widgets to their actions.
        self.pushButtonClose.clicked.connect(
            self.close
        )

    def set_help_window_images(self):

        if self.appIcon32 is not None:
            self.setWindowIcon(QIcon(self.appIcon32))

        if self.dashboardEmpty is not None:
            self.dashboardEmptyPixmap = QPixmap(self.dashboardEmpty)
            self.dashboardEmptyPixmap = self.dashboardEmptyPixmap.scaled(self.label_img_01.width(),
                                                                         self.label_img_01.height(),
                                                                         Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.label_img_01.setPixmap(self.dashboardEmptyPixmap)

        if self.selectFilesSingle is not None:
            self.selectFilesSinglePixmap = QPixmap(self.selectFilesSingle)
            self.selectFilesSinglePixmap = self.selectFilesSinglePixmap.scaled(self.label_img_02.width(),
                                                                               self.label_img_02.height(),
                                                                               Qt.KeepAspectRatio,
                                                                               Qt.SmoothTransformation)
            self.label_img_02.setPixmap(self.selectFilesSinglePixmap)

        if self.filesAddedSingle is not None:
            self.filesAddedSinglePixmap = QPixmap(self.filesAddedSingle)
            self.filesAddedSinglePixmap = self.filesAddedSinglePixmap.scaled(self.label_img_03.width(),
                                                                             self.label_img_03.height(),
                                                                             Qt.KeepAspectRatio,
                                                                             Qt.SmoothTransformation)
            self.label_img_03.setPixmap(self.filesAddedSinglePixmap)

        if self.selectFilesMulti is not None:
            self.selectFilesMultiPixmap = QPixmap(self.selectFilesMulti)
            self.selectFilesMultiPixmap = self.selectFilesMultiPixmap.scaled(self.label_img_04.width(),
                                                                             self.label_img_04.height(),
                                                                             Qt.KeepAspectRatio,
                                                                             Qt.SmoothTransformation)
            self.label_img_04.setPixmap(self.selectFilesMultiPixmap)

        if self.filesAddedMulti is not None:
            self.filesAddedMultiPixmap = QPixmap(self.filesAddedMulti)
            self.filesAddedMultiPixmap = self.filesAddedMultiPixmap.scaled(self.label_img_05.width(),
                                                                           self.label_img_05.height(),
                                                                           Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.label_img_05.setPixmap(self.filesAddedMultiPixmap)

        if self.confirmFiles is not None:
            self.confirmFilesPixmap = QPixmap(self.confirmFiles)
            self.confirmFilesPixmap = self.confirmFilesPixmap.scaled(self.label_img_06.width(),
                                                                     self.label_img_06.height(),
                                                                     Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.label_img_06.setPixmap(self.confirmFilesPixmap)

        if self.dashboardProcessing is not None:
            self.dashboardProcessingPixmap = QPixmap(self.dashboardProcessing)
            self.dashboardProcessingPixmap = self.dashboardProcessingPixmap.scaled(self.label_img_07.width(),
                                                                                   self.label_img_07.height(),
                                                                                   Qt.KeepAspectRatio,
                                                                                   Qt.SmoothTransformation)
            self.label_img_07.setPixmap(self.dashboardProcessingPixmap)

        return

    def set_help_window_headers(self):

        self.groupBox_01.setTitle('Dashboard (Empty)')

        self.groupBox_02.setTitle('Select Files (Single)')

        self.groupBox_03.setTitle('Files Added (Single)')

        self.groupBox_04.setTitle('Select Files (Multiple)')

        self.groupBox_05.setTitle('Files Added (Multiple)')

        self.groupBox_06.setTitle('Confirm Files')

        self.groupBox_07.setTitle('Dashboard (Running)')

        return

    def set_help_window_descriptions(self):

        # <span style =\" font-size:14pt; font-weight:bold;\">example</span>
        # <span style=\" text-decoration: underline; color:#ff5500;\">text</span>

        self.label_text_01.setText(helpDashboardEmptyText)

        self.label_text_02.setText(helpSelectFilesSingleText)

        self.label_text_03.setText(helpSelectFilesSingleAdded)

        self.label_text_04.setText(helpSelectFilesMulti)

        self.label_text_05.setText(helpSelectFilesMultiAdded)

        self.label_text_06.setText(helpConfirmFiles)

        self.label_text_07.setText(helpDashboardProcessing)

        return

    def closeEvent(self, event):
        # Get latest settings and save settings to file.
        self.helpWindowSize = self.size()
        return