# -*- coding: utf-8 -*-
"""
Kinetic Vision
Scott Klausing

Selects the geomagic template macro, replaces inputs and then runs
subprocess to execute the macro with the geomagic core application.
"""

import os
import sys
import subprocess
import traceback

# Define module constants.
# Get info about this file.

if getattr(sys, 'frozen', False):
    # Set path for UI Assets
    SCRIPT_INTERPRETER_DIR = os.path.dirname(sys.executable)
    GEOMAGIC_DIR = os.path.join(SCRIPT_INTERPRETER_DIR, 'Geomagic')
else:
    # Set path for UI Assets
    GEOMAGIC_DIR = os.path.dirname(__file__)

BIN_DIR = os.path.join(GEOMAGIC_DIR, 'bin')

VAR_CAD_REF_PTS_TEMPLATE = os.path.join(BIN_DIR, 'VarianceCalculator_Geo_Template_Cad_Ref_Pts.py3')
# VAR_GEO_COMPARE_TEMPLATE = os.path.join(BIN_DIR, 'VarianceCalculator_Geo_Template_Var_Analysis.py3')


def get_files(directory, dataTypeList, contains=None, location='any'):  # location = 'any' or 'end'
    """Summary of function.

    Returns a list of files found based on the the search directory, type of file given and a specific end string (if any).

    Args:
        directory: search directory where files should be found
        dataTypeList: type = list, file extensions for valid files desired
        contains: type = string, a string that is in the files you want, identifier
        locatoin: type = string, 'any' used for the contain string to be anywhere in the file, 'end' to have the file specifically end with the string

    Returns:
        A list of files found

    Raises:
        No raises, returns None if no files found based on input arguments

    Example Usage:
        get_files(input_directory, ['.stl'], endsWithStr='Baseline')
    """

    print("\nSearching for %s files in directory:\n%s" % (str(dataTypeList), directory))

    # Filter for finding valid file
    def data_filter(file):
        fileNameNoExt, fileExt = os.path.splitext(file)
        if fileExt in dataTypeList:
            if contains != None:
                if location == 'end':
                    goodFile = fileNameNoExt.endswith(contains)
                elif location == 'any':
                    if contains in fileNameNoExt:
                        goodFile = True
                    else:
                        goodFile = False
            else:
                goodFile = True
        else:
            goodFile = False
        return goodFile

    # Create list of all files in directory
    if not os.path.isdir(directory):  # check input
        print('Directory does not exist.\n%s' % (directory))
        return None
    else:
        all_files = os.listdir(directory)  # get all files in directory
        data_files = list(filter(data_filter, all_files))  # filter out everything that isnt a valid data file
        data_files = [os.path.join(directory, validData) for validData in data_files]  # create full file path

        if len(data_files) == 0:  # error if no valid data types
            print('No valid files %s in directory.\n%s' % (str(dataTypeList), directory))
            return None

    print("Found %i %s files in directory." % (len(data_files), str(dataTypeList)))

    return data_files


def valid_dirs_in_directory(directory):
    """Summary of function.

    Function that finds valid folders within a given directory

    Args:
        directory: Path to a directory

    Returns:
        list of file paths to a valid directories found

    Raises:
        Returns empty if input direcotry is None

    Example Usage:
        (optional, but recommended; write an example line calling the function)
    """
    dirs_in_directory = []
    if directory == None:
        return directory
    else:
        directoryitems = os.listdir(directory)
        for item in directoryitems:
            tempdir = os.path.join(directory, item)
            if os.path.isdir(tempdir):
                dirs_in_directory.append(tempdir)
            else:
                pass
    return (dirs_in_directory)


# Select and edit template macro file based on inputs
def edit_template(geoMacroFilePath, importCadFile, numOfPts, ptsSaveFile, exportUnits):

    # Open file and read all lines
    print("Opening macro file to edits inputs:\n%s" % geoMacroFilePath)
    try:
        with open(geoMacroFilePath, 'r') as fileObject:
            allLines = fileObject.readlines()
    except:
        print("Error opening marco file:\n%s\n%s" % (sys.exc_info()[0], sys.exc_info()[1]))
        return None

    # Create dictionary for finding expected inputs
    # key = str: input name to find in macro file and replace
    # value = [boolean, inputVariable]
    #   boolean: used for keeping track of inputs found
    #   inputVariable: is value passed to function
    inputDict = dict([
        ('cadFile', [False, importCadFile]),
        ('numOfPts', [False, numOfPts]),
        ('savePts', [False, ptsSaveFile]),
        ('exportUnits', [False, exportUnits])
    ])

    # Loop through lines and find the lines marked for replacement
    lineIndex = -1
    replacementLines = []
    for line in allLines:
        # Keep track of current line index
        lineIndex += 1

        # Get list of booleans for inputs found
        inputsFound = [value[0] for value in inputDict.values()]

        # Stop searching lines if all inputs found
        if all(inputsFound):
            break

        # Remove end of line charcter
        line = line.strip('\n')

        # Check line start
        startsWithIsValid = line.startswith("<>")
        if not startsWithIsValid:
            continue

        # Check line end
        endsWithIsValid = line.endswith("<>")
        if not endsWithIsValid:
            continue

        # If it gets to here both start and end are valid
        line = line[2:-2]  # remove <> from start/end

        # Find expected inputs and process
        for key, value in inputDict.items():
            if key in line:
                inputDict[key] = [True, value[1]]  # Update dictionary
                if isinstance(value[1], str):
                    replacementText = "%s = r\"%s\" \n" % (key, value[1])
                else:
                    replacementText = "%s = %s \n" % (key, value[1])
                replacementLines.append([lineIndex, replacementText])
                continue

    # Replace lines in template file
    for item in replacementLines:
        allLines[item[0]] = item[1]

    # Save edited template to new path
    templateBaseName = os.path.splitext(os.path.basename(importCadFile))[0]
    templateSaveDir = os.path.join(os.path.dirname(importCadFile), 'geo_scratch')
    if not os.path.isdir(templateSaveDir):
        os.makedirs(templateSaveDir)

    templateFileName = ("%s-%s_Geo_Template.py3" % (templateBaseName, str(numOfPts)))
    templateFileSaveAs = os.path.join(templateSaveDir, templateFileName)
    try:
        with open(templateFileSaveAs, 'w+') as filObject:
            filObject.writelines(allLines)
    except:
        print("Error saving modified macro file:\n%s\n%s" % (sys.exc_info()[0], sys.exc_info()[1]))
        return None

    print("Modified macro file successfully created:\n%s" % templateFileSaveAs)

    return templateFileSaveAs


# Run the geomagic application from the command line as a subprocess
def run_geomagic_app(geomagicExePath, geoMacroFilePath):
    """

    Args:
        geomagicExePath:
        geoMacroFilePath:

    Returns:

    """
    print("Running geomagic application to process script:\n%s" % geoMacroFilePath)

    # Get the path and name of the current macro file
    currentFilePath = os.path.dirname(geoMacroFilePath)
    currentFileName = os.path.basename(geoMacroFilePath)

    # Create log file path
    logFileName = "%s_LogFile.txt" % (currentFileName.rsplit(".", 1)[0])
    logFilePath = os.path.join(currentFilePath, logFileName)

    # Check to see if the geomagic exe is CORE (guiless)
    geoExeName = os.path.basename(geomagicExePath)
    isCORE = "CORE" in geoExeName

    # Set up the command line to run geomagic based on exe
    # <path to Geomagic Wrap>/studio.exe <path to script>/scriptname.py -ExitOnMacroEnd
    cmdline = ("\"%s\" \"%s\" -ExitOnMacroEnd > \"%s\"") % (geomagicExePath, geoMacroFilePath, logFilePath)
    # cmdline = ("\"%s\" \"%s\" > \"%s\"") %(geomagicExePath, geoMacroFilePath, logFilePath)

    # Try and run geomagic application
    try:
        subprocess.call(cmdline, shell=True)
    except:
        print("Error executing geomagic script:\n%s\n%s" % (sys.exc_info()[0], sys.exc_info()[1]))
        return False

    print("Successfully executed geomagic application script.")

    return True


def main(importCadFile, numOfPts, outputPtFile, geomagicExe, exportUnits, progressSignal=None):

    if progressSignal is not None:
        progressSignal.emit(30, 'None')
    geoMacroCadSample = edit_template(VAR_CAD_REF_PTS_TEMPLATE, importCadFile, numOfPts, outputPtFile, exportUnits)
    if geoMacroCadSample is None:
        return

    if progressSignal is not None:
        progressSignal.emit(45, 'None')

    print("\nExecuting geomagic_process to obtain CAD Pts for Ref...")
    status = run_geomagic_app(geomagicExe, geoMacroCadSample)

    if progressSignal is not None:
        progressSignal.emit(60, 'None')

    print("Finished geomagic_process.")

    return


# Execute
if __name__ == '__main__':

    # Test execution
    cadFile = r"C:\_Data\PG\Variance_Files\QB04374\00001429714-001 Mercury_BO_800ml_SC27-6_ISBM (2)_rot_CAD_Aligned.stp"
    numberOfPts = 60000
    geomagicExe = r"C:\Program Files\3D Systems\Geomagic Wrap 2017\wrapCORE.exe"
    exportUnits = 'Inches'
    outputFile = r"C:\_Data\PG\Variance_Files\QB04374\00001429714-001 Mercury_BO_800ml_SC27-6_ISBM (2)_rot_CAD_Aligned-%s-%s.asc" % (str(numberOfPts), exportUnits)
    main(cadFile, numberOfPts, outputFile, geomagicExe, exportUnits)
