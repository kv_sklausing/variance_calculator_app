import geomagic.app.v3
from geomagic.app.v3.imports import *
import pickle
import os
import sys


################################################  Inputs  ################################################
<>cadPtsSampled<>
<>cadAlignedFile<>
<>meshFile<>

################################################  Functions  ################################################
<>reshiftSaveMesh<>
<>saveCompPickle<>


def save_pts_file(ptsObject, ptsFile):
    # create write object
    filewriter = WriteFile()
    writeOptions = FileSaveOptions()
    writeOptions.units = Length.Millimeters
    filewriter.options = writeOptions
    # set path
    filewriter.filename = ptsFile  # r"C:\_Data\PG\Variance_Files\QB02502\0900A011_REV_C_rot_60k_Pts.asc"
    # set mesh within filewrite object
    filewriter.points = ptsObject
    # write the file
    filewriter.run()
    return


def save_mesh_file(meshObject, meshFile):
    # create write object
    filewriter = WriteFile()
    writeOptions = FileSaveOptions()
    writeOptions.units = Length.Millimeters
    filewriter.options = writeOptions
    # set path
    filewriter.filename = meshFile
    # set mesh within filewrite object
    filewriter.mesh = meshObject
    # write the file
    filewriter.run()
    return


def load_mesh_and_reshift(alignedCadFile, shiftedMeshFile, meshSaveFile, fillHoles=False):
    # Load Aligned CAD and get BBox
    cadFileLoader = ReadCADFromFile()
    cadFileLoader.fileName = alignedCadFile
    cadFileLoader.healCAD = 0
    cadFileLoader.run()
    cadModelLoaded = cadFileLoader.cadModel
    cadModelLoadedMesh = cadModelLoaded.mesh
    cadAlignedBoundingBox = cadModelLoadedMesh.boundingBox
    xMax, yMax, zMax = cadModelLoadedMesh.maxBoxCoord
    xMin, yMin, zMin = cadModelLoadedMesh.minBoxCoord
    print(zMin)
    print(zMax)

    # Load Mesh Object
    # create ReadFile object
    openOptions = FileOpenOptions()
    openOptions.units = Length.Millimeters

    reader = ReadFile()
    reader.filename = shiftedMeshFile
    reader.options = openOptions
    reader.run()
    mesh = reader.mesh

    if fillHoles:
        # Fill all holes with Flat method
        filler = FillAllHoles(mesh)
        filler.type = FillAllHoles.Flat
        filler.maxHoleLength = 1000.0
        filler.run()  # exclude the largest one

    # Transform the mesh back to original best-fit alignment
    # (Outside_Wrapped file was shifted in Bottle Alignment step to z=0, cad shift same amount)
    # (this step reshifts the sample back to original best-fit alignment so comparison to CAD pts is same across samples)
    trans = Transform3D()
    trans.setTranslation(Vector3D(0, 0, zMin*(-1)))
    mesh.transform(trans)

    # meshSaveFile = r"C:\_Data\PG\Variance_Files\QB02502\20160923_QB02502_GFHC_Europe_Fairy_900_ml_PET_Pearl-1_4\20160923_QB02502_GFHC_Europe_Fairy_900_ml_PET_Pearl-1_4_Outside_Wrapped-Reshift.stl"
    save_mesh_file(mesh, meshSaveFile)
    # # Add points object to application and name the model samplePoints.
    # filledMeshModel = geoapp.addModel(mesh, u"filledMeshModel")

    # # perform transform on aligned CAD to check z=0 bbox after
    # cadModelLoaded.transform(trans)
    # # Add points object to application and name the model samplePoints.
    # transCADModel = geoapp.addModel(cadModelLoaded, u"transCADModel")

    return mesh


def compare_cad_pts_and_mesh(comparisonPts, testMesh):
    # Compare points in p against the mesh m
    compare = ComparePointsToMesh(testMesh)
    compare.points = comparisonPts
    compare.maxDeviation = 0.01
    # compare.spacing = 0.25
    compare.comparisonType = compare.ComparisonType_Regular3D
    compare.algorithmType = compare.AlgorithmType_Dense
    compare.criticalAngle = 45.0
    compare.run()
    compareResults = compare.comparisonResult
    return compareResults


def main_process(cadPtsSampledFile, cadAlignedFile, meshFile, reshiftSaveMesh, saveCompPickle):

    # Read 60k Pts to Compare to
    openOptions = FileOpenOptions()
    openOptions.units = Length.Millimeters
    
    reader = ReadFile()
    reader.filename = cadPtsSampledFile
    reader.options = openOptions
    reader.run()
    cadPtsSampled = reader.points
    
    reshiftedMesh = load_mesh_and_reshift(cadAlignedFile, meshFile, reshiftSaveMesh)
    compResults = compare_cad_pts_and_mesh(cadPtsSampled, reshiftedMesh)

    compareDict = dict()
    comparedPts = compResults.comparedPointSelection
    print('Iterating Results...')
    for i in comparedPts:
       pt2 = compResults.getPoint(i)
       mpt2proj = compResults.getMeshPoint(i)
       dist_2 = pt2.dist(mpt2proj.point)
       distance = compResults.getDistance(i) * (-1)
       ptCoords = compResults.getPoint(i)
       compareDict[i] = {'dist1': distance,
                         'dist2': dist_2,
                         'ref pt x': ptCoords[0],
                         'ref pt y': ptCoords[1],
                         'ref pt z': ptCoords[2],
                         'mesh pt x': mpt2proj.point[0],
                         'mesh pt y': mpt2proj.point[1],
                         'mesh pt z': mpt2proj.point[2],
                         }

    with open(saveCompPickle, 'wb') as handle:
        pickle.dump(compareDict, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return



################################################  Execute  ################################################
main_process(cadPtsSampled, cadAlignedFile, meshFile, reshiftSaveMesh, saveCompPickle)


