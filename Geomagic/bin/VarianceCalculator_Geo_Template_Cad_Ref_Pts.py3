import geomagic.app.v3
from geomagic.app.v3.imports import *
import pickle
import os
import sys


################################################  Inputs  ################################################
<>cadFile<>
<>numOfPts<>
<>exportUnits<>

################################################  Outputs  ################################################
<>savePts<>


def save_pts_file(ptsObject, ptsFile, writeUnits):
    # create write object
    filewriter = WriteFile()
    writeOptions = FileSaveOptions()
    
    if writeUnits == 'Inches':
        writeOptions.units = Length.Inches
    if writeUnits == 'Millimeters':
        writeOptions.units = Length.Millimeters
    if writeUnits == 'Meters':
        writeOptions.units = Length.Meters
    
    filewriter.options = writeOptions
    # set path
    filewriter.filename = ptsFile  # r"C:\_Data\PG\Variance_Files\QB02502\0900A011_REV_C_rot_60k_Pts.asc"
    # set mesh within filewrite object
    filewriter.points = ptsObject
    # write the file
    filewriter.run()
    return


def save_mesh_file(meshObject, meshFile, writeUnits):
    # create write object
    filewriter = WriteFile()
    writeOptions = FileSaveOptions()
    
    if writeUnits == 'Inches':
        writeOptions.units = Length.Inches
    if writeUnits == 'Millimeters':
        writeOptions.units = Length.Millimeters
    if writeUnits == 'Meters':
        writeOptions.units = Length.Meters
    
    filewriter.options = writeOptions
    # set path
    filewriter.filename = meshFile
    # set mesh within filewrite object
    filewriter.mesh = meshObject
    # write the file
    filewriter.run()
    return


def geo_cad_pts_creation(importCadFile, numOfPts, savePtsFile, writeUnits):
    # print(cadFile)

    # Load CAD File
    cadFileLoader = ReadCADFromFile()
    cadFileLoader.fileName = importCadFile
    cadFileLoader.healCAD = True
    cadFileLoader.run()
    cadModelLoaded = cadFileLoader.cadModel
    cadModelLoadedMesh = cadModelLoaded.mesh

    # Remesh CAD File
    cadRemesher = RebuildCADMesh()
    cadRemesher.cadModel = cadModelLoaded
    cadRemesher.edgeTolerance = 0.000001
    cadRemesher.maxTriangleSize = 0.001
    cadRemesher.maxTriangles = 4000000
    cadRemesher.planarTolerance = 0.000001
    cadRemesher.stlMeshType = 0
    cadRemesher.run()

    # Get CAD Mesh
    cadMeshObj = cadRemesher.mesh

    # Convert Mesh to Points
    cadMeshPtsMod = CreatePointsFromMesh(cadMeshObj)
    cadMeshPtsMod.generateNormals = False
    cadMeshPtsMod.run()
    cadMeshPts = cadMeshPtsMod.points

    # pointsFromCad = CreatePointsFromCAD()
    # pointsFromCad.cadModel = cadModelLoaded
    # pointsFromCad.targetNumPoints = 1000000
    # pointsFromCad.run()
    # cadMeshPts = pointsFromCad.points

    # Sample Points to 60k
    samplePtsmod = SamplePoints()
    samplePtsmod.points = cadMeshPts
    samplePtsmod.targetNumPoints = numOfPts  # target 60k points in result
    samplePtsmod.curvaturePriority = 1.0  # Set curve priority to Max
    samplePtsmod.run()
    cadPtsSampled = samplePtsmod.points

    # Add points object to application and name the model samplePoints.
    # cadPtsSampledModel = geoapp.addModel(cadPtsSampled, u"cadPtsSampled")

    # savePtsFile = r"C:\_Data\PG\Variance_Files\QB02502\0900A011_REV_C_rot_60k_Pts.asc"
    save_pts_file(cadPtsSampled, savePtsFile, writeUnits)

    # Run the decimator
    # decimator = Decimate(cadMeshObj)
    # decimator.useTolerance = 0
    # decimator.targetNumTriangles = 118000
    # decimator.run()

    # decimatedMesh = decimator.mesh
    # save_mesh_file(decimatedMesh, decRefMeshFile)

    return cadPtsSampled


def geo_cad_pts_uniform_down(inputPts, targetPtCount):
    # sample points so that axis-neighbors get remoted; they have
    # distance 1 from each other.  diagonal neighbors have distance
    # sqrt(1.4) and are kept.
    # will have more than desired points
    samplePtsMod = SamplePoints()
    samplePtsMod.points = inputPts
    samplePtsMod.targetNumPoints = targetPtCount
    samplePtsMod.colorPriority = 0.0
    samplePtsMod.curvaturePriority = 0.0
    samplePtsMod.run()
    uniformPts = samplePtsMod.points

    # randomly sample down to exact amount of desired points
    sampleRandomMod = SamplePointsRandomly()
    sampleRandomMod.targetNumPoints = targetPtCount
    sampleRandomMod.points = uniformPts
    sampleRandomMod.run()
    uniformRandomPts = sampleRandomMod.points

    return uniformRandomPts


def geo_cad_pts_creation_v2(importCadFile, numOfPts, savePtsFile, writeUnits):

        # Load CAD File
    cadFileLoader = ReadCADFromFile()
    cadFileLoader.fileName = importCadFile
    cadFileLoader.healCAD = 0
    cadFileLoader.run()
    cadModelLoaded = cadFileLoader.cadModel
    cadModelLoadedMesh = cadModelLoaded.mesh

        # Sample Pts from Cad Surface
    pointSampler = CreatePointsFromCAD()
    pointSampler.cadModel = cadModelLoaded
    pointSampler.vertexPolicy = 0
    pointSampler.generateNormals = True
    pointSampler.targetNumPoints = numOfPts * 10
    pointSampler.run()
    cadPtsSampled = pointSampler.points

    uniformRandomPts = geo_cad_pts_uniform_down(cadPtsSampled, numOfPts)

        # Save Pts as File
    save_pts_file(uniformRandomPts, savePtsFile, writeUnits)

    return uniformRandomPts



def main_process(cadFile, numOfPts, savePts, writeUnits):
    # cadPtsSampled = geo_cad_pts_creation(cadFile, numOfPts, savePts)
    uniformRandomPts = geo_cad_pts_creation_v2(cadFile, numOfPts, savePts, writeUnits)
    return uniformRandomPts


################################################  Execute  ################################################
main_process(cadFile, numOfPts, savePts, exportUnits)


