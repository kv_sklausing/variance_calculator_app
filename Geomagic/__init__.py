'''
The Kinetic Vision Imaging Group library (KVIMG)

This code is developed, maintained, and owned by Kinetic Vision with the intent
of aiding imaging engineers in typical project work.
'''

__package__ = 'Geomagic'
__version__ = '1.0.0'
__author__ = 'Kinetic Vision'

# The contents of __all__ are imported when the user calls - import from "folder" *
__all__ = [
    'VarianceCalculator_Geo_MainProcess'
]

from .VarianceCalculator_Geo_MainProcess import *

