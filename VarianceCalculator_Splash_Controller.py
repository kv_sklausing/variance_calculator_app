'''
GUI SplashScreen for VarianceCalculator.py.
'''

from PySide2.QtCore import Qt

from UserInterface.VarianceCalculator_Widgets import *


class VarianceVisionSplash(TVDialogWindow):
    '''
    Class for the VAR Distribution main application window.
    '''

    def __init__(self, splashScreenImage):
        # Initialize the parent classes.
        super().__init__()

        self.splashScreenImage = splashScreenImage

        self.splash_screen_time()
        self.splash_screen_creation()

    def splash_screen_time(self):
        self.splashTimer = QTimer()
        return

    def splash_screen_creation(self):
        self.splashPixmap = QPixmap(self.splashScreenImage)
        self.splashScreen = QSplashScreen(self.splashPixmap, Qt.WindowStaysOnTopHint)
        return

    def show(self, timeToSplash=5000):
        # timeToSplash in [milliseconds]
        self.splashScreen.setEnabled(False)
        self.splashScreen.show()
        self.splashTimer.singleShot(timeToSplash, self.close)
        return
