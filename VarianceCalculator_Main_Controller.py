'''
Main Dashboard Control for VarianceCalculator.py.
'''

import pandas
import sys
import os
import traceback
import threading
from datetime import datetime
import trimesh

# Define module constants.
# Get info about this file.
SCRIPT_INTERPRETER_DIR = os.path.dirname(sys.executable)
if getattr(sys, 'frozen', False):
    # Set path for PySide 2 plugins
    qtPluginPath = os.path.join(SCRIPT_INTERPRETER_DIR, r'PySide2\plugins')
else:
    # Set path for PySide 2 plugins
    qtPluginPath = os.path.join(SCRIPT_INTERPRETER_DIR, r'Lib\site-packages\PySide2\plugins')

os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = qtPluginPath

from PySide2.QtWidgets import (QApplication, QStyleFactory,
                               QAbstractItemView, QHeaderView,
                               QAbstractScrollArea, QLabel)
from PySide2.QtCore import Signal, QThreadPool
from PySide2.QtGui import QIcon, QFontDatabase, QPixmap, QColor

import Utilities as VarianceUtilities

import VarianceCalculator_Main_Processor
import VarianceCalculator_Analyze_Processor
import VarianceCalculator_Logger_Controller
from VarianceCalculator_SelectFiles_Controller import VarianceCalculatorSelectFilesController
from VarianceCalculator_Contact_Controller import VarianceCalculatorContactController
from VarianceCalculator_Help_Controller import VarianceCalculatorHelpController

import UserInterface.VarianceCalculator_Styles as KV_Stylist
from UserInterface.Windows import VarianceCalculator_VarianceWindow
from UserInterface.VarianceCalculator_Assets import *

APP_ASSETS_LIST = (
    TC_APP_ICON_32, TC_APP_ICON_48, TC_APP_ICON_256,
    KV_FONT_PATH, KV_FONT_PATH_BOLD,
    KV_ALERT, KV_OKAY, TC_APP_SPLASH,
    DASHBOARD_EMPTY_HOWTO, SELECT_FILES_SINGLE_HOWTO,
    FILES_ADDED_SINGLE_HOWTO, SELECT_FILES_MULTI_HOWTO,
    FILES_ADDED_MULTI_HOWTO, CONFIRM_FILES_HOWTO, DASHBOARD_PROCESSING_HOWTO,
)

__version__ = '1.1.0'
__author__ = 'Kinetic Vision'
__release_date__ = '2/8/2021'

# Public AppData folder. Windows users should have write access here.
APP_DATA_DIRECTORY = r"C:\Users\Public\AppData\VarianceCalculatorApp"
if not os.path.isdir(APP_DATA_DIRECTORY):
    os.makedirs(APP_DATA_DIRECTORY)

# Define user settings file.
SETTINGS_FILENAME = 'VarianceCalculatorSettings.config'
SETTINGS_FULLPATH = os.path.join(APP_DATA_DIRECTORY, SETTINGS_FILENAME)

# Define icon file for application.
for APP_ICON in APP_ASSETS_LIST:
    if os.path.isfile(APP_ICON):
        continue
    else:
        print("Missing Icon File: %s" % os.path.basename(APP_ICON))

DEFAULT_INPUT_DIRECTORY = APP_DATA_DIRECTORY

GEOMAGIC_WRAP_CORE_EXE = r"C:\Program Files\3D Systems\Geomagic Wrap 2017\wrapCORE.exe"


class VarianceVisionMainController(VarianceCalculator_VarianceWindow.VarianceVisionMainGUI):
    '''
    Class for the VAR Distribution main application window.
    '''

    signalAnalysisThreadProgress = Signal(int, str)
    signalAnalysisThreadComplete = Signal()
    signalAnalysisThreadStatus = Signal(str)
    signalAnalysisThreadPause = Signal(bool)
    signalAnalysisThreadKill = Signal()
    signalAddPair = Signal(dict)
    signalDisplayMesh = Signal(trimesh.base.Trimesh)

    def __init__(self, font=None, appEnv='DEV'):
        # Initialize the parent classes.
        super().__init__()

        self.iconPath32 = TC_APP_ICON_32
        self.iconPath48 = TC_APP_ICON_48
        self.iconPath256 = TC_APP_ICON_256
        self.kvlogo = KV_LOGO_ICON
        self.appEnv = appEnv

        self.version = __version__
        self.releaseDate = __release_date__

        self.paused = False
        self.dashboardEmpty = DASHBOARD_EMPTY_HOWTO
        self.selectFilesSingle = SELECT_FILES_SINGLE_HOWTO
        self.filesAddedSingle = FILES_ADDED_SINGLE_HOWTO
        self.selectFilesMulti = SELECT_FILES_MULTI_HOWTO
        self.filesAddedMulti = FILES_ADDED_MULTI_HOWTO
        self.confirmFiles = CONFIRM_FILES_HOWTO
        self.dashboardProcessing = DASHBOARD_PROCESSING_HOWTO

        self.font = font

        # Initilize Logger
        self.init_logger()

        self.contactWindow = VarianceCalculatorContactController(parent=self, appIcon32=self.iconPath32,
                                                                  appIcon256=self.iconPath256,
                                                                  kvIcon=self.kvlogo)

        self.helpWindow = VarianceCalculatorHelpController(parent=self, appIcon32=self.iconPath32,
                                                            dashboardEmpty=self.dashboardEmpty,
                                                            selectFilesSingle=self.selectFilesSingle,
                                                            filesAddedSingle=self.filesAddedSingle,
                                                            selectFilesMulti=self.selectFilesMulti,
                                                            filesAddedMulti=self.filesAddedMulti,
                                                            confirmFiles=self.confirmFiles,
                                                            dashboardProcessing=self.dashboardProcessing
                                                            )

        # Initialize the Add Pair window.
        self.selectFilesController = VarianceCalculatorSelectFilesController(
            parent=self,
            iconPath32=self.iconPath32,
            iconPath48=self.iconPath48,
            addPairSignal=self.signalAddPair,
            appEnv=self.appEnv,
            mainLogger=self.mainLogger)

        # Initialize window properties.
        self.init_window()

        # Initialize Table View
        self.init_sample_table()

        # Initialize window settings.
        self.init_window_settings()

        # Apply settings for the window.
        self.apply_window_settings()

        # Initialize signal actions.
        self.init_signal_actions()

        # Initialize widget actions.
        self.init_widget_actions()

        self.analyzeThreadPool = QThreadPool()
        self.analyzeThreadPool.setMaxThreadCount(4)

        # Load and apply settings from file.
        self.load_settings_from_file()

        self.preAnalyzeThreadPool = QThreadPool()
        self.preAnalyzeThreadPool.setMaxThreadCount(2)

        return

    def init_logger(self):
        # Initialize Logger in different thread.
        self.launchTime = datetime.now()  # current date and time
        self.launchTimeFmt = self.launchTime.strftime("%y%m%d_%H%M%S")
        self.logFile = os.path.join(APP_DATA_DIRECTORY, '%s_VarianceCalculator.log' % self.launchTimeFmt)
        self.mainLoggerObject = VarianceCalculator_Logger_Controller.VarianceCalculatorLogging(
            logFileOutput=self.logFile)
        self.mainLogger = self.mainLoggerObject.mainControllerLogger

        # Remove log files older than 30 days
        self.clean_old_logs()
        return

    def clean_old_logs(self):
        oldLogFiles = VarianceUtilities.get_files(APP_DATA_DIRECTORY, dataTypeList='.log')
        for oldLogFile in oldLogFiles:
            logFileName = os.path.basename(oldLogFile)
            logFileDateFmt = logFileName.split("_VarianceCalculator")[0]
            logFileDate = datetime.strptime(logFileDateFmt, "%y%m%d_%H%M%S")
            logFileAge = self.launchTime - logFileDate
            if logFileAge.days > 7:
                os.remove(oldLogFile)

    def init_window(self):
        # Set some options for the window.
        if self.appEnv == 'DEV':
            self.setWindowTitle('Variance Calculator - v%s - DEV' % __version__ )
        else:
            self.setWindowTitle('Variance Calculator - v%s' % __version__)
        self.setWindowIcon(QIcon(self.iconPath32))
        self.setWindowModality(Qt.NonModal)

        self.mainStatusBar = self.statusBar()
        self.mainStatusBar.setStyleSheet(KV_Stylist.TVStatusBar)
        self.mainStatusBar.showMessage('Application Launched.')

        if os.path.isfile(GEOMAGIC_WRAP_CORE_EXE):
            self.actionGeoApp.setChecked(True)
            self.cadApp = GEOMAGIC_WRAP_CORE_EXE
        else:
            self.actionGeoApp.setChecked(False)
            self.actionGeoApp.setEnabled(False)
            self.actionGeoApp.setToolTip('Geomagic Wrap not found.')
            self.cadApp = None

        return

    def init_sample_table(self):
        self.tableWidgetMeshQueue.clearContents()
        self.tableWidgetMeshQueue.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)

        # Get Header Info
        self.tableWidgetMeshQueueHeader = self.tableWidgetMeshQueue.horizontalHeader()
        self.tableWidgetMeshQueueHeader.setDefaultAlignment(Qt.AlignCenter)
        self.tableWidgetMeshQueueHeader.setSectionResizeMode(QHeaderView.Stretch)
        self.tableWidgetMeshQueueHeader.setStretchLastSection(False)
        return

    def init_signal_actions(self):
        # Connect the signals to their actions.
        self.signalAnalysisThreadProgress.connect(
            self.update_status_progress_bar
        )
        self.signalAnalysisThreadComplete.connect(
            self.analysis_thread_complete
        )
        self.signalAnalysisThreadStatus.connect(
            self.update_status_bar_main
        )
        """ Signals that will be tied to functions in analyzing thread
        self.signalAnalysisThreadPause.connect(
            self.pause_processing_action
        )
        """
        self.signalAddPair.connect(
            self.add_pair_information
        )

        self.signalDisplayMesh.connect(
            self.update_mesh_to_display
        )

        return

    def init_widget_actions(self):
        # Connect the widgets to their actions.
        """ Add Mesh Pair"""
        ### Select Mesh Directory
        self.pushButtonAddPair.clicked.connect(
            self.select_add_pair_action
        )
        """ Remove Mesh Pair Action """
        self.pushButtonRemovePair.clicked.connect(
            self.remove_mesh_pair_main
        )
        """ Remove Mesh Pair Action """
        self.pushButtonClearComp.clicked.connect(
            self.remove_completed_items
        )
        """ Menu Bar Actions """
        self.actionFileAbout.triggered.connect(
            self.menu_bar_file_about
        )
        self.actionFileHowTo.triggered.connect(
            self.menu_bar_file_how_to
        )
        self.actionFileContact.triggered.connect(
            self.menu_bar_file_contact
        )
        self.actionReshiftCADandMesh.triggered.connect(
            self.menu_bar_preproc_shift
        )
        self.actionCloseManifold.triggered.connect(
            self.menu_bar_preproc_manifold
        )
        self.actionCADPtCover.triggered.connect(
            self.menu_bar_preproc_pt_cover
        )
        """ Processing Start Action """
        self.pushButtonStartProcessing.clicked.connect(
            self.start_processing_action
        )
        self.pushButtonPauseProcessing.clicked.connect(
            self.pause_processing_action
        )
        self.pushButtonResumeProcessing.clicked.connect(
            self.resume_processing_action
        )
        # """ Cancel Action """
        # self.pushButtonViewLog.clicked.connect(
        #     self.view_log_action
        # )
        return

    def init_window_settings(self):
        """ Initialize Window Settings on Startup"""

        # Create settings for window.
        self.settingsMain = VarianceUtilities.WindowSettings('MainWindow')

        self.meshPairDict = dict()
        self.meshPairSettings = dict()

        self.editingBool = False

        return

    def apply_window_settings(self):
        """ Apply setting to widgets for the window."""
        pass
        return

    def store_window_settings(self):
        """ Retrieve the settings from the widgets for the window."""
        pass
        return

    def load_settings_from_file(self):
        # Load the settings from file.
        try:
            settingsAll = VarianceUtilities.load_settings_file(SETTINGS_FULLPATH)
            if settingsAll is not None:
                # Apply settings for the main window.
                self.settingsMain = settingsAll.settingsMain
                self.apply_window_settings()

                # Apply settings for the add pair window.
                self.selectFilesController.settingsSelectFiles = settingsAll.settingsSelectFiles
                self.selectFilesController.apply_window_settings()

                self.helpWindow.helpWindowSize = settingsAll.helpWindowSize
                if self.helpWindow.helpWindowSize is not None:
                    self.helpWindow.resize(self.helpWindow.helpWindowSize)
        except:
            self.mainLogger.critical('Corrupt settings file. Removing.')
            if os.path.isfile(SETTINGS_FULLPATH):
                os.remove(SETTINGS_FULLPATH)

        return

    def save_settings_to_file(self):
        # Get the latest settings.
        settingsAll = VarianceUtilities.WindowSettings('AllWindows')

        self.store_window_settings()
        settingsAll.settingsMain = self.settingsMain

        self.selectFilesController.store_window_settings()
        settingsAll.settingsSelectFiles = self.selectFilesController.settingsSelectFiles

        settingsAll.helpWindowSize = self.helpWindow.helpWindowSize

        # Save the settings file.
        VarianceUtilities.save_settings_file(SETTINGS_FULLPATH, settingsAll)
        return

    def closeEvent(self, event):
        # Get latest settings and save settings to file.
        self.save_settings_to_file()

        handlers = self.mainLogger.handlers
        for handler in handlers:
            handler.close()
            self.mainLogger.removeHandler(handler)
        return

    def menu_bar_file_about(self):
        # NOTE: executes the closeEvent function.
        # licenseDialog.setPage(VarianceCalculatorLicenseController.VarianceMAPPERLICENSECONTROLLER_PAGE_LICENSE_STATUS)
        # licenseDialog.activateWindow()
        # licenseDialog.show()
        return

    def menu_bar_file_how_to(self):
        self.helpWindow.activateWindow()
        self.helpWindow.show()
        return

    def menu_bar_file_contact(self):
        self.contactWindow.activateWindow()
        self.contactWindow.show()
        return

    def menu_bar_preproc_shift(self):
        # TODO:
        ## Add shift code here
        return

    def menu_bar_preproc_manifold(self):
        # TODO:
        ## Add mesh manifold code here
        return

    def menu_bar_preproc_pt_cover(self):
        # TODO:
        ## Add CAD Pt Cover code here
        return

    def select_add_pair_action(self):
        # Open parameters window.
        self.update_status_bar_main('Selecting Files...', clear=True)

        self.editingBool = False
        self.selectFilesController.close_valid = False
        self.rowCt = self.tableWidgetMeshQueue.rowCount()
        self.selectFilesController.emptyRow = self.rowCt

        self.selectFilesController.editingData = None
        self.selectFilesController.editingRow = None
        self.selectFilesController.setup_single_file_ui()
        self.selectFilesController.show()
        self.selectFilesController.activateWindow()
        return

    def setup_pre_analyzing(self, subAddPairDict):
        ## TODO
        # Add check to see if duplicate entry already exists
        # If so, pop up dialog box with message and re open the add pair window
        # Create analysis thread.
        analyzeMesh = VarianceCalculator_Analyze_Processor.AnalyzeMesh(True, subAddPairDict,
                                                                       mainLogger=self.mainLogger,
                                                                       parent=self)

        analyzeMesh.signals.starting.connect(
            self.update_table_with_mesh_analysis_start
        )
        analyzeMesh.signals.finished.connect(
            self.update_table_with_mesh_analysis_finish
        )
        analyzeMesh.signals.error.connect(
            self.update_table_with_mesh_analysis_error
        )

        return analyzeMesh

    def add_pair_information(self, addPairInfo):
        newRowCount = len(addPairInfo)
        analyzeMeshList = list()
        self.tableWidgetMeshQueue.setEditTriggers(QAbstractItemView.NoEditTriggers)
        if not self.editingBool:
            self.tableWidgetMeshQueue.setRowCount(self.rowCt + newRowCount)

        for addPairRowNum, addPairRowInfoDict in addPairInfo.items():
            analyzeMesh = self.setup_pre_analyzing(addPairRowInfoDict)
            self.meshPairDict[addPairRowNum] = dict()
            self.meshPairDict[addPairRowNum].update(addPairRowInfoDict)
            self.meshPairDict[addPairRowNum]['QRunnable'] = analyzeMesh
            self.populate_table(addPairRowNum, addPairRowInfoDict)
            analyzeMeshList.append(analyzeMesh)

        # Start analyzing
        for analyzeMeshRunnable in analyzeMeshList:
            self.preAnalyzeThreadPool.start(analyzeMeshRunnable)
            # time.sleep(0.1)
        self.rowCt = self.tableWidgetMeshQueue.rowCount()

        return

    def populate_table(self, rowNum, addPairRowInfo):

        testItem = addPairRowInfo['test File Item']
        refItem = addPairRowInfo['ref File Item']
        outputDirItem = addPairRowInfo['Output Dir Item']
        outputTypeItem = addPairRowInfo['Output Type Item']
        analysisItem = addPairRowInfo['Analysis Item']
        pointDenItem = addPairRowInfo['Density Item']
        statusItemsText = addPairRowInfo['Status Items Text']

        actionItemsEdit = addPairRowInfo['Action Items Edit']
        actionItemsEdit.setEnabled(False)
        actionItemsOpen = addPairRowInfo['Action Items Open']
        actionItemsDelete = addPairRowInfo['Action Items Delete']
        actionItemsDisplay = addPairRowInfo['Action Items Display']
        actionItemsDisplay.setEnabled(False)
        actionItemsWidget = addPairRowInfo['Action Items Widget']

        outputTypeText = ' / '.join(addPairRowInfo['Output File Type'])

        # Set Table Text
        testItem.setText(addPairRowInfo['Test Mesh Name'])
        testItem.setToolTip(addPairRowInfo['Test Mesh File'])

        refItem.setText(addPairRowInfo['Ref Mesh Name'])
        refItem.setToolTip(addPairRowInfo['Ref Mesh File'])

        outputDirItem.setText(addPairRowInfo['Output Dir Name'])
        outputDirItem.setToolTip(addPairRowInfo['Output Dir'])

        outputTypeItem.setText(outputTypeText)

        pointDenItem.setText(f'{addPairRowInfo["Point Density"]:,}')
        clearBackground = QBrush()
        clearBackground.setStyle(Qt.NoBrush)
        pointDenItem.setBackground(clearBackground)

        try:
            # Set Table Text
            rowNumber = statusItemsText.row()
            self.tableWidgetMeshQueue.removeCellWidget(rowNumber, 4)
            analysisItem.setText('Waiting...')
            statusItemsText.setText('Waiting...')
        except RuntimeError:
            # Items deleted while anlayzing
            self.mainLogger.info('Mesh entry deleted while in pre-analyze queue (populate).')

        actionItemsEdit.clicked.connect(
            self.edit_mesh_pair_settings, type=Qt.UniqueConnection
        )
        actionItemsOpen.clicked.connect(
            self.open_mesh_pair_output_directory, type=Qt.UniqueConnection
        )
        actionItemsDelete.clicked.connect(
            self.remove_mesh_pair_sub, type=Qt.UniqueConnection
        )
        actionItemsDisplay.clicked.connect(
            self.display_colored_mesh, type=Qt.UniqueConnection
        )

        self.tableWidgetMeshQueue.setItem(rowNum, 0, testItem)
        self.tableWidgetMeshQueue.setItem(rowNum, 1, refItem)
        self.tableWidgetMeshQueue.setItem(rowNum, 2, outputDirItem)
        self.tableWidgetMeshQueue.setItem(rowNum, 3, outputTypeItem)
        self.tableWidgetMeshQueue.setItem(rowNum, 4, analysisItem)
        self.tableWidgetMeshQueue.setItem(rowNum, 5, pointDenItem)
        self.tableWidgetMeshQueue.setItem(rowNum, 6, statusItemsText)
        self.tableWidgetMeshQueue.setCellWidget(rowNum, 7, actionItemsWidget)

        self.tableWidgetMeshQueueHeader.setSectionResizeMode(QHeaderView.Stretch)

        return

    def update_table_with_mesh_analysis_start(self, analysisInfo):

        testItem = analysisInfo['test File Item']
        refItem = analysisInfo['ref File Item']
        outputDirItem = analysisInfo['Output Dir Item']
        outputTypeItem = analysisInfo['Output Type Item']
        pointDenItem = analysisInfo['Density Item']
        analysisItem = analysisInfo['Analysis Item']
        statusItemsText = analysisInfo['Status Items Text']
        statusItemsText.setToolTip(None)
        # analysisItemLabel = analysisInfo['QLabel']

        actionItemsEdit = analysisInfo['Action Items Edit']
        actionItemsOpen = analysisInfo['Action Items Open']
        actionItemsDelete = analysisInfo['Action Items Delete']
        actionItemsDisplay = analysisInfo['Action Items Display']
        actionItemsWidget = analysisInfo['Action Items Widget']

        analysisMessage = analysisInfo['Analysis']['Start']['Message']
        analysisToolTip = analysisInfo['Tooltip']['Start']
        statusMessage = analysisInfo['Status']

        try:
            # Set Table Text
            rowNumber = statusItemsText.row()
            self.tableWidgetMeshQueue.removeCellWidget(rowNumber, 4)
            analysisItem.setText(analysisMessage)
            analysisItem.setToolTip(analysisToolTip)
            statusItemsText.setText(statusMessage)
        except RuntimeError:
            # Items deleted while anlayzing
            self.mainLogger.info('Mesh entry deleted while in pre-analyze queue. (starting)')
        return

    def update_table_with_mesh_analysis_finish(self, analysisInfo):

        try:
            testItem = analysisInfo['test File Item']
            refItem = analysisInfo['ref File Item']
            outputDirItem = analysisInfo['Output Dir Item']
            outputTypeItem = analysisInfo['Output Type Item']
            pointDenItem = analysisInfo['Density Item']
            analysisItem = analysisInfo['Analysis Item']
            statusItemsText = analysisInfo['Status Items Text']
            # analysisItemLabel = analysisInfo['QLabel']

            actionItemsEdit = analysisInfo['Action Items Edit']
            actionItemsOpen = analysisInfo['Action Items Open']
            actionItemsDelete = analysisInfo['Action Items Delete']
            actionItemsDisplay = analysisInfo['Action Items Display']
            actionItemsWidget = analysisInfo['Action Items Widget']

            densityCheckPass = analysisInfo['Analysis']['Density Check']['Pass']
            fileCheckPass = analysisInfo['Analysis']['File Check']['Pass']
            bboxCheckPass = analysisInfo['Analysis']['Bbox Check']['Pass']
            areaCheckPass = analysisInfo['Analysis']['Area Check']['Pass']
            sampleCheckPass = analysisInfo['Analysis']['Sample Check']['Pass']
            swapCheckPass = analysisInfo['Analysis']['Swap Check']['Pass']
            testManifoldPass = analysisInfo['Analysis']['Test Manifold Check']['Pass']
            refManifoldPass = analysisInfo['Analysis']['Ref Manifold Check']['Pass']
            checkBoolSeries = pandas.Series(
                (densityCheckPass, fileCheckPass, bboxCheckPass, areaCheckPass, sampleCheckPass, swapCheckPass,
                 testManifoldPass, refManifoldPass),
                index=['Density Check', 'File Check', 'Bbox Check', 'Area Check', 'Sample Check', 'Swap Check',
                       'Test Manifold Check', 'Ref Manifold Check'])
            checkBoolKeys = checkBoolSeries[~checkBoolSeries].index.to_list()
            allAnalysisMsgs = list()
            allAnalysisTools = list()
            for checkBoolKey in checkBoolKeys:
                # analysisMessageTest = analysisInfo['Analysis'][checkBoolKey]['Pass']
                # allAnalysisMsgs.append(analysisMessageTest)
                analysisToolTipTest = analysisInfo['Tooltip'][checkBoolKey]
                allAnalysisTools.append(analysisToolTipTest)
                if checkBoolKey == 'Density Check' and 'differs from desired' in analysisToolTipTest:
                    newBackground = QBrush()
                    newBackground.setStyle(Qt.DiagCrossPattern)
                    newBackground.setColor(QColor(0, 124, 185))
                    pointDenItem.setBackground(newBackground)

            analysisToolTip = '\n'.join(allAnalysisTools)
            statusMessage = analysisInfo['Status']
        except:
            errorMsg = traceback.format_exc()
            # Items deleted while anlayzing
            self.mainLogger.error(errorMsg)
            return

        try:
            # Set Table Text and Icons
            analysisItemLabel = QLabel()
            analysisItemLabel.setAlignment(Qt.AlignCenter)
            rowNumber = statusItemsText.row()
            # Update point density for object
            self.meshPairDict[rowNumber]['Point Density'] = analysisInfo['Point Density']
            self.meshPairDict[rowNumber]['Subdivide'] = analysisInfo['Subdivide']
            self.meshPairDict[rowNumber]['Tool Tip Main'] = analysisToolTip
            self.meshPairDict[rowNumber]['Status'] = statusMessage
            self.tableWidgetMeshQueue.removeCellWidget(rowNumber, 4)
            analysisItem.setText('')
            if checkBoolSeries.all():
                meshOkayPixMap = QPixmap(KV_OKAY)
                analysisItemLabel.setPixmap(meshOkayPixMap)
                self.tableWidgetMeshQueue.setCellWidget(rowNumber, 4, analysisItemLabel)
                self.meshPairDict[rowNumber]['Analysis Pixmap'] = meshOkayPixMap
            else:
                meshAlertPixMap = QPixmap(KV_ALERT)
                analysisItemLabel.setPixmap(meshAlertPixMap)
                self.tableWidgetMeshQueue.setCellWidget(rowNumber, 4, analysisItemLabel)
                self.meshPairDict[rowNumber]['Analysis Pixmap'] = meshAlertPixMap
                analysisItem.setToolTip(analysisToolTip)

            statusItemsText.setText(statusMessage)
            actionItemsEdit.setEnabled(True)
        except RuntimeError:
            # Items deleted while anlayzing
            self.mainLogger.info('Mesh entry deleted while in pre-analyze queue. (finishing)')
        else:
            self.pushButtonAddPair.setEnabled(True)
            self.pushButtonRemovePair.setEnabled(True)
            self.pushButtonStartProcessing.setEnabled(True)
            self.pushButtonStartProcessing.setStyleSheet(KV_Stylist.TVPushButtonBlue)

        return

    def update_table_with_mesh_analysis_error(self, analysisInfo):

        testItem = analysisInfo['test File Item']
        refItem = analysisInfo['ref File Item']
        outputDirItem = analysisInfo['Output Dir Item']
        outputTypeItem = analysisInfo['Output Type Item']
        pointDenItem = analysisInfo['Density Item']
        analysisItem = analysisInfo['Analysis Item']
        statusItemsText = analysisInfo['Status Items Text']
        # analysisItemLabel = analysisInfo['QLabel']

        actionItemsEdit = analysisInfo['Action Items Edit']
        actionItemsOpen = analysisInfo['Action Items Open']
        actionItemsDelete = analysisInfo['Action Items Delete']
        actionItemsDisplay = analysisInfo['Action Items Display']
        actionItemsWidget = analysisInfo['Action Items Widget']

        analysisMessagePass = analysisInfo['Analysis']['Error']['Pass']
        analysisToolTip = analysisInfo['Tooltip']['Error']
        statusMessage = analysisInfo['Status']

        try:
            # Set Table Text and Icons
            analysisItemLabel = QLabel()
            analysisItemLabel.setAlignment(Qt.AlignCenter)
            rowNumber = statusItemsText.row()
            self.meshPairDict[rowNumber]['Subdivide'] = analysisInfo['Subdivide']
            self.meshPairDict[rowNumber]['Tool Tip Main'] = analysisToolTip
            self.meshPairDict[rowNumber]['Status'] = statusMessage
            self.tableWidgetMeshQueue.removeCellWidget(rowNumber, 4)
            analysisItem.setText('')
            meshErrorPixMap = QPixmap(KV_ERROR)
            analysisItemLabel.setPixmap(meshErrorPixMap)
            self.meshPairDict[rowNumber]['Analysis Pixmap'] = meshErrorPixMap
            self.tableWidgetMeshQueue.setCellWidget(rowNumber, 4, analysisItemLabel)
            # analysisItem.setText(analysisMessage)
            analysisItem.setToolTip(analysisToolTip)
            statusItemsText.setText(statusMessage)
            actionItemsEdit.setEnabled(True)
        except RuntimeError:
            # Items deleted while anlayzing
            self.mainLogger.info('Mesh entry deleted while in pre-analyze queue. (error)')
        else:
            self.pushButtonAddPair.setEnabled(True)
            self.pushButtonRemovePair.setEnabled(True)
        return

    def remove_mesh_pair_main(self):
        self.previouslyPaused = self.paused
        selectedRows = self.tableWidgetMeshQueue.selectionModel().selectedRows()

        # Get all row index
        selectedRowIndexes = []
        for selectedRow in selectedRows:
            selectedRowIndexes.append(selectedRow.row())

        # Reverse sort rows selectedRowIndexes
        selectedRowIndexes = sorted(selectedRowIndexes, reverse=True)

        rowsIP = self.get_table_in_prog_information()
        if rowsIP:
            for rowIP in rowsIP:
                if rowIP in selectedRowIndexes:
                    self.pause_processing_action()
                    killActiveThread = VarianceUtilities.confirm_user_dialog(
                        message='Are you sure you want to kill the active processing thread?',
                        iconPath=self.iconPath48,
                        notifyIcon=KV_ALERT)
                    if killActiveThread:
                        self.mainLogger.info('Killing active processing thread with [main] delete.')
                        self.threadAnalysis.kill()
                        self.threadAnalysis.join()
                        self.paused = False
                        self.show_start_processing_widget()
                    else:
                        rowToKeep = selectedRowIndexes.index(rowIP)
                        selectedRowIndexes.pop(rowToKeep)
                        if not self.previouslyPaused:
                            self.resume_processing_action()

        # Delete rows
        for rowidx in selectedRowIndexes:
            qRunnable = self.meshPairDict[rowidx]['QRunnable']
            try:
                self.preAnalyzeThreadPool.tryTake(qRunnable)
            except RuntimeError:
                self.mainLogger.info('Deleted mesh entry: pre-analyzing already completed.')
            self.tableWidgetMeshQueue.removeRow(rowidx)
            del self.meshPairDict[rowidx]

        self.reorder_mesh_pair_dict()

        return

    def remove_mesh_pair_sub(self):
        self.previouslyPaused = self.paused
        rowPressedFrom = self.get_button_index()
        rowInProg = self.get_table_in_prog_information()

        if rowPressedFrom is None:
            return

        if rowPressedFrom in rowInProg:
            self.pause_processing_action()
            killActiveThread = VarianceUtilities.confirm_user_dialog(
                message='Are you sure you want to kill the active processing thread?',
                iconPath=self.iconPath48,
                notifyIcon=KV_ALERT)
            if killActiveThread:
                self.mainLogger.info('Killing active processing thread with [action] delete.')
                self.threadAnalysis.kill()
                self.threadAnalysis.join()
                self.paused = False
                self.show_start_processing_widget()
            else:
                if not self.previouslyPaused:
                    self.resume_processing_action()
                return

        # Try to take from preanalyzing threadpool if hasnt started
        qRunnable = self.meshPairDict[rowPressedFrom]['QRunnable']
        try:
            self.preAnalyzeThreadPool.tryTake(qRunnable)
        except RuntimeError:
            self.mainLogger.info('Deleted mesh entry: pre-analyzing already completed.')

        self.tableWidgetMeshQueue.removeRow(rowPressedFrom)
        del self.meshPairDict[rowPressedFrom]
        self.reorder_mesh_pair_dict()

        return

    def remove_completed_items(self):
        rowsCompleted = self.get_table_complete_information()
        rowsError = self.get_table_error_information()
        rowsToClear = rowsCompleted + rowsError
        rowsToClear.sort(reverse=True)
        for rowidx in rowsToClear:
            self.tableWidgetMeshQueue.removeRow(rowidx)
            del self.meshPairDict[rowidx]

        self.reorder_mesh_pair_dict()
        return

    def get_button_index(self):
        # button = app.focusWidget()
        button = self.sender()
        index = self.tableWidgetMeshQueue.indexAt(button.parent().pos())
        if index.isValid():
            return index.row()

    def edit_mesh_pair_settings(self):
        rowPressedFrom = self.get_button_index()
        rowInformation = self.meshPairDict[rowPressedFrom]
        self.editingBool = True
        self.selectFilesController.close_valid = False
        self.selectFilesController.editingData = rowInformation
        self.selectFilesController.editingRow = rowPressedFrom
        self.selectFilesController.setup_single_file_ui()
        self.selectFilesController.pushButtonSaveFiles.setEnabled(True)
        self.selectFilesController.pushButtonSaveFiles.setStyleSheet(KV_Stylist.TVPushButtonBlue)
        # self.selectFilesController.setWindowModality(Qt.WindowModal)

        self.tableWidgetMeshQueue.removeCellWidget(rowPressedFrom, 4)
        analysisItem = rowInformation['Analysis Item']
        statusItemsText = rowInformation['Status Items Text']
        analysisItem.setText('Editing...')
        statusItemsText.setText('Editing...')

        self.selectFilesController.show()
        self.selectFilesController.activateWindow()
        return

    def open_mesh_pair_output_directory(self):
        rowPressedFrom = self.get_button_index()
        outputDirectory = self.meshPairDict[rowPressedFrom]['Output Dir']
        VarianceUtilities.open_directory(outputDirectory)
        return

    def get_table_ready_information(self):
        rowToProcessList = list()
        readyToProcessItems = self.tableWidgetMeshQueue.findItems('Ready', Qt.MatchExactly)
        for itemReady in readyToProcessItems:
            rowToProcess = itemReady.row()
            rowToProcessList.append(rowToProcess)
        rowToProcessList.sort()
        if rowToProcessList:
            rowNumNextProcess = rowToProcessList[0]
            return rowNumNextProcess
        return None

    def get_table_in_prog_information(self):
        rowIPList = list()
        inProgItems = self.tableWidgetMeshQueue.findItems('In Progress', Qt.MatchExactly)
        for itemIP in inProgItems:
            rowIP = itemIP.row()
            rowIPList.append(rowIP)
        rowIPList.sort(reverse=True)
        return rowIPList

    def get_table_complete_information(self):
        rowCompletedList = list()
        completedItems = self.tableWidgetMeshQueue.findItems('Complete', Qt.MatchExactly)
        for itemComplete in completedItems:
            rowComplete = itemComplete.row()
            rowCompletedList.append(rowComplete)
        rowCompletedList.sort(reverse=True)
        return rowCompletedList

    def get_table_error_information(self):
        rowErrorList = list()
        errorItems = self.tableWidgetMeshQueue.findItems('Processing Error', Qt.MatchExactly)
        for itemError in errorItems:
            rowError = itemError.row()
            rowErrorList.append(rowError)
        rowErrorList.sort(reverse=True)
        return rowErrorList

    def reorder_mesh_pair_dict(self):
        meshPairDf = pandas.DataFrame.from_dict(self.meshPairDict, orient='index')
        meshPairDf.sort_index(inplace=True)
        meshPairDf.reset_index(inplace=True, drop=True)
        meshPairDf.loc[:, 'Row Number'] = meshPairDf.index.values
        self.meshPairDict = meshPairDf.to_dict(orient='index')
        return

    def start_processing_action(self):

        self.threadAnalysis = None
        del self.threadAnalysis

        self.show_pause_processing_widget()

        nextToProcess = self.get_table_ready_information()
        if nextToProcess is None:
            self.show_start_processing_widget()
            VarianceUtilities.notify_user_dialog(message='No more files ready to be processed.',
                                                  iconPath=self.iconPath32)
            return

        self.pauseEvent = threading.Event()
        self.killEvent = threading.Event()

        # Set next processing information dictionary
        self.VariancePairDict = self.meshPairDict[nextToProcess]

        # Get files for check
        refMeshFile = self.VariancePairDict['Ref Mesh File']
        testMeshFile = self.VariancePairDict['Test Mesh File']

        # Get action item and set to false
        self.activeRowEditTool = self.VariancePairDict['Action Items Edit']
        self.activeRowEditTool.setEnabled(False)

        # get status items for text and progress bar
        self.statusItemsTextActive = self.VariancePairDict['Status Items Text']
        self.statusItemsBarActive = self.VariancePairDict['Status Items Bar']

        rowNumberActive = self.statusItemsTextActive.row()
        self.tableWidgetMeshQueue.setCellWidget(rowNumberActive, 6, self.statusItemsBarActive)

        if not os.path.isfile(refMeshFile):
            # Error, file doesnt exist before processing
            # User must have renamed or moved prior to processing reaching this entry
            errorMsg = 'ref mesh file has been removed/renamed from location and no longer exists.'
            self.update_status_progress_bar(None, errorMsg)
            return

        if not os.path.isfile(testMeshFile):
            # Error, file doesnt exist before processing
            # User must have renamed or moved prior to processing reaching this entry
            errorMsg = 'test mesh file has been removed/renamed from location and no longer exists.'
            self.update_status_progress_bar(None, errorMsg)
            return

        pointDensity = self.VariancePairDict['Point Density']
        outputDir = self.VariancePairDict['Output Dir']
        outputTypes = self.VariancePairDict['Output File Type']
        outputFileName = "{name}-{density}".format(
            name=os.path.splitext(os.path.basename(testMeshFile))[0],
            density=pointDensity)

        outputFileAlreadyExists = VarianceUtilities.get_files(directory=outputDir,
                                                               dataTypeList=outputTypes,
                                                               contains=outputFileName)
        if outputFileAlreadyExists is not None:
            stopProcessing = VarianceUtilities.confirm_user_dialog(
                message='Variance File already exists for inputs, do you wish to overwrite?\n Selecting No will timestamp the current file.',
                iconPath=self.iconPath48,
                notifyIcon=KV_ALERT)
            if stopProcessing:
                # Overwriting
                pass
            else:
                # Not overwriting, time stamping
                launchTime = datetime.now()  # current date and time
                launchTimeFmt = launchTime.strftime("%y%m%d_%H%M%S")
                outputFileName = '%s_%s' % (outputFileName, launchTimeFmt)

        self.VariancePairDict['Output File Name'] = outputFileName
        self.threadAnalysis = AnalysisThread(
            self.signalAnalysisThreadProgress,
            self.signalAnalysisThreadComplete,
            self.signalAnalysisThreadStatus,
            self.signalDisplayMesh,
            self.pauseEvent,
            self.killEvent,
            self.VariancePairDict,
            mainLogger=self.mainLogger,
            geomagicExe=GEOMAGIC_WRAP_CORE_EXE)

        self.signalAnalysisThreadPause.connect(
            self.threadAnalysis.pause_thread
        )

        self.mainLogger.info('Starting the Variance analysis thread for row entry: %s' % nextToProcess)
        self.update_status_bar_main('Processing File: %s' % outputFileName)
        self.threadAnalysis.start()

        return

    def resume_processing_action(self):
        try:
            self.show_pause_processing_widget()
            self.pauseEvent.clear()
            self.mainLogger.info('Processing Paused: %s' % str(self.pauseEvent.is_set()))
            self.statusItemsBarActive.setStyleSheet(KV_Stylist.TVProgressBarEnabled)
            self.paused = False
        except:
            errorMsg = traceback.format_exc()
            self.mainLogger.exception(errorMsg)

        return

    def pause_processing_action(self):
        try:
            self.show_resume_processing_widget()
            self.pauseEvent.set()
            self.mainLogger.info('Processing Paused: %s' % str(self.pauseEvent.is_set()))
            self.statusItemsBarActive.setStyleSheet(KV_Stylist.TVProgressBarDisabled)
            self.paused = True
        except:
            errorMsg = traceback.format_exc()
            self.mainLogger.exception(errorMsg)
        return

    def analysis_thread_complete(self):
        try:
            rowNumberActive = self.statusItemsTextActive.row()
            self.statusItemsTextActive.setText('Complete')
            self.tableWidgetMeshQueue.removeCellWidget(rowNumberActive, 6)
            self.pushButtonClearComp.setEnabled(True)
            self.start_processing_action()
        except:
            errorMsg = traceback.format_exc()
            self.mainLogger.exception(errorMsg)
        return

    def display_colored_mesh(self):
        rowPressedFrom = self.get_button_index()
        meshToDisplay = self.meshPairDict[rowPressedFrom]['Colored Mesh']
        meshToDisplay.show()
        return

    def update_status_progress_bar(self, percentComplete, errorMsg):
        # Update the Qlabel for the current file that is processing.
        try:
            rowNumberActive = self.statusItemsTextActive.row()
            if percentComplete is not None and percentComplete >= 0:
                self.statusItemsTextActive.setText('In Progress')
                self.statusItemsBarActive.setValue(percentComplete)
            else:
                self.statusItemsTextActive.setText('Processing Error')
                self.statusItemsTextActive.setToolTip(errorMsg)
                self.statusItemsBarActive.setValue(0)
                self.tableWidgetMeshQueue.removeCellWidget(rowNumberActive, 6)
                self.pushButtonClearComp.setEnabled(True)
                self.start_processing_action()
        except RuntimeError:
            self.mainLogger.info('Active mesh entry deleted from using cancel.')
        return

    def update_mesh_to_display(self, meshToDisplay):
        try:
            rowPressedFrom = self.statusItemsTextActive.row()
            actionItemsDisplay = self.meshPairDict[rowPressedFrom]['Action Items Display']
            actionItemsDisplay.setEnabled(True)
            self.meshPairDict[rowPressedFrom]['Colored Mesh'] = meshToDisplay

        except RuntimeError:
            self.mainLogger.info('Active mesh entry deleted from using cancel.')
        return

    def update_status_bar_main(self, messageToDisplay, clear=False):
        if clear:
            self.mainStatusBar.clearMessage()
        currentMsg = self.mainStatusBar.currentMessage()
        if currentMsg != '':
            basesMsg = currentMsg.split(' - ')[0]
            messageToDisplay = [basesMsg, messageToDisplay]
            messageToDisplay = ' - '.join(messageToDisplay)
        self.mainStatusBar.showMessage(messageToDisplay)
        return

# threading.Thread
# QRunnable
class AnalysisThread(threading.Thread):
    """
    Class that handles processing the analysis routine in its own thread.
    """

    def __init__(self, signalAnalysisProgressThread, signalAnalysisCompleteThread, signalAnalysisStatusThread,
                 signalDisplayMesh,
                 pauseEvent, killEvent,
                 VariancePairInfo,
                 mainLogger=None,
                 geomagicExe=None):
        # Initialize the parent class.
        super().__init__()

        self.killed = False
        # self.setDaemon(True)

        # Set thread name.
        self.name = 'AnalysisThread'

        # Initialize variables.
        self.signalAnalysisCompleteThread = signalAnalysisCompleteThread

        # Signals to pass to processor
        self.signalAnalysisProgressThread = signalAnalysisProgressThread

        # Signals to pass message to status bar
        self.signalAnalysisStatusThread = signalAnalysisStatusThread

        # Signal that passes mesh to store in memory for button to display
        self.signalDisplayMesh = signalDisplayMesh

        self.VariancePairInfo = VariancePairInfo

        self.isAnalysisSuccess = False

        self.pauseEvent = pauseEvent

        self.killEvent = killEvent

        self.mainLogger = mainLogger

        self.geoExe = geomagicExe

        # Initialize Mesh Pair Settings
        self.init_mesh_pair_settings()

        self.VarianceCalculator = VarianceCalculator_Main_Processor.VarianceCalculatorProcessor(
            progressSignal=self.signalAnalysisProgressThread,
            statusSignal=self.signalAnalysisStatusThread,
            displayMeshSignal=self.signalDisplayMesh,
            pauseEvent=self.pauseEvent,
            mainLogger=self.mainLogger,
            geomagicExe=self.geoExe
        )

        return

    def start(self):
        self.__run_backup = self.run
        self.run = self.__run
        threading.Thread.start(self)

    def __run(self):
        sys.settrace(self.globaltrace)
        self.__run_backup()
        self.run = self.__run_backup

    def globaltrace(self, frame, event, arg):
        if event == 'call':
            return self.localtrace
        else:
            return None

    def localtrace(self, frame, event, arg):
        # If the thread is killed during this calculation, a exception would be caught and it would start a SLOW loop
        # I edited the trimesh.geometry exception handling to return None if SystemExit was rasied (thread killed)
        # to avoid the SLOW loop execution of this and just kill the thread
        # see lines 363 on in trimesh.geometry, Scott Klausing
        if self.killed:
            if event == 'line':
                raise SystemExit()
        return self.localtrace

    def kill(self):
        self.killed = True

    def init_mesh_pair_settings(self):
        self.refMeshFile = self.VariancePairInfo['Ref Mesh File']
        self.testMeshFile = self.VariancePairInfo['Test Mesh File']
        self.pointDensity = self.VariancePairInfo['Point Density']
        self.outputFormats = self.VariancePairInfo['Output File Type']
        self.outputDir = self.VariancePairInfo['Output Dir']
        self.subdivideNeeded = self.VariancePairInfo['Subdivide']
        self.rowNumber = self.VariancePairInfo['Row Number']
        self.outputFileName = self.VariancePairInfo['Output File Name']

        self.analysisItem = self.VariancePairInfo['Analysis Item']
        self.statusItemsText = self.VariancePairInfo['Status Items Text']
        self.statusItemsBar = self.VariancePairInfo['Status Items Bar']
        self.actionItemsEdit = self.VariancePairInfo['Action Items Edit']
        self.actionItemsOpen = self.VariancePairInfo['Action Items Open']
        self.actionItemsDelete = self.VariancePairInfo['Action Items Delete']
        self.actionItemsWidget = self.VariancePairInfo['Action Items Widget']

        self.referenceFileType = self.VariancePairInfo['Reference File Type']
        self.referenceFileExt = self.VariancePairInfo['Reference File Ext']

        self.geoUnits = self.VariancePairInfo['Geo Units']
        return

    def run(self):
        self.mainLogger.info('Variance Calculation thread started.')
        # Use try/except statement to catch an unexpected errors during analysis.
        # This ensures the app does not crash entirely during use.
        try:
            # Execute the analysis routine.
            self.analysis_routine()
        except SystemExit:
            self.mainLogger.info('Thread killed using trace because active was deleted/cancelled.')
        except Exception:
            errorTraceback = traceback.format_exc()
            self.mainLogger.exception(
                'Variance processing error encountered processing files:\n%s\n%s\n.' % (
                self.refMeshFile, self.testMeshFile))
            self.mainLogger.exception(errorTraceback)
            errorTip = errorTraceback.splitlines()[-1]
            # Will cause runtime exception if thread was killed during processing
            self.signalAnalysisProgressThread.emit(-1, errorTip)
        else:
            self.complete_thread()

        self.mainLogger.info('Variance Calculation thread finished.')
        return

    def analysis_routine(self):

        # Batch process mesh files in input directory.
        # Use try/except statement to catch an unexpected errors during analysis.
        # This ensures the analysis continues on to next file.
        # Emit the process complete signal.
        # Standard Processing Settings
        samplingType = 'uniform'
        vertexNormalType = 'single'
        vertexNeighborSize = None

        self.signalAnalysisProgressThread.emit(0, 'None')

        self.VarianceCalculator.testMeshFile = self.testMeshFile
        self.VarianceCalculator.refMeshFile = self.refMeshFile
        self.VarianceCalculator.pointDensity = self.pointDensity
        self.VarianceCalculator.outputFormats = self.outputFormats
        self.VarianceCalculator.samplingType = samplingType
        self.VarianceCalculator.vertexNormalType = vertexNormalType
        self.VarianceCalculator.vertexNeighborSize = vertexNeighborSize
        self.VarianceCalculator.outputDir = self.outputDir
        self.VarianceCalculator.outputFileName = self.outputFileName
        self.VarianceCalculator.subdivideMesh = self.subdivideNeeded
        self.VarianceCalculator.statusItemsBar = self.statusItemsBar
        self.VarianceCalculator.statusItemsText = self.statusItemsText
        self.VarianceCalculator.actionItemsDelete = self.actionItemsDelete
        self.VarianceCalculator.VarianceType = 'combined'
        self.VarianceCalculator.referenceType = self.referenceFileType
        self.VarianceCalculator.referenceExt = self.referenceFileExt
        self.VarianceCalculator.geoUnits = self.geoUnits

        self.VarianceCalculator.main_variance_process()
        return

    def pause_thread(self, bool):
        self.mainLogger.info('Thread is paused: %s' % bool)
        self.statusItemsBar.setEnabled(~bool)
        return

    def complete_thread(self):
        self.mainLogger.info('Variance Calulation thread completed: emitting signal.')
        self.signalAnalysisCompleteThread.emit()
        return


if __name__ == '__main__':
    # Add support for multiprocessing for when code has been frozen as Windows executable.
    # https://docs.python.org/2/library/multiprocessing.html#miscellaneous
    # freeze_support()

    # Enable dpi scaling so if the windows system setting for scale is large than 100%
    # the application window scales accordingly without getting distorted.
    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling, on=True)

    # Look at this line
    QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps, on=True)

    # Start the application.
    print('\nApplication started.')
    app = QApplication(sys.argv)

    fontPath = KV_FONT_PATH
    fontPathBold = KV_FONT_PATH_BOLD
    fontId = QFontDatabase.addApplicationFont(fontPath)
    fontIdBold = QFontDatabase.addApplicationFont(fontPathBold)

    fontList = QFontDatabase.applicationFontFamilies(fontId)
    fontName = fontList[0]
    print(fontName)

    # Set the main window style. Use windowsvista for newest windows style.
    app.setStyle(QStyleFactory.create('windows'))

    app.setStyleSheet(KV_Stylist.TVAppStyles)
    app.setFont(fontName)

    # SPLASH_SCREEN = True
    # if SPLASH_SCREEN:
    #     # Initialize and show the splash screen.
    #     splashScreen = VarianceVisionSplash(TC_APP_SPLASH)
    #     splashScreen.show()
    #     app.exec_()
    #
    # licenseDialog = VarianceCalculatorLicenseController(appIcon256=TC_APP_ICON_256,
    #                                                      appIcon32=TC_APP_ICON_32,
    #                                                      kvIcon=KV_LOGO_ICON,
    #                                                      releaseDate=__release_date__,
    #                                                      version=__version__)
    # licenseActive, statusString = licenseDialog.licenseIsActive()
    # if licenseActive:
    #     print('License is active!')
    # else:
    #     print('License is not active. Prompting the user to activate. (status: %s)' % statusString)
    #     licenseDialog.setPage(
    #         VarianceCalculatorLicenseController.VarianceMAPPERLICENSECONTROLLER_PAGE_ACTIVATE_LICENSE)
    #     licenseDialog.exec_()
    #
    # licenseActive, statusString = licenseDialog.licenseIsActive()
    # if not licenseActive:
    #     print('License is still not activated. Exiting. (status: %s)' % statusString)
    #     sys.exit(1)

    # Initialize and show the main window.
    mainGui = VarianceVisionMainController(font=fontName)  # , appEnv=licenseDialog.PRODUCT_ENV)
    mainGui.show()
    app.exit(app.exec_())
    print('\nApplication closed.')
