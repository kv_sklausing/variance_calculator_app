'''
Main Backend Processing for varianceCalculator.py.
'''

import trimesh
from trimesh import proximity as triprox
import pyembree  # Used by trimesh (not directly called)
import open3d as o3d
from scipy import spatial
import scipy.stats as stats
import numba
import operator
import time

import os
import pandas
import numpy
import copy
import matplotlib.cm
import matplotlib.colors

import sys

import Utilities as varianceUtilities

import Geomagic as varianceGeo

BYTES_CONVERSION = 1073741824


class VarianceCalculatorProcessor:
    """
    Class that handles the processing for the variance distribution application.
    """

    def __init__(self, progressSignal=None, statusSignal=None,
                 displayMeshSignal=None, pauseEvent=None, mainLogger=None, geomagicExe=None):
        self.DEBUG = False

        # Inputs to processing
        self.refMeshFile = None
        self.testMeshFile = None
        self.pointDensity = None
        self.outputFormats = None
        self.samplingType = None
        self.vertexNormalType = None
        self.vertexNeighborSize = None
        self.outputDir = None
        self.outputFileName = None
        self.subdivideMesh = False
        self.statusItemsBar = None
        self.statusItemsText = None
        self.varianceType = 'dist'
        self.referenceType = None
        self.referenceExt = None

        # Variables created during processing
        self.refTriMesh = None
        self.ref3dMesh = None

        self.testTriMesh = None
        self.test3dMesh = None

        self.refUniformPoints = None
        self.refGridPoints = None
        self.sampledPointsEstimate = None
        self.sampledPointsFinal = None
        self.sampledPointsFinalNormals = None
        self.varianceVals = None

        self.decimated3dMesh = None
        self.decimatedTriMesh = None

        self.geoPointFile = None

        self.progressSignal = progressSignal
        self.statusSignal = statusSignal
        self.displayMeshSignal = displayMeshSignal
        self.pauseEvent = pauseEvent

        self.paused = False

        self.mainLogger = mainLogger
        self.geomagicExe = geomagicExe

        self.geoUnits = None

    @varianceUtilities.timer
    def subdivide_ref_mesh(self):
        numVerts = len(self.refTriMesh.vertices)
        while numVerts < self.pointDensity:
            self.refTriMesh = self.refTriMesh.subdivide()
            numVerts = len(self.refTriMesh.vertices)
        return

    @varianceUtilities.timer
    def load_mesh_files(self):
        """
        Subdivides and converts to Open3D if necessary
        NOTE: Meshes are loaded in controller for checks before processing is ran!
        """

        if self.referenceType == 'mesh':
            self.meshPairDict = {'Test Mesh File': self.testMeshFile,
                                 'Ref Mesh File': self.refMeshFile}
        else:
            self.meshPairDict = {'Test Mesh File': self.testMeshFile}

        self.mainLogger.info('Reading Test and Ref mesh.')
        self.testTriMesh, self.refTriMesh = varianceUtilities.start_mesh_load_thread(self.meshPairDict)

        if not self.testTriMesh.is_watertight:
            raise OSError('Test Mesh is not watertight.')

        if self.subdivideMesh:
            self.subdivide_ref_mesh()

        # Check pyembree was packaged correctly using pyinstaller
        print(self.testTriMesh.ray)
        self.mainLogger.info('Trimesh Ray Library: %s' % self.testTriMesh.ray)
        if isinstance(self.testTriMesh.ray, trimesh.ray.ray_pyembree.RayMeshIntersector):
            pass
        else:
            raise OSError('Pyembree not installed correctly on machine.')

        # convert to open3d mesh for decimation
        # self.test3dMesh = self.testTriMesh.as_open3d

        return

    def scipy_grid_spacing(self, inputVertices, binSize):
        x = inputVertices[:, 0]
        y = inputVertices[:, 1]
        z = inputVertices[:, 2]
        xMin = x.min()
        xMax = x.max()
        yMin = y.min()
        yMax = y.max()
        zMax = z.max()
        zMin = z.min()
        binCountX = int((xMax - xMin) / binSize)
        binCountY = int((yMax - yMin) / binSize)
        binCountZ = int((zMax - zMin) / binSize)
        binx = numpy.linspace(xMin, xMax, binCountX)
        biny = numpy.linspace(yMin, yMax, binCountY)
        binz = numpy.linspace(zMin, zMax, binCountZ)
        statRet, binEdge, binNum = stats.binned_statistic_dd((x, y, z), [x, y, z], 'mean',
                                                             bins=[binx, biny, binz])

        statRetXFlat = statRet[0].flatten()
        statRetYFlat = statRet[1].flatten()
        statRetZFlat = statRet[2].flatten()
        meanBinArray = numpy.vstack([statRetXFlat, statRetYFlat, statRetZFlat]).transpose()
        isNanFilt = ~numpy.isnan(meanBinArray).any(axis=1)
        newCoords = meanBinArray[isNanFilt]
        ptCount = newCoords.shape[0]
        return newCoords, ptCount

    @varianceUtilities.timer
    def iterate_grid_spacing_scipy(self, voxelSpacing, pointDensity, inputVertices):

        refGridPoints, numPts = self.scipy_grid_spacing(inputVertices, voxelSpacing)

        # Do Iteration with No Number of Pts Set First
        keepIterating = True
        minPts = pointDensity * 0.995
        maxPts = pointDensity * 1.005
        precision = 0.1
        checkBool = numpy.array(numPts > pointDensity)

        # Iterate Through precision to get approximation for number of points
        while keepIterating:
            if numPts > pointDensity:
                voxelSpacing += precision
            if numPts < pointDensity:
                voxelSpacing -= precision
            refGridPoints, numPts = self.scipy_grid_spacing(inputVertices, voxelSpacing)
            checkBool = numpy.append(checkBool, (numPts > pointDensity))

            if operator.ne(checkBool[-2], checkBool[-1]):
                precision /= 10
            if numPts > maxPts:
                keepIterating = True
            elif numPts < minPts:
                keepIterating = True
            else:
                keepIterating = False

        return refGridPoints

    @varianceUtilities.timer
    def sample_ref_vertices(self):

        # Create Open3D mesh objects from Trimesh Objects
        self.mainLogger.info('Converting the Ref mesh to Open 3D type.')
        self.ref3dMesh = self.refTriMesh.as_open3d
        # self.test3dMesh = self.testTriMesh.as_open3d

        # Start by sampling points uniformly
        self.refUniformPoints = numpy.asarray(
            self.ref3dMesh.sample_points_uniformly(number_of_points=self.pointDensity).points)

        if self.samplingType == 'uniform':
            self.mainLogger.info('Sampling ref points using a uniform spacing.')
            self.sampledPointsEstimate = copy.deepcopy(self.refUniformPoints)

        if self.samplingType == 'voxel':
            self.mainLogger.info('Sampling ref points using a voxel grid.')
            # # Use Trimesh Sampling
            # refMeshSampledPtsEven, refMeshSampledFaceIDEven = trimesh.sample.sample_surface_even(self.refMesh, self.pointDensity,
            #                                                                                              radius=None)

            # Get Mean Distance from uniform sampling to use as estimate for voxel size
            refUniformTree = spatial.cKDTree(self.refUniformPoints)
            minDistPt2PtUniform, minIdPt2PtUniform = refUniformTree.query(self.refUniformPoints, k=2)
            closetPtsDistances = minDistPt2PtUniform[:, 1]
            meanDistance = closetPtsDistances.mean()
            voxelSpacing = meanDistance * 1.5

            self.mainLogger.info('Finding voxel spacing iteratively...')
            # Added Numba Jit Decorator to speed up
            self.sampledPointsEstimate = self.iterate_grid_spacing_scipy(voxelSpacing,
                                                                         self.pointDensity,
                                                                         numpy.asarray(self.ref3dMesh.vertices))

        if self.DEBUG:
            uniformDownPcdFile = os.path.join(DEBUG_SAVE_DIR, '_debug_point_sampling_%s.xyz' % self.samplingType)
            numpy.savetxt(uniformDownPcdFile, self.sampledPointsEstimate, delimiter=',', fmt="%10.6f")
            # o3d.io.write_point_cloud(uniformDownPcdFile, self.sampledPointsEstimate)
            return

    @varianceUtilities.timer
    def related_by_distance_duplicate_fix(self, uniqueSurfacePtCount, pointsOnSurfIdx):
        # Iterate subdivide in while loop to get non-duplicated points back on actual mesh

        refVertsDf = pandas.DataFrame(self.refTriMesh.vertices)
        pointsOnSurfIdxDfUnique = numpy.unique(pointsOnSurfIdx)

        uniquePtRatio = uniqueSurfacePtCount / len(self.sampledPointsEstimate)
        if uniquePtRatio <= 0.99:
            counter = 1
            while uniquePtRatio < 0.99 and counter <= 2:
                # Sampled Points related back to ref vertices by their closest point contained
                # duplicate ref vertex points as a closet point; attempting to get more unique by subdivision
                # limited to 2 subdivisions if neccesary to avoid major time consume
                self.mainLogger.info(
                    'Subdividing to relate to more unique Ref vertices #%i; unique point ratio: %0.2f.' % (
                        counter, uniquePtRatio))
                self.refTriMesh = self.refTriMesh.subdivide()
                refVertsDf = pandas.DataFrame(self.refTriMesh.vertices)
                refTriMeshTree = spatial.cKDTree(refVertsDf)
                pointsOnSurfDist, pointsOnSurfIdx = refTriMeshTree.query(self.sampledPointsEstimate, k=1)

                pointsOnSurfIdxDfUnique = numpy.unique(pointsOnSurfIdx)
                uniqueSurfacePtCount = len(pointsOnSurfIdxDfUnique)

                uniquePtRatio = uniqueSurfacePtCount / len(self.sampledPointsEstimate)
                counter += 1

        # If after 2 subdivisions and still not enough unique Ref points
        # randomly grab the remaining amount to get to desired point count
        if uniqueSurfacePtCount < len(self.sampledPointsEstimate):
            uniqueCtDiff = len(self.sampledPointsEstimate) - uniqueSurfacePtCount
            self.mainLogger.info('Randomly Selecting %i unique points...' % uniqueCtDiff)
            refVertsDfUnused = refVertsDf.drop(pointsOnSurfIdxDfUnique)
            randomUnusedPtsIdxs = refVertsDfUnused.sample(uniqueCtDiff).index.values
            pointsOnSurfIdx = numpy.concatenate([pointsOnSurfIdxDfUnique, randomUnusedPtsIdxs])

        return pointsOnSurfIdx

    @varianceUtilities.timer
    def finalize_pt_information(self):
        self.mainLogger.info('Relating points back to ref nodes.')
        # get exact mesh vertex points that relate to the sampled points
        # for each sampled point, the closed mesh vertex is used
        refTriMeshTree = spatial.cKDTree(self.refTriMesh.vertices)
        pointsOnSurfDist, pointsOnSurfIdx = refTriMeshTree.query(self.sampledPointsEstimate,
                                                                 k=1)

        uniqueSurfacePtCount = len(numpy.unique(pointsOnSurfIdx))
        if uniqueSurfacePtCount < len(self.sampledPointsEstimate):
            self.mainLogger.info('Distance Relation had duplicate points; attempting to relate more unique.')
            pointsOnSurfIdx = self.related_by_distance_duplicate_fix(uniqueSurfacePtCount, pointsOnSurfIdx)

        self.sampledPointsFinal = self.refTriMesh.vertices[pointsOnSurfIdx]

        if self.vertexNormalType == 'single':
            # The vertex normals of the mesh.
            # If the normals were loaded Trimesh checks to make sure they have the
            # same number of vertex normals and vertices before returning them.
            # If there are no vertex normals defined or a shape mismatch Trimesh
            # calculates the vertex normals from the mean normals of the faces the vertex is used in.
            # If the thread is killed during this calculation, a exception would be caught and it would start a SLOW loop
            # I edited the trimesh.geometry exception handling to return None if SystemExit was rasied (thread killed)
            # to avoid the SLOW loop execution of this and just kill the thread
            # see lines 363 on in trimesh.geometry, Scott Klausing
            self.mainLogger.info('Extracting point normals from faces connected to vertex.')
            self.sampledPointsFinalNormals = self.refTriMesh.vertex_normals[pointsOnSurfIdx]  # * (-1)
            return

        if self.vertexNormalType == 'neighbor':
            self.mainLogger.info('Extracting point normals from faces (and neighboring) connected to vertex.')
            # Find # of closest points to point in question
            minDistPt2PtNeighbors, minIdPt2PtNeighbors = refTriMeshTree.query(self.sampledPointsFinal,
                                                                              k=self.vertexNeighborSize + 1)
            # Remove the point itself since exists in tree and will have exact match
            # minDistPt2PtNeighbors = minDistPt2PtNeighbors[:, 1:]
            minIdPt2PtNeighbors = minIdPt2PtNeighbors[:, 1:]

            # Get normals for neighboring # of vertices and take the mean
            self.sampledPointsFinalNormals = self.refTriMesh.vertex_normals[minIdPt2PtNeighbors].mean(axis=1)
            return

    @varianceUtilities.timer
    def compute_variance(self):
        print("Getting distances...")
        # Ran for several hours, didnt finish with 60k pts
        # If pyembree installed, took 2 minutes to run
        # runs trimesh closet_point and then contains_point

        """
        Find the signed distance from a mesh to a list of points.
        * Points outside the mesh will have NEGATIVE distance
        * Points within tol.merge of the surface will have POSITIVE distance
        * Points inside the mesh will have POSITIVE distance
        """
        # no need to times by (-1) because
        # using the test mesh as the mesh, sampled points from reference
        # and points out of mesh have negative (test lies in reference)
        # and vice versa
        # does "cloest point" method for distance
        # on mesh then uses trianlge normal for sign or intersections from ray casting

        # Closet Point Method
        self.varianceVals = triprox.signed_distance(self.testTriMesh, self.sampledPointsFinal)

        # Orthogonal method
        # if self.varianceType.lower() == 'orthogonal':  # Outside to Inside only using ray method
        #     self.varianceVals = triprox.longest_ray(self.testTriMesh, self.sampledPointsFinal, self.sampledPointsFinalNormals * (-1))

        # Times by negative 1 to put ref points as positive, test points as negative
        # self.varianceVals = signedDistances * (-1)

        self.minValcMap = self.varianceVals.min()
        self.maxValcMap = self.varianceVals.max()
        print("Finished distances...")

        # # trimesh closest point (ran for 30 min, didnt finish)
        # closetPt, distances, triIds = trimesh.proximity.closest_point(meshBottom, meshTopVerts)
        # test = mesh.ray.contains_points(points)
        # sign = (test.astype(int) * 2) - 1
        # signedDistances = sign * distance
        # refDf.loc[:, 'Dev'] = signedDistances

        # trimesh distance and vtx idx (only gets absolute distance)
        # SIMILAR TO PREPOST SEPARATION X,Y,Z DISTANCE (Pts to Pts - PREOPST GOES TO POINTS TO MESH)
        # EXACT same as scipy.spatial.cKDTree but a little slower
        # meshTopQuery = trimesh.proximity.ProximityQuery(meshTop)
        # distances2, vertexIDs = meshTopQuery.vertex(meshBottomVerts)
        # meshBottomDf.loc[:, 'Distances Trimesh'] = distances2
        # meshBottomDf.loc[:, 'Min ID Trimesh'] = vertexIDs

        # Get alignment and variance statistics
        averageDeviation = self.varianceVals.mean()
        stdDeviation = self.varianceVals.std()

        avgError = numpy.abs(self.varianceVals).sum() / len(self.varianceVals)
        rmsEstimate = numpy.sqrt((self.varianceVals ** 2).mean())

        return averageDeviation, stdDeviation, avgError, rmsEstimate

    @varianceUtilities.timer
    def save_var_results_as_file(self):

        # Make sure output directory is valid
        if not os.path.isdir(self.outputDir):
            os.makedirs(self.outputDir)

        self.varInfoDf = pandas.DataFrame(data=self.sampledPointsFinal, columns=['Ref X', 'Ref Y', 'Ref Z'])
        self.varInfoDf.loc[:, 'Deviation'] = self.varianceVals
        if self.sampledPointsFinalNormals is not None:
            self.varInfoDf.loc[:, 'Normal X'] = self.sampledPointsFinalNormals[:, 0]
            self.varInfoDf.loc[:, 'Normal Y'] = self.sampledPointsFinalNormals[:, 1]
            self.varInfoDf.loc[:, 'Normal Z'] = self.sampledPointsFinalNormals[:, 2]

        # remove duplicate coordinates from output file
        self.varInfoDf.drop_duplicates(inplace=True, subset=['Ref X', 'Ref Y', 'Ref Z'])

        self.varInfoDf = self.varInfoDf.round(3).astype(float)

        # Remove duplicates after rounding
        self.varInfoDf.drop_duplicates(inplace=True)

        self.varInfoDfFinal = self.varInfoDf.replace(numpy.inf, numpy.nan)
        self.varInfoDfFinal.dropna(inplace=True)
        self.varInfoDfFinal.reset_index(drop=True, inplace=True)
        self.varInfoDfFinal.sort_values(by=['Deviation', 'Ref Z', 'Ref Y', 'Ref X'],
                                        axis=0, inplace=True, ascending=True)

        if 'csv' in self.outputFormats:
            varInfoFile = os.path.join(self.outputDir, '%s.csv' % self.outputFileName)
            self.mainLogger.info('Saving Results to CSV format: %s' % varInfoFile)
            numpy.savetxt(varInfoFile, self.varInfoDfFinal.values,
                          delimiter=',', fmt="%10.4f",
                          header='Ref X,Ref Y,Ref Z,Deviation,Normal X, Normal Y, Normal Z')

        if 'txt' in self.outputFormats:
            varInfoFile = os.path.join(self.outputDir, '%s.txt' % self.outputFileName)
            self.mainLogger.info('Saving Results to TXT format: %s' % varInfoFile)
            numpy.savetxt(varInfoFile, self.varInfoDfFinal.values,
                          delimiter=',', fmt="%10.4f",
                          header='Ref X,Ref Y,Ref Z,Deviation,Normal X, Normal Y, Normal Z')

        @numba.jit(forceobj=True)
        def var_loop_formatting_numpy(varInfoList):
            for rowVals in self.varInfoDfFinal.values:
                xFmt = '{:0.3f}'.format(rowVals[0])
                yFmt = '{:0.3f}'.format(rowVals[1])
                zFmt = '{:0.3f}'.format(rowVals[2])
                devFmt = '{:0.3f}'.format(rowVals[3])
                lineFormatted = '{:>10}{:>10}{:>10}{:>10}'.format(xFmt, yFmt, zFmt, devFmt)
                varInfoList.append('%s' % str(lineFormatted))
            return varInfoList

        if 'var' in self.outputFormats:
            varInfo = []
            varInfo.append('# File: %s.var' % self.outputFileName)
            varInfo.append('# Format: Standard Variance File')
            varInfo.append('# Data sorted by deviation (min to max)')
            varInfo.append('# Reference File: %s' % os.path.basename(self.refMeshFile))
            varInfo.append('#        X         Y         Z Deviation')
            varInfo = var_loop_formatting_numpy(varInfo)
            # varInfo.append('\n')
            varInfoString = '\n'.join(varInfo)
            varFile = os.path.join(self.outputDir, '%s.var' % self.outputFileName)

            self.mainLogger.info('Saving Results to var format: %s' % varFile)
            with open(varFile, 'w') as fileObject:
                fileObject.write(varInfoString)
        return

    def receive_pause_resume(self, pauseBool):
        self.paused = pauseBool
        return

    def pause_processing(self):
        if self.pauseEvent is not None:
            while self.pauseEvent.is_set():
                time.sleep(1)
        return

    @varianceUtilities.timer
    def decimate_or_wrap_mesh(self):
        if self.referenceType == 'mesh':
            self.mainLogger.info('Decimating mesh file to visualization.')
            self.decimated3dMesh = self.ref3dMesh.simplify_quadric_decimation(
                target_number_of_triangles=(self.pointDensity * 3)
            )
            self.decimated3dMesh.compute_triangle_normals()
            self.decimated3dMesh.compute_vertex_normals()

            # https://stackoverflow.com/questions/56965268/how-do-i-convert-a-3d-point-cloud-ply-into-a-mesh-with-faces-and-vertices
            # # create the triangular mesh with the vertices and faces from open3d
            self.decimatedTriMesh = trimesh.Trimesh(vertices=numpy.asarray(self.decimated3dMesh.vertices),
                                                    faces=numpy.asarray(self.decimated3dMesh.triangles),
                                                    vertex_normals=numpy.asarray(self.decimated3dMesh.vertex_normals))
            self.decimatedTriMesh.fill_holes()
            if self.DEBUG:
                decMeshFile = os.path.join(DEBUG_SAVE_DIR, '_debug_decimated_ref.stl')
                # o3d.io.write_triangle_mesh(decMeshFile, self.decimated3dMesh)
                self.decimatedTriMesh.export(decMeshFile)
        else:
            pointsToWrap = o3d.geometry.PointCloud()

            pointsToWrapCoords = o3d.utility.Vector3dVector(self.sampledPointsFinal)
            pointsToWrap.points = pointsToWrapCoords

            ## has to contains normals
            if self.sampledPointsFinalNormals is not None:
                pointsToWrapNormals = o3d.utility.Vector3dVector(self.sampledPointsFinalNormals)
                pointsToWrap.normals = pointsToWrapNormals
            else:
                pointsToWrap.estimate_normals()

            # wrappedPoints, wrappedPtsDblVector = o3d.geometry.TriangleMesh.create_from_point_cloud_poisson(pointsToWrap, depth=8, width=0, scale=1.1, linear_fit=False, n_threads=- 1)
            meanDistance = numpy.asarray(pointsToWrap.compute_nearest_neighbor_distance())
            radii = o3d.utility.DoubleVector([meanDistance.min(), meanDistance.mean(), meanDistance.max()])
            wrappedPoints = o3d.geometry.TriangleMesh.create_from_point_cloud_ball_pivoting(pointsToWrap, radii)

            self.decimatedTriMesh = trimesh.Trimesh(vertices=numpy.asarray(wrappedPoints.vertices),
                                                    faces=numpy.asarray(wrappedPoints.triangles),
                                                    vertex_normals=numpy.asarray(wrappedPoints.vertex_normals))
            self.decimatedTriMesh.fill_holes()
            if self.DEBUG:
                wrappedMeshFile = os.path.join(DEBUG_SAVE_DIR, '_debug_wrapped_ref_pts.stl')
                self.decimatedTriMesh.export(wrappedMeshFile)
        return

    @varianceUtilities.timer
    def create_custom_colormap(self,
                               maxNominalControllerUser,
                               maxCriticalControllerUser,
                               minCriticalControllerUser,
                               minNominalControllerUser,
                               colorSegCountUser):

        red = numpy.array((255, 0, 0))
        yellow = numpy.array((255, 255, 0))
        green = numpy.array((0, 255, 0))
        cyan = numpy.array((0, 255, 255))
        blue = numpy.array((0, 0, 255))

        scaleCenter = (maxCriticalControllerUser + minCriticalControllerUser) / 2.0

        numColors = (colorSegCountUser - 1) / 2  # subtract 1 for green color, split into halves for other colors
        spacing = round(((float(maxNominalControllerUser) - float(maxCriticalControllerUser)) / numColors), 4)
        upperBound = numpy.linspace(maxNominalControllerUser, maxCriticalControllerUser, int(numColors) + 1).round(6)
        lowerBound = numpy.linspace(minCriticalControllerUser, minNominalControllerUser, int(numColors) + 1).round(6)
        bounds = list(upperBound) + [scaleCenter] + list(lowerBound)
        bounds.reverse()
        greenList = [(0, 1.0, 0), (0, 1.0, 0)]

        colorRGBIncr = int(255 / numColors)
        yellowToRed = [(yellow / 255).tolist()]
        cyanToBlue = [(cyan / 255).tolist()]
        iteratorList = [x + 1 for x in list(range(int(numColors - 2)))]
        for color in iteratorList:
            yTr0 = (yellow[0]) / 255.0
            yTr1 = (yellow[1] - (colorRGBIncr * color)) / 255.0
            yTr2 = (yellow[2]) / 255.0
            yellowToRed.append((yTr0, yTr1, yTr2))
            cTb0 = (cyan[0]) / 255.0
            cTb1 = (cyan[1] - (colorRGBIncr * color)) / 255.0
            cTb2 = (cyan[2]) / 255.0
            cyanToBlue.append((cTb0, cTb1, cTb2))
        yellowToRed.append((red / 255).tolist())
        cyanToBlue.append((blue / 255).tolist())
        cyanToBlue.reverse()
        allColors = cyanToBlue + greenList + yellowToRed

        cmap = matplotlib.colors.LinearSegmentedColormap.from_list('customScale', allColors, N=len(allColors))
        norm = matplotlib.colors.BoundaryNorm(bounds, cmap.N)

        darkRed = numpy.array([139, 0, 0])
        darkRedNormed = (darkRed / 255).tolist()
        cmap.set_over(darkRedNormed)

        darkBlue = numpy.array([0, 0, 139])
        darkBlueNomred = (darkBlue / 255).tolist()
        cmap.set_under(darkBlueNomred)

        return cmap, norm

    @varianceUtilities.timer
    def apply_heatmap_to_mesh(self, maxNom=1.5, maxCrit=0.1, minCrit=-0.1, minNom=-1.5, numSegs=13):
        # cmap = matplotlib.cm.get_cmap('hsv')
        # minVal = 0.3
        # maxVal = 1.9
        # norm = matplotlib.colors.Normalize(vmin=self.minValcMap, vmax=self.maxValcMap)

        custCmap, custNorm = self.create_custom_colormap(maxNominalControllerUser=maxNom,
                                                         maxCriticalControllerUser=maxCrit,
                                                         minCriticalControllerUser=minCrit,
                                                         minNominalControllerUser=minNom,
                                                         colorSegCountUser=numSegs)

        pointColors = custCmap(custNorm(self.varInfoDf.loc[:, 'Deviation'].values))
        pointColors = pointColors[:, :-1]

        decimatedTriMeshTree = spatial.cKDTree(self.sampledPointsFinal)
        pointsOnSurfDist, pointsOnSurfIdx = decimatedTriMeshTree.query(self.decimatedTriMesh.vertices, k=1)
        colorsForDecMesh = pointColors[pointsOnSurfIdx]

        # Trimesh color mesh object
        self.decimatedTriMesh.visual.vertex_colors = colorsForDecMesh

        if self.DEBUG:
            decMeshFile = os.path.join(DEBUG_SAVE_DIR, '_debug_decimated_ref_colored.ply')
            # o3d.io.write_triangle_mesh(decMeshFile, self.decimated3dMesh)
            self.decimatedTriMesh.export(decMeshFile)

        return

    @varianceUtilities.timer
    def main_variance_process(self):

        self.pause_processing()
        if self.statusSignal is not None:
            self.statusSignal.emit('Loading in Mesh Files.')
        self.load_mesh_files()
        if self.progressSignal is not None:
            self.progressSignal.emit(15, 'None')

        if self.referenceType == 'cad':
            self.geoPointFile = os.path.join(os.path.dirname(self.refMeshFile), '%s-%s.asc' %
                                             (os.path.splitext(os.path.basename(self.refMeshFile))[0],
                                              str(self.pointDensity))
                                             )
            varianceGeo.VarianceCalculator_Geo_MainProcess.main(importCadFile=self.refMeshFile,
                                                                numOfPts=self.pointDensity,
                                                                outputPtFile=self.geoPointFile,
                                                                geomagicExe=self.geomagicExe,
                                                                exportUnits=self.geoUnits,
                                                                progressSignal=self.progressSignal)
            self.sampledPointsFinal, self.sampledPointsFinalNormals = varianceUtilities.read_asc_pt_file(
                self.geoPointFile)
            if self.progressSignal is not None:
                self.progressSignal.emit(45, 'None')

        elif self.referenceType == 'mesh':
            self.pause_processing()
            if self.statusSignal is not None:
                self.statusSignal.emit('Sampling ref Vertices.')
            self.sample_ref_vertices()
            if self.progressSignal is not None:
                self.progressSignal.emit(30, 'None')

            self.pause_processing()
            if self.statusSignal is not None:
                self.statusSignal.emit('Finalizing Point Information.')
            self.finalize_pt_information()
            if self.progressSignal is not None:
                self.progressSignal.emit(45, 'None')

        elif self.referenceType == 'point':
            self.geoPointFile = self.refMeshFile
            if self.referenceExt == '.asc':
                self.sampledPointsFinal, self.sampledPointsFinalNormals = varianceUtilities.read_asc_pt_file(
                    self.geoPointFile)
            if self.referenceExt == '.txt':
                self.sampledPointsFinal, self.sampledPointsFinalNormals = varianceUtilities.read_txt_pt_file(
                    self.geoPointFile)
            if self.referenceExt == '.csv':
                self.sampledPointsFinal, self.sampledPointsFinalNormals = varianceUtilities.read_csv_pt_file(
                    self.geoPointFile)
            if self.referenceExt == '.var':  # no normal information
                self.sampledPointsFinal = varianceUtilities.read_var_pt_file(self.geoPointFile)
            if self.progressSignal is not None:
                self.progressSignal.emit(45, 'None')
        else:
            raise TypeError('Reference File Type not applicable.')

        self.pause_processing()
        if self.statusSignal is not None:
            self.statusSignal.emit('Computing variance.')
        self.compute_variance()
        if self.progressSignal is not None:
            self.progressSignal.emit(75, 'None')

        self.pause_processing()
        if self.statusSignal is not None:
            self.statusSignal.emit('Saving Results.')
        self.save_var_results_as_file()
        if self.progressSignal is not None:
            self.progressSignal.emit(100, 'None')

        self.decimate_or_wrap_mesh()

        self.apply_heatmap_to_mesh(maxNom=1.5, maxCrit=0.1, minCrit=-0.1, minNom=-1.5, numSegs=13)

        self.displayMeshSignal.emit(self.decimatedTriMesh)

        return


if __name__ == "__main__":

    debug = True

    import logging

    debugLogger = logging.getLogger()

    if debug:
        testMeshFile = r"C:\_Data\VarianceCalculator\example-data\test\20200616_QB04374-001_FHC_Europe_Fairy_800ml_PET_Outside_Wrapped_S1.stl"
        refMeshFile = r"C:\_Data\VarianceCalculator\example-data\reference\00001429714-001 Mercury_BO_800ml_SC27-6_ISBM (2)_rot_CAD_Aligned-60000-Millimeters.asc"
        referenceExt = '.asc'
        referenceType = 'point'
        pointDensity = 60000
        outputFormats = ['csv', 'var']
        samplingType = 'uniform'
        vertexNormalType = 'single'
        vertexNeighborSize = None
        outputDir = r"C:\_Data\VarianceCalculator\example-data"
        outputFileName = "{name}-{density}".format(
            name=os.path.splitext(os.path.basename(testMeshFile))[0],
            density=pointDensity)

        DEBUG_SAVE_DIR = outputDir

        varianceCalculator = VarianceCalculatorProcessor(mainLogger=debugLogger)
        varianceCalculator.testMeshFile = testMeshFile
        varianceCalculator.refMeshFile = refMeshFile
        varianceCalculator.referenceType = referenceType
        varianceCalculator.referenceExt = referenceExt
        varianceCalculator.pointDensity = pointDensity
        varianceCalculator.outputFormats = outputFormats
        varianceCalculator.samplingType = samplingType
        varianceCalculator.vertexNormalType = vertexNormalType
        varianceCalculator.vertexNeighborSize = vertexNeighborSize
        varianceCalculator.outputDir = outputDir
        varianceCalculator.outputFileName = outputFileName

        varianceCalculator.subdivideMesh = False
        varianceCalculator.varianceType = 'combined'
        varianceCalculator.DEBUG = debug

        varianceCalculator.main_variance_process()
        print('Complete!')


    else:

        varianceCalculator = VarianceCalculatorProcessor(mainLogger=debugLogger)
        varianceCalculator.samplingType = 'uniform'
        varianceCalculator.vertexNormalType = 'single'
        varianceCalculator.vertexNeighborSize = None
        varianceCalculator.subdivideMesh = False
        varianceCalculator.DEBUG = False

        # Create argument parser
        import argparse
        import textwrap

        # Create argument parser and add all necessary arguments
        parser = argparse.ArgumentParser(
            formatter_class=argparse.RawTextHelpFormatter,
            description='Executes variance Calculator Application backend code.',
            usage=textwrap.dedent('''\
                    [python] [script_full_filepath] [input_arguments]
                    To see help on valid input arguments, enter following line into Windows Command Prompt and press enter:
                    python "%s" --help''') % (sys.argv[0])
        )
        groupDeviceType = parser.add_argument_group(
            title='process type arguments',
            description=textwrap.dedent('''\
                    Important: this argument is required and only one type can be set at a time.
                    Specify the type of data you wish to process (matlab or proA).
                    Note: input_directory argument should only contain files from one type.''')
        )

        subMutGroup = groupDeviceType.add_mutually_exclusive_group(required=True)
        subMutGroup.add_argument(
            '-c', '--csv',
            dest='outputFileType', action='store_const', const=['csv'],
            help='set this type if the output files are to be .csv'
        )
        subMutGroup.add_argument(
            '-t', '--var',
            dest='outputFileType', action='store_const', const=['var'],
            help='set this type if the output files are to be .var'
        )
        subMutGroup.add_argument(
            '-b', '--both',
            dest='outputFileType', action='store_const', const=['csv', 'var'],
            help='set this type if the output files are to be .csv and .var'
        )

        """"""
        parser.add_argument(
            'pointDensity',
            metavar='point_density', type=int,
            help='the approximate desired number of points on the output file'
        )
        parser.add_argument(
            'testMeshFile',
            metavar='test_mesh', type=varianceUtilities.valid_file,
            help='the test mesh file as .stl / .ply / .obj'
        )
        parser.add_argument(
            'refMeshFile',
            metavar='ref_mesh', type=varianceUtilities.valid_file,
            help='the ref mesh file as .stl / .ply / .obj'
        )
        parser.add_argument(
            'outputDir',
            metavar='output_dir', type=varianceUtilities.valid_directory,
            help='the directory where the results files will be saved'
        )
        parser.add_argument(
            'varianceType',
            metavar='variance_type', type=str,
            help='the type of variance calculation to use ("Real" is more accurate method, "Ref" is Ref to Test only)'
        )
        numArgs = len(sys.argv[1:])

        # Process based on number of input args given
        if numArgs == 0:
            # Command line mode with prompts

            testMeshFileSet = False
            refMeshFileSet = False
            outputFileTypeSet = False
            pointDensitySet = False
            outputDirSet = False
            varianceTypeSet = False

            while not testMeshFileSet:
                testMeshFile = input('\nPlease enter the full filepath to the Test mesh file:\n').strip('\"')
                # Try to parse the inputs using the argumnet parser
                if os.path.isfile(testMeshFile):
                    testMeshFileSet = True
                else:
                    # This message will print after the error message returned by the argument parser.
                    print('\nInvalid user input. Is not a valid file!')

            while not refMeshFileSet:
                refMeshFile = input('\nPlease enter the full filepath to the Ref mesh file:\n').strip('\"')
                # Try to parse the inputs using the argumnet parser
                if os.path.isfile(refMeshFile):
                    refMeshFileSet = True
                else:
                    # This message will print after the error message returned by the argument parser.
                    print('\nInvalid user input. Is not a valid file!')

            VALID_OUTPUT_TYPES = ['csv', 'var', 'both']
            while not outputFileTypeSet:
                outputFileType = input('\nPlease enter the output file type csv, var, both:\n')
                # Try to parse the inputs using the argumnet parser
                if outputFileType in VALID_OUTPUT_TYPES:
                    if outputFileType == 'both':
                        outputFileType = ['csv', 'var']
                    outputFileTypeSet = True
                else:
                    # This message will print after the error message returned by the argument parser.
                    print('\nInvalid user input. Not a valid output type.')

            while not pointDensitySet:
                pointDensity = input('\nPlease enter the desired number of points in the output file:\n')
                # Try to parse the inputs using the argumnet parser
                try:
                    pointDensity = int(pointDensity)
                except:
                    # This message will print after the error message returned by the argument parser.
                    print('\nInvalid user input. Not a valid integer.')
                else:
                    pointDensitySet = True

            while not outputDirSet:
                outputDir = input('\nPlease enter the desired output directory:\n')
                # Try to parse the inputs using the argumnet parser
                if os.path.isdir(outputDir):
                    outputDirSet = True
                else:
                    # This message will print after the error message returned by the argument parser.
                    print('\nInvalid user input. Not a valid integer.')

            while not varianceTypeSet:
                varianceType = input(
                    '\nPlease enter the desired variance calculation type (real / outer / dist / combined):\n')
                # Try to parse the inputs using the argumnet parser
                if varianceType.lower() in ['real', 'outer', 'dist', 'combined']:
                    varianceTypeSet = True
                else:
                    # This message will print after the error message returned by the argument parser.
                    print('\nInvalid user input. Not a valid variance type.')

            varianceCalculator.testMeshFile = testMeshFile
            varianceCalculator.refMeshFile = refMeshFile
            varianceCalculator.pointDensity = pointDensity
            varianceCalculator.outputFormats = outputFileType
            varianceCalculator.outputDir = outputDir
            varianceCalculator.varianceType = varianceType
            varianceCalculator.outputFileName = "{name}-{density}".format(
                name=os.path.splitext(os.path.basename(refMeshFile))[0],
                density=pointDensity)
            varianceCalculator.main_variance_process()

        else:

            # Command line mode with arguments
            args = parser.parse_args()

            varianceCalculator.testMeshFile = args.testMeshFile
            varianceCalculator.refMeshFile = args.refMeshFile
            varianceCalculator.pointDensity = args.pointDensity
            varianceCalculator.outputFormats = args.outputFileType
            varianceCalculator.outputDir = args.outputDir
            varianceCalculator.varianceType = args.varianceType
            varianceCalculator.outputFileName = "{name}-{density}".format(
                name=os.path.splitext(os.path.basename(args.refMeshFile))[0],
                density=args.pointDensity)
            varianceCalculator.main_variance_process()

        print('Complete!')
