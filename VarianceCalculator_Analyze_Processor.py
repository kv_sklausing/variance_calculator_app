'''
Main Pre Mesh Analyze Control for VarianceCalculator.py.
'''


from PySide2.QtCore import QRunnable, Slot, QObject, Signal
import traceback
import trimesh
import os

import Utilities as varianceUtilities
from Utilities.VarianceCalculator_Exceptions import CustomReadFileError

PERCENTAGE_SAFETY_FACTOR = 50  # percent



class WorkerSignals(QObject):
    '''
    Defines the signals available from a running worker thread.

    Supported signals are:

    finished
        No data

    error
        tuple (exctype, value, traceback.format_exc() )

    result
        object data returned from processing, anything

    '''
    finished = Signal(dict)
    starting = Signal(dict)
    error = Signal(dict)
    result = Signal(dict)


class AnalyzeMesh(QRunnable):
    '''
    Class that handles processing the analysis routine in its own thread.
    '''

    def __init__(self, validAddtoTable, meshPairDict, mainLogger=None, parent=None):
        # Initialize the parent class.
        super().__init__()
        # Set thread name.
        self.name = 'AnalysisThread'
        self.parent = parent

        # Initialize variables.
        self.validAddtoTable = validAddtoTable
        self.meshPairDict = meshPairDict

        self.signals = WorkerSignals()

        self.pointDensity = None
        self.testMeshFile = None
        self.refMeshFile = None
        self.referenceType = None
        self.rowNumber = None

        self.testTriMesh = None
        self.refTriMesh = None

        self.analysisToolTip = ''
        self.analysisMessage = ''
        self.statusMessage = ''
        self.subdivide = False

        self.loadError = None

        self.mainLogger = mainLogger

        return


    def run(self):
        # Use try/except statement to catch an unexpected errors during analysis.
        # This ensures the app does not crash entirely during use.
        try:
            # Execute the analysis routine.
            self.set_parameters()
            self.mainLogger.info('Pre Analyzing Started for: {%s and %s}.' % (os.path.basename(self.testMeshFile),
                                                                           os.path.basename(self.refMeshFile)))
            self.signals.starting.emit(self.meshPairDict)
            self.load_routine()
            self.analysis_routine()
        except CustomReadFileError:
            self.mainLogger.exception('Error reading in file during pre-analysis check.')
            self.mainLogger.exception(self.loadError)
            errorTip = self.loadError.splitlines()[-1]
            self.error_occured(errorTip)
            self.signals.error.emit(self.meshPairDict)
        except:
            errorTraceback = traceback.format_exc()
            self.mainLogger.exception('Unexpected error encountered during pre-analysis check.')
            self.mainLogger.exception(errorTraceback)
            errorTip = errorTraceback.splitlines()[-1]
            self.error_occured(errorTip)
            self.signals.error.emit(self.meshPairDict)
        else:
            self.isAnalysisSuccess = True
            self.meshPairDict['Status'] = 'Ready'
            self.signals.finished.emit(self.meshPairDict)
            self.mainLogger.info('Pre Analyzing Complete for: {%s and %s}.' % (os.path.basename(self.testMeshFile),
                                                                           os.path.basename(self.refMeshFile)))
        return

    def analysis_routine(self):
        # Populate Checkmark or warning based on trimesh checks

        # point count check
        # checks Ref vertex count against desired point count
        self.point_count_check()

        # same file check
        # checks Test and Ref mesh file vertex count
        self.same_file_check()

        # bounding box check
        # checks bounding box volumes to ensure not significantly different
        self.bounding_box_volume_check()

        # area check
        # checks Test and Ref mesh surface area to ensure not significantly different
        self.area_check()

        # sample number match check
        # checks whether the two input files have same QB number and sample number
        # mainly used for internal projects
        self.same_sample_check()

        # used to check if test and ref files may have been accidentally entered backwards
        # and need swapped
        self.bounding_box_file_swap_check()

        # checks if test mesh file is closed manifold
        # needs to be closed or else trimesh cant determine if points lie inside/outside of mesh
        self.test_file_closed_manifold()

        # checks if ref mesh file is open manifold
        # ideally its open due to bottle opening surface being removed to
        # remove undesirable reference points to use as comparison points
        self.ref_file_open_manifold()

        return

    def point_count_check(self):
        self.mainLogger.info('Pre Analyzing Desired Point Count Check for: {%s and %s}.' % (os.path.basename(self.testMeshFile),
                                                                           os.path.basename(self.refMeshFile)))
        if isinstance(self.refTriMesh, trimesh.base.Trimesh) and len(self.refTriMesh.vertices) < self.pointDensity:
            self.meshPairDict['Analysis']['Density Check'] = {'Message': 'Warning', 'Pass': False}
            self.meshPairDict['Tooltip']['Density Check'] = 'Point Count Greater than ref mesh vertex count. Will automatically subdivide.'
            self.meshPairDict['Subdivide'] = True
        elif self.referenceType == 'cad':
            self.meshPairDict['Analysis']['Density Check'] = {'Message': 'Warning', 'Pass': False}
            self.meshPairDict['Tooltip']['Density Check'] = 'Reference CAD File will use 3rd party software for point sampling.'
            self.meshPairDict['Subdivide'] = False
        elif self.referenceType == 'point':
            self.geoPointFile = self.refTriMesh
            if self.referenceExt == '.asc':
                sampledPointsFinal, sampledPointsNormals = varianceUtilities.read_asc_pt_file(self.geoPointFile)
            elif self.referenceExt == '.txt':
                sampledPointsFinal, sampledPointsNormals = varianceUtilities.read_txt_pt_file(self.geoPointFile)
            elif self.referenceExt == '.csv':
                sampledPointsFinal, sampledPointsNormals = varianceUtilities.read_csv_pt_file(self.geoPointFile)
            elif self.referenceExt == '.var':
                sampledPointsFinal = varianceUtilities.read_var_pt_file(self.geoPointFile)
            else:
                self.loadError = 'Invalid point type for reference file.'
                raise CustomReadFileError

            refFileNumPts = len(sampledPointsFinal)
            if refFileNumPts != self.pointDensity:
                self.meshPairDict['Analysis']['Density Check'] = {'Message': 'Warning', 'Pass': False}
                self.meshPairDict['Tooltip']['Density Check'] = f'Using {int(refFileNumPts):,} points from reference file (differs from desired).'
                self.meshPairDict['Subdivide'] = False
                self.meshPairDict['Point Density'] = refFileNumPts
        else:
            self.meshPairDict['Analysis']['Density Check'] = {'Message': 'Passed', 'Pass': True}
            self.meshPairDict['Tooltip']['Density Check'] = ''
            self.meshPairDict['Subdivide'] = False
        return

    def same_file_check(self):
        self.mainLogger.info('Pre Analyzing Same File Check for: {%s and %s}.' % (os.path.basename(self.testMeshFile),
                                                                           os.path.basename(self.refMeshFile)))
        if isinstance(self.refTriMesh, trimesh.base.Trimesh) and (len(self.refTriMesh.vertices) == len(self.testTriMesh.vertices)):
            self.meshPairDict['Analysis']['File Check'] = {'Message': 'Warning', 'Pass': False}
            self.meshPairDict['Tooltip']['File Check'] = 'Ref and Test mesh files may be the same file.'
        elif self.referenceType == 'point':
            self.geoPointFile = self.refTriMesh
            if self.referenceExt == '.asc':
                sampledPointsFinal, sampledPointsNormals = varianceUtilities.read_asc_pt_file(self.geoPointFile)
            elif self.referenceExt == '.txt':
                sampledPointsFinal, sampledPointsNormals = varianceUtilities.read_txt_pt_file(self.geoPointFile)
            elif self.referenceExt == '.csv':
                sampledPointsFinal, sampledPointsNormals = varianceUtilities.read_csv_pt_file(self.geoPointFile)
            elif self.referenceExt == '.var':
                sampledPointsFinal = varianceUtilities.read_var_pt_file(self.geoPointFile)
            else:
                self.loadError = 'Invalid point type for reference file.'
                raise CustomReadFileError

            refFileNumPts = len(sampledPointsFinal)
            if refFileNumPts == len(self.testTriMesh.vertices):
                self.meshPairDict['Analysis']['File Check'] = {'Message': 'Warning', 'Pass': False}
                self.meshPairDict['Tooltip']['File Check'] = 'Reference Points and Test Mesh Vertex Count are the same.'

        else:  # pass if CAD option
            self.meshPairDict['Analysis']['File Check'] = {'Message': 'Passed', 'Pass': True}
            self.meshPairDict['Tooltip']['File Check'] = ''
        return

    def bounding_box_volume_check(self):
        self.mainLogger.info('Pre Analyzing Bounding Box Check for: {%s and %s}.' % (os.path.basename(self.testMeshFile),
                                                                           os.path.basename(self.refMeshFile)))
        # get Test mesh bounding box volume
        TestMinBounds = self.testTriMesh.bounds[0]
        TestMaxBounds = self.testTriMesh.bounds[1]
        TestXBboxDim = TestMaxBounds[0] - TestMinBounds[0]
        TestYBboxDim = TestMaxBounds[1] - TestMinBounds[1]
        TestZBboxDim = TestMaxBounds[2] - TestMinBounds[2]
        TestMeshBboxVolume = TestXBboxDim * TestYBboxDim * TestZBboxDim

        if isinstance(self.refTriMesh, trimesh.base.Trimesh):
            # get Ref mesh bounding box volume
            RefMinBounds = self.refTriMesh.bounds[0]
            RefMaxBounds = self.refTriMesh.bounds[1]
            RefXBboxDim = RefMaxBounds[0] - RefMinBounds[0]
            RefYBboxDim = RefMaxBounds[1] - RefMinBounds[1]
            RefZBboxDim = RefMaxBounds[2] - RefMinBounds[2]
            RefMeshBboxVolume = RefXBboxDim * RefYBboxDim * RefZBboxDim

            bboxPercentage = (RefMeshBboxVolume / TestMeshBboxVolume) * 100

            # Factor of safety: 50 %
            if bboxPercentage <= PERCENTAGE_SAFETY_FACTOR or bboxPercentage >= (100 + PERCENTAGE_SAFETY_FACTOR):
                self.meshPairDict['Analysis']['Bbox Check'] = {'Message': 'Warning', 'Pass': False}
                self.meshPairDict['Tooltip']['Bbox Check'] = 'Ref and Test mesh bounding box sizes are significantly different. - Please confirm files.'

        elif self.referenceType == 'point':
            self.geoPointFile = self.refTriMesh
            if self.referenceExt == '.asc':
                sampledPointsFinal, sampledPointsNormals = varianceUtilities.read_asc_pt_file(self.geoPointFile)
            elif self.referenceExt == '.txt':
                sampledPointsFinal, sampledPointsNormals = varianceUtilities.read_txt_pt_file(self.geoPointFile)
            elif self.referenceExt == '.csv':
                sampledPointsFinal, sampledPointsNormals = varianceUtilities.read_csv_pt_file(self.geoPointFile)
            elif self.referenceExt == '.var':
                sampledPointsFinal = varianceUtilities.read_var_pt_file(self.geoPointFile)
            else:
                self.loadError = 'Invalid point type for reference file.'
                raise CustomReadFileError

            refPtCloud = trimesh.PointCloud(sampledPointsFinal)
            # get Ref mesh bounding box volume
            RefMinBounds = refPtCloud.bounds[0]
            RefMaxBounds = refPtCloud.bounds[1]
            RefXBboxDim = RefMaxBounds[0] - RefMinBounds[0]
            RefYBboxDim = RefMaxBounds[1] - RefMinBounds[1]
            RefZBboxDim = RefMaxBounds[2] - RefMinBounds[2]
            RefMeshBboxVolume = RefXBboxDim * RefYBboxDim * RefZBboxDim

            bboxPercentage = (RefMeshBboxVolume / TestMeshBboxVolume) * 100

            # Factor of safety: 50 %
            if bboxPercentage <= PERCENTAGE_SAFETY_FACTOR or bboxPercentage >= (100 + PERCENTAGE_SAFETY_FACTOR):
                self.meshPairDict['Analysis']['Bbox Check'] = {'Message': 'Warning', 'Pass': False}
                self.meshPairDict['Tooltip']['Bbox Check'] = 'Ref and Test mesh bounding box sizes are significantly different. - Please confirm files.'

        else:  # pass if CAD option
            self.meshPairDict['Analysis']['Bbox Check'] = {'Message': 'Passed', 'Pass': True}
            self.meshPairDict['Tooltip']['Bbox Check'] = ''
        return

    def area_check(self):
        self.mainLogger.info('Pre Analyzing Surface Area Check for: {%s and %s}.' % (os.path.basename(self.testMeshFile),
                                                                           os.path.basename(self.refMeshFile)))
        TestMeshArea = self.testTriMesh.area

        if isinstance(self.refTriMesh, trimesh.base.Trimesh):
            RefMeshArea = self.refTriMesh.area

            areaPercentage = (RefMeshArea / TestMeshArea) * 100

            # Factor of safety: 50 %
            if areaPercentage <= PERCENTAGE_SAFETY_FACTOR or areaPercentage >= (100 + PERCENTAGE_SAFETY_FACTOR):
                self.meshPairDict['Analysis']['Area Check'] = {'Message': 'Warning', 'Pass': False}
                self.meshPairDict['Tooltip']['Area Check'] = 'Ref and Test mesh surface areas are significantly different. - Please confirm files.'
        else:  # cad or point pass
            self.meshPairDict['Analysis']['Area Check'] = {'Message': 'Passed', 'Pass': True}
            self.meshPairDict['Tooltip']['Area Check'] = ''
        return

    def same_sample_check(self):
        # Used for internal QB#-Sample# Check to make sure same samples are selected for test and ref files
        self.mainLogger.info('Pre Analyzing Sample Match Check for: {%s and %s}.' % (os.path.basename(self.testMeshFile),
                                                                           os.path.basename(self.refMeshFile)))

        testFileName = os.path.basename(self.testMeshFile).lower()
        testQbStart = testFileName.find('qb0')
        testQbFull = ''
        if testQbStart != -1:  # qb0 found in filename
            testQbBase = testFileName[testQbStart:testQbStart + 3]
            testQbLong = testFileName[testQbStart + 3: testQbStart + 7]
            testQbNum = '%s%s' % (testQbBase, testQbLong)
            testQbNumLen = len(testQbNum)
            testSampleStart = testFileName.find(testQbNum)
            testQbSample = testFileName[testSampleStart + testQbNumLen + 1: testSampleStart + testQbNumLen + 4]
            testQbFull = '%s-%s' % (testQbNum, testQbSample)

        if isinstance(self.refTriMesh, trimesh.base.Trimesh):
            refFileName = os.path.basename(self.refMeshFile).lower()
            refQbStart = refFileName.find('qb0')
            refQbFull = ''
            if refQbStart != -1:  # qb0 found in filename
                refQbBase = refFileName[refQbStart:refQbStart + 3]
                refQbLong = refFileName[refQbStart + 3:refQbStart + 7]
                refQbNum = '%s%s' % (refQbBase, refQbLong)
                refQbLen = len(refQbNum)
                refSampleStart = refFileName.find(refQbNum)
                refQbSample = refFileName[refSampleStart + refQbLen + 1: refSampleStart + refQbLen + 4]
                refQbFull = '%s-%s' % (refQbNum, refQbSample)

            # Qb# and Sample # checks
            if testQbFull != refQbFull:
                self.meshPairDict['Analysis']['Sample Check'] = {'Message': 'Warning', 'Pass': False}
                self.meshPairDict['Tooltip']['Sample Check'] = 'Ref and Test mesh files may be from different samples. - Please confirm files.'
        else:  # cad and point pass
            self.meshPairDict['Analysis']['Sample Check'] = {'Message': 'Passed', 'Pass': True}
            self.meshPairDict['Tooltip']['Sample Check'] = ''

        return

    def bounding_box_file_swap_check(self):
        # Used for bottles mostly to check if test and ref files were accidentally flipped when selected
        self.mainLogger.info('Pre Analyzing File Swap Check for: {%s and %s}.' % (os.path.basename(self.testMeshFile),
                                                                           os.path.basename(self.refMeshFile)))

        # get Test mesh bounding box volume
        TestMinBounds = self.testTriMesh.bounds[0]
        TestMaxBounds = self.testTriMesh.bounds[1]
        TestXBboxDim = TestMaxBounds[0] - TestMinBounds[0]
        TestYBboxDim = TestMaxBounds[1] - TestMinBounds[1]
        TestZBboxDim = TestMaxBounds[2] - TestMinBounds[2]
        TestMeshBboxVolume = TestXBboxDim * TestYBboxDim * TestZBboxDim

        if isinstance(self.refTriMesh, trimesh.base.Trimesh):
            # get Ref mesh bounding box volume
            RefMinBounds = self.refTriMesh.bounds[0]
            RefMaxBounds = self.refTriMesh.bounds[1]
            RefXBboxDim = RefMaxBounds[0] - RefMinBounds[0]
            RefYBboxDim = RefMaxBounds[1] - RefMinBounds[1]
            RefZBboxDim = RefMaxBounds[2] - RefMinBounds[2]
            RefMeshBboxVolume = RefXBboxDim * RefYBboxDim * RefZBboxDim

            # Qb# and Sample # checks
            if TestMeshBboxVolume > RefMeshBboxVolume:
                self.meshPairDict['Analysis']['Swap Check'] = {'Message': 'Warning', 'Pass': False}
                self.meshPairDict['Tooltip']['Swap Check'] = 'Ref and Test mesh files may be swapped. - Please confirm files.'
        else:  # cad and point pass
            self.meshPairDict['Analysis']['Swap Check'] = {'Message': 'Passed', 'Pass': True}
            self.meshPairDict['Tooltip']['Swap Check'] = ''
        return

    def test_file_closed_manifold(self):

        self.mainLogger.info('Pre Analyzing Test File Closed Manifold Check for: {%s and %s}.' % (os.path.basename(self.testMeshFile),
                                                                                                  os.path.basename(self.refMeshFile)))
        if isinstance(self.testTriMesh, trimesh.base.Trimesh):
            if not self.testTriMesh.fill_holes():  # returns true if watertight after fill holes command
                self.meshPairDict['Analysis']['Test Manifold Check'] = {'Message': 'Warning', 'Pass': False}
                self.meshPairDict['Tooltip']['Test Manifold Check'] = 'Test Mesh File is not a closed manifold object. - Please fill all holes manually.'
                self.loadError = 'Test Mesh File is not a closed manifold object. - Please fill all holes manually.'
                raise CustomReadFileError
            else:
                self.meshPairDict['Analysis']['Test Manifold Check'] = {'Message': 'Passed', 'Pass': True}
                self.meshPairDict['Tooltip']['Test Manifold Check'] = ''

        return

    def ref_file_open_manifold(self):

        self.mainLogger.info('Pre Analyzing Ref File Open Manifold Check for: {%s and %s}.' % (os.path.basename(self.testMeshFile),
                                                                                              os.path.basename(self.refMeshFile)))
        if isinstance(self.refTriMesh, trimesh.base.Trimesh):
            if self.refTriMesh.fill_holes():  # returns true if watertight after fill holes command
                self.meshPairDict['Analysis']['Ref Manifold Check'] = {'Message': 'Warning', 'Pass': False}
                self.meshPairDict['Tooltip']['Ref Manifold Check'] = 'Ref Mesh File is a closed manifold object. - Please open top flat (undesired reference points).'
            else:
                self.meshPairDict['Analysis']['Ref Manifold Check'] = {'Message': 'Passed', 'Pass': True}
                self.meshPairDict['Tooltip']['Ref Manifold Check'] = ''

        return

    def load_routine(self):

        if self.validAddtoTable:
            if self.referenceType == 'mesh':
                self.meshPairDictLoad = {'Test Mesh File': self.testMeshFile,
                                        'Ref Mesh File': self.refMeshFile}
            else:
                self.meshPairDictLoad = {'Test Mesh File': self.testMeshFile}

            self.testTriMesh, self.refTriMesh = varianceUtilities.start_mesh_load_thread(self.meshPairDictLoad)
            if not isinstance(self.testTriMesh, trimesh.base.Trimesh):
                self.loadError = self.testTriMesh
                raise CustomReadFileError
            if self.refTriMesh is None:
                self.refTriMesh = self.refMeshFile
                if not self.parent.cadApp:
                    self.loadError = 'No CAD Package Available for Reference File'
                    raise CustomReadFileError
        return

    def set_parameters(self):
        self.pointDensity = self.meshPairDict['Point Density']
        self.testMeshFile = self.meshPairDict['Test Mesh File']
        self.refMeshFile = self.meshPairDict['Ref Mesh File']
        self.referenceType = self.meshPairDict['Reference File Type']
        self.referenceExt = self.meshPairDict['Reference File Ext']

        self.meshPairDict['Status'] = 'Analyzing...'
        self.meshPairDict['Analysis'] = {
            'Start':
                {'Message': 'Analyzing...', 'Pass': True},
            'Density Check':
                {'Message': 'Analyzing...', 'Pass': True},
            'File Check':
                {'Message': 'Analyzing...', 'Pass': True},
            'Bbox Check':
                {'Message': 'Analyzing...', 'Pass': True},
            'Area Check':
                {'Message': 'Analyzing...', 'Pass': True},
            'Sample Check':
                {'Message': 'Analyzing...', 'Pass': True},
            'Swap Check':
                {'Message': 'Analyzing...', 'Pass': True},
            'Test Manifold Check':
                {'Message': 'Analyzing...', 'Pass': True},
            'Ref Manifold Check':
                {'Message': 'Analyzing...', 'Pass': True},
            'Error':
                {'Message': 'Analyzing...', 'Pass': True}
        }
        self.meshPairDict['Tooltip'] = {
            'Start': '',
            'Density Check': '',
            'File Check': '',
            'Bbox Check': '',
            'Area Check': '',
            'Sample Check': '',
            'Swap Check': '',
            'Test Manifold Check': '',
            'Ref Manifold Check': '',
            'Error': ''
        }
        self.meshPairDict['Subdivide'] = False
        return

    def error_occured(self, errorMsg):
        self.meshPairDict['Status'] = 'Error'
        self.meshPairDict['Analysis']['Error'] = {'Message': 'Error', 'Pass': False}
        self.meshPairDict['Tooltip']['Error'] = errorMsg
        self.meshPairDict['Subdivide'] = False
        return
