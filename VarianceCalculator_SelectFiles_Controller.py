'''
Main Select Entry Files Control for VarianceCalculator.py.
'''

from PySide2.QtWidgets import QTableWidgetItem, QDialog, QWhatsThis, QLabel
from PySide2.QtCore import Signal, QEvent
from PySide2.QtGui import QIcon

import Utilities as VarianceUtilities

from UserInterface import VarianceCalculator_Styles as KV_Stylist
from UserInterface.Windows.VarianceCalculator_SelectFiles_Window import VarianceVisionSelectFilesGUI
import VarianceCalculator_SortFiles_Controller
from UserInterface.VarianceCalculator_Assets import *
from UserInterface import VarianceCalculator_Widgets
import numpy

if getattr(sys, 'frozen', False):
    # frozen
    SCRIPT_FULLPATH = sys.executable
else:
    # unfrozen
    SCRIPT_FULLPATH = __file__
SCRIPT_DIRECTORY = os.path.dirname(SCRIPT_FULLPATH)

DEFAULT_INPUT_DIR = SCRIPT_DIRECTORY

try:
    # Get User documents folder
    import ctypes.wintypes

    CSIDL_PERSONAL = 5  # My Documents
    SHGFP_TYPE_CURRENT = 1  # Get current, not default value
    userDocuments = ctypes.create_unicode_buffer(ctypes.wintypes.MAX_PATH)
    ctypes.windll.shell32.SHGetFolderPathW(None, CSIDL_PERSONAL, None, SHGFP_TYPE_CURRENT, userDocuments)
    userDocumentsPath = userDocuments.value
except:
    userDocumentsPath = r'C:\Users\Public\Documents'

# Set Defaults
DEFAULT_OUTPUT_DIR = userDocumentsPath
DEFAULT_POINT_DENSITY = 60000  # 60,000 point density typical for VAR files
DEFAULT_OUTPUT_TYPE_INDEX = 0  # First Index, CSV type
DEFAULT_GEO_UNITS_INDEX = 1  # Second Index, Millimeters
CAD_FILE_LIST = ['.stp', '.step', '.igs']
MESH_FILE_LIST = ['.stl', '.ply', '.obj']
POINT_FILE_LIST = ['.csv', '.txt', '.var', '.asc']


class VarianceCalculatorSelectFilesController(VarianceVisionSelectFilesGUI):
    '''
    Class for the Variance Calculator Select Files Dialog.
    '''

    signalConfirmedPairs = Signal()

    def __init__(self, parent=None, iconPath32=None, iconPath48=None,
                 alertIcon54=None, addPairSignal=None, appEnv=None, mainLogger=None):
        # Initialize the parent classes.
        super().__init__()

        # Set Parent Window
        self.parent = parent

        self.iconPath32 = iconPath32
        self.iconPath48 = iconPath48
        self.alertIcon54 = alertIcon54
        self.appEnv = appEnv

        self.mainLogger = mainLogger

        # Initialize window properties.
        self.init_window()

        # Initialize window settings.
        self.init_window_settings()

        # Apply settings for the window.
        self.apply_window_settings()

        # Initialize widget actions.
        self.init_widget_actions()

        # Initialize Signal Actions
        self.init_signal_actions()

        # Set Signal Relay
        self.addPairSignal = addPairSignal

        # Initialize Sort Files Controller
        self.sortFilesController = VarianceCalculator_SortFiles_Controller.SortFilesController(
            parent=self,
            confirmedPairsSignal=self.signalConfirmedPairs,
            iconPath32=self.iconPath32,
            mainLogger=self.mainLogger)

        return

    def event(self, event):
        if event.type() == QEvent.EnterWhatsThisMode:
            QWhatsThis.leaveWhatsThisMode()
            self.parent.menu_bar_file_how_to()
            return True
        return QDialog.event(self, event)

    def init_window(self):
        # Set some options for the window.
        self.setWindowTitle('Select Mesh Files')
        self.setWindowIcon(QIcon(self.iconPath32))
        self.setFixedSize(self.size())
        self.setWindowModality(Qt.ApplicationModal)

        if self.appEnv == 'PROD':
            self.comboBoxOutputType.addItem('CSV')
            self.comboBoxOutputType.addItem('TXT')

        if self.appEnv == 'DEV':
            self.comboBoxOutputType.addItem('CSV')
            self.comboBoxOutputType.addItem('VAR')
            self.comboBoxOutputType.addItem('CSV / VAR')

        self.comboBoxGeoUnits.addItem('Inches')
        self.comboBoxGeoUnits.addItem('Millimeters')
        self.comboBoxGeoUnits.addItem('Meters')

        return

    def init_signal_actions(self):
        self.signalConfirmedPairs.connect(
            self.sorted_files_confirmed
        )

    def init_window_settings(self):
        # Create settings for window.
        self.settingsSelectFiles = VarianceUtilities.WindowSettings('SelectFilesSettings')

        self.settingsSelectFiles.defaultInputDir = DEFAULT_INPUT_DIR

        # test and ref Mesh Files
        self.settingsSelectFiles.userTestFile = None
        self.settingsSelectFiles.userRefFile = None

        # test and ref Mesh Folders
        self.settingsSelectFiles.usertestDir = None
        self.settingsSelectFiles.userrefDir = None

        # Pt Density
        self.settingsSelectFiles.defaultPointDensity = DEFAULT_POINT_DENSITY
        self.settingsSelectFiles.userPointDensity = None

        # Output Directory
        self.settingsSelectFiles.defaultOutputDirectory = DEFAULT_OUTPUT_DIR
        self.settingsSelectFiles.userOutputDirectory = None

        # Output Type
        self.settingsSelectFiles.defaultOutputTypeIdx = DEFAULT_OUTPUT_TYPE_INDEX
        self.settingsSelectFiles.userOutputTypeIdx = None

        # comboBoxGeoUnits
        self.settingsSelectFiles.defaultGeoUnitsIdx = DEFAULT_GEO_UNITS_INDEX
        self.settingsSelectFiles.userGeoUnitsIdx = None

        # Other information for each time dialog runs
        self.addPairDict = dict()
        self.addPairConfirmed = dict()

        self.sortedFiles = False
        self.testDirSet = False
        self.refDirSet = False
        self.outputDirSet = False
        self.testFileSet = False
        self.refFileSet = False

        self.emptyRow = 0

        self.editingRow = None
        self.editingData = None
        self.previousData = None

        self.refFileExtList = list()

        return

    def apply_window_settings(self):
        """ Apply setting to widgets for the window."""
        # Widget: lineEdittestFile
        if self.settingsSelectFiles.userTestFile is not None and os.path.isfile(
                self.settingsSelectFiles.userTestFile):
            self.lineEdittestFile.setText(
                self.settingsSelectFiles.userTestFile)
        else:
            self.lineEdittestFile.setText(
                self.settingsSelectFiles.defaultInputDir)
        # Widget: lineEditRefFile
        if self.settingsSelectFiles.userRefFile is not None and os.path.isfile(
                self.settingsSelectFiles.userRefFile):
            self.lineEditRefFile.setText(
                self.settingsSelectFiles.userRefFile)
        else:
            self.lineEditRefFile.setText(
                self.settingsSelectFiles.defaultInputDir)

        # Widget: lineEdittestDir
        if self.settingsSelectFiles.usertestDir is not None and os.path.isdir(
                self.settingsSelectFiles.usertestDir):
            self.lineEdittestDir.setText(
                self.settingsSelectFiles.usertestDir)
        else:
            self.lineEdittestDir.setText(
                self.settingsSelectFiles.defaultInputDir)

        # Widget: lineEditRefDir
        if self.settingsSelectFiles.userrefDir is not None and os.path.isdir(
                self.settingsSelectFiles.userrefDir):
            self.lineEditRefDir.setText(
                self.settingsSelectFiles.userrefDir)
        else:
            self.lineEditRefDir.setText(
                self.settingsSelectFiles.defaultInputDir)

        # Widget: userPointDensity
        if self.settingsSelectFiles.userPointDensity is not None and isinstance(
                self.settingsSelectFiles.userPointDensity, int):
            self.spinBoxPointDensity.setValue(
                self.settingsSelectFiles.userPointDensity)
        else:
            self.spinBoxPointDensity.setValue(
                self.settingsSelectFiles.defaultPointDensity)

        # Widget: userOutputDirectory
        if self.settingsSelectFiles.userOutputDirectory is not None and os.path.isdir(
                self.settingsSelectFiles.userOutputDirectory):
            self.lineEditOutputDir.setText(
                self.settingsSelectFiles.userOutputDirectory)
        else:
            self.lineEditOutputDir.setText(
                self.settingsSelectFiles.defaultOutputDirectory)
        self.outputDirSet = True

        # Widget: userOutputTypeIdx
        if self.appEnv == 'DEV':
            if self.settingsSelectFiles.userOutputTypeIdx is not None and isinstance(
                    self.settingsSelectFiles.userOutputTypeIdx, int):
                self.comboBoxOutputType.setCurrentIndex(
                    self.settingsSelectFiles.userOutputTypeIdx)
            else:
                self.comboBoxOutputType.setCurrentIndex(
                    self.settingsSelectFiles.defaultOutputTypeIdx)
        else:
            if self.settingsSelectFiles.userOutputTypeIdx is not None and isinstance(
                    self.settingsSelectFiles.userOutputTypeIdx, int):
                # Reset to 0 if the index doesnt exist in the PROD App
                if self.settingsSelectFiles.userOutputTypeIdx > 1:
                    self.settingsSelectFiles.userOutputTypeIdx = 0
                self.comboBoxOutputType.setCurrentIndex(
                    self.settingsSelectFiles.userOutputTypeIdx)
            else:
                self.comboBoxOutputType.setCurrentIndex(
                    self.settingsSelectFiles.defaultOutputTypeIdx)
        self.set_output_type()

        # Widget: comboBoxGeoUnits
        if self.settingsSelectFiles.userGeoUnitsIdx is not None and isinstance(
                self.settingsSelectFiles.userGeoUnitsIdx, int):
            # Reset to 0 if the index doesnt exist in the PROD App
            if self.settingsSelectFiles.userGeoUnitsIdx > 2:
                self.settingsSelectFiles.userGeoUnitsIdx = 0
            self.comboBoxGeoUnits.setCurrentIndex(
                self.settingsSelectFiles.userGeoUnitsIdx)
        else:
            self.comboBoxGeoUnits.setCurrentIndex(
                self.settingsSelectFiles.defaultGeoUnitsIdx)
        self.set_geo_output_units()

        return

    def store_window_settings(self):
        """ Retrieve the settings from the widgets for the window."""
        # Widget: lineEdittestFile
        self.settingsSelectFiles.userTestFile = self.lineEdittestFile.text()

        # Widget: lineEditRefFile
        self.settingsSelectFiles.userRefFile = self.lineEditRefFile.text()

        # Widget: lineEdittestDir
        self.settingsSelectFiles.usertestDir = self.lineEdittestDir.text()

        # Widget: lineEditRefDir
        self.settingsSelectFiles.userrefDir = self.lineEditRefDir.text()

        # Widget: userPointDensity
        self.settingsSelectFiles.userPointDensity = self.spinBoxPointDensity.value()

        # Widget: userOutputDirectory
        self.settingsSelectFiles.userOutputDirectory = self.lineEditOutputDir.text()

        # Widget: comboBoxGeoUnits
        self.set_geo_output_units()

        # Widget: comboBoxOutputType
        self.set_output_type()

        # no widget, get ref file type from ref file
        self.set_ref_file_type()

        if self.addPairType == 'dir' and self.close_valid:
            self.get_test_mesh_files(self.settingsSelectFiles.usertestDir)
            self.get_ref_mesh_files(self.settingsSelectFiles.userrefDir)

        return

    def set_output_type(self):
        userOutputTypeIdx = self.comboBoxOutputType.currentIndex()
        self.settingsSelectFiles.userOutputTypeIdx = userOutputTypeIdx
        if self.appEnv == 'PROD':
            if userOutputTypeIdx == 0:
                self.userOutputType = ['csv']
            elif userOutputTypeIdx == 1:
                self.userOutputType = ['txt']
            else:
                self.userOutputType = ['csv']

        if self.appEnv == 'DEV':
            if userOutputTypeIdx == 0:
                self.userOutputType = ['csv']
            elif userOutputTypeIdx == 1:
                self.userOutputType = ['var']
            elif userOutputTypeIdx == 2:
                self.userOutputType = ['csv', 'var']
            else:
                self.userOutputType = ['csv']
        return

    def set_geo_output_units(self):
        userGeoUnitsIdx = self.comboBoxGeoUnits.currentIndex()
        self.settingsSelectFiles.userGeoUnitsIdx = userGeoUnitsIdx
        if userGeoUnitsIdx == 0:
            self.userGeoUnits = 'Inches'
        elif userGeoUnitsIdx == 1:
            self.userGeoUnits = 'Millimeters'
        elif userGeoUnitsIdx == 2:
            self.userGeoUnits = 'Meters'
        else:
            self.userGeoUnits = 'Millimeters'
        return

    def set_ref_file_type(self):
        self.refFileExt = os.path.splitext(os.path.basename(self.settingsSelectFiles.userRefFile))[-1]
        if self.refFileExt in CAD_FILE_LIST:
            self.refFileType = 'cad'
        elif self.refFileExt in MESH_FILE_LIST:
            self.refFileType = 'mesh'
        elif self.refFileExt in POINT_FILE_LIST:
            self.refFileType = 'point'
        else:
            self.refFileType = 'n/a'
        return

    def set_ref_dir_type(self, inputFile):
        self.refFileExt = os.path.splitext(os.path.basename(inputFile))[-1]
        if self.refFileExt in CAD_FILE_LIST:
            self.refFileType = 'cad'
        elif self.refFileExt in MESH_FILE_LIST:
            self.refFileType = 'mesh'
        elif self.refFileExt in POINT_FILE_LIST:
            self.refFileType = 'point'
        else:
            self.refFileType = 'n/a'

    def init_widget_actions(self):
        # Connect the widgets to their actions.
        """ Switch input type from multiple to single file"""
        self.pushButtonSelectSingleFile.clicked.connect(
            self.reset_single_file_selection
        )

        self.pushButtonSelectMultiFile.clicked.connect(
            self.reset_multi_file_selection
        )

        """ Button Inputs that open file / directory dialogs to select"""
        self.pushButtonSelectTestFile.clicked.connect(
            self.select_test_file_action
        )
        self.pushButtonSelectTestDir.clicked.connect(
            self.select_test_dir_action
        )

        self.pushButtonSelectRefFile.clicked.connect(
            self.select_ref_file_action
        )
        self.pushButtonSelectRefDir.clicked.connect(
            self.select_ref_dir_action
        )
        """ The icon button to change the original selection """
        self.lineEdittestFile.lineEditIcon.clicked.connect(
            self.select_test_file_action
        )
        self.lineEdittestDir.lineEditIcon.clicked.connect(
            self.select_test_dir_action
        )
        self.lineEditRefFile.lineEditIcon.clicked.connect(
            self.select_ref_file_action
        )
        self.lineEditRefDir.lineEditIcon.clicked.connect(
            self.select_ref_dir_action
        )
        self.lineEditOutputDir.lineEditIcon.clicked.connect(
            self.select_output_dir_action
        )
        """ Change Point Density"""
        self.spinBoxPointDensity.valueChanged.connect(
            self.set_point_density
        )

        """ Processing Start Action """
        self.pushButtonSaveDirs.clicked.connect(
            self.closeParametersOkay
        )

        """ Processing Start Action """
        self.pushButtonSaveFiles.clicked.connect(
            self.closeParametersOkay
        )

        self.pushButtonSortFiles.clicked.connect(
            self.sort_input_files
        )
        return

    def setup_single_file_ui(self):
        # self.previousData = None
        self.reset_single_file_selection()

        # Single File Entry
        if self.editingData is not None:
            self.settingsSelectFiles.userTestFile = self.editingData['Test Mesh File']
            self.settingsSelectFiles.userRefFile = self.editingData['Ref Mesh File']
            self.settingsSelectFiles.userPointDensity = self.editingData['Point Density']
            self.settingsSelectFiles.userOutputDirectory = self.editingData['Output Dir']
            self.settingsSelectFiles.userOutputTypeIdx = self.editingData['Output File Type Idx']
            self.settingsSelectFiles.userGeoUnitsIdx = self.editingData['Geo Units Idx']

            self.update_test_file_widget(TestFilePath=self.settingsSelectFiles.userTestFile)
            self.update_ref_file_widget(RefFilePath=self.settingsSelectFiles.userRefFile)
            self.spinBoxPointDensity.setValue(self.settingsSelectFiles.userPointDensity)
            self.lineEditOutputDir.setText(self.settingsSelectFiles.userOutputDirectory)
            self.comboBoxOutputType.setCurrentIndex(self.settingsSelectFiles.userOutputTypeIdx)
            self.comboBoxGeoUnits.setCurrentIndex(self.settingsSelectFiles.userGeoUnitsIdx)

            self.pushButtonSelectSingleFile.setEnabled(False)
            self.pushButtonSelectMultiFile.setEnabled(False)
            self.testFileSet = True
            self.refFileSet = True

            if self.editingData['Reference File Type'] == 'cad':
                self.add_geomagic_units_combobox()
            else:
                self.remove_geomagic_units_combobox()

        return

    def reset_single_file_selection(self):
        self.testFileSet = False
        self.refFileSet = False
        self.reset_single_file_selection_ui()
        return

    def reset_multi_file_selection(self):
        self.testDirSet = False
        self.refDirSet = False
        self.reset_multi_file_selection_ui()
        return

    def edit_test_dir_selection(self, filePath):

        if filePath is None:
            return False

        if filePath is not None:
            self.get_test_mesh_files(filePath)
            if self.userTestFiles is None:
                VarianceUtilities.alert_user_dialog('No appropriate files found in directory: \n%s' % filePath,
                                                    parent=self,
                                                    iconPath=self.iconPath32, alertIcon=self.alertIcon54)
                self.settingsSelectFiles.usertestDir = self.lineEdittestDir.text()
                return False

        return True

    def edit_ref_dir_selection(self, filePath):

        if filePath is None:
            return False

        if filePath is not None:
            self.get_ref_mesh_files(filePath)
            if self.userRefFiles is None:
                VarianceUtilities.alert_user_dialog('No appropriate files found in directory: \n%s' % filePath,
                                                    parent=self,
                                                    iconPath=self.iconPath32, alertIcon=self.alertIcon54)
                self.settingsSelectFiles.userrefDir = self.lineEditRefDir.text()
                return False

        return True

    def get_test_mesh_files(self, filePath):
        self.userTestFiles = VarianceUtilities.get_files(filePath, dataTypeList=('.stl', '.ply', '.obj'))
        return

    def get_ref_mesh_files(self, filePath):
        self.userRefFiles = VarianceUtilities.get_files(filePath, dataTypeList=('.step', '.stp', '.igs.',  # CAD
                                                                                '.stl', '.ply', '.obj',  # Mesh
                                                                                '.txt', '.csv', '.var',
                                                                                '.asc'))  # Point Cloud
        return

    def sort_input_files(self):
        self.store_window_settings()
        if self.sortedFiles is False:
            self.sortFilesController.TestFiles = self.userTestFiles
            self.sortFilesController.RefFiles = self.userRefFiles

            self.TestFilesCount = len(self.sortFilesController.TestFiles)
            self.RefFilesCount = len(self.sortFilesController.RefFiles)

            if self.TestFilesCount != self.RefFilesCount:
                VarianceUtilities.alert_user_dialog(message=('Input directories do not have the same amount of files!\n'
                                                             'Please select reference file to use.'),
                                                    parent=self,
                                                    iconPath=self.iconPath32,
                                                    alertIcon=self.alertIcon54)
                self.select_ref_file_action()
                self.sortFilesController.RefFiles = numpy.full(self.TestFilesCount, self.settingsSelectFiles.userRefFile).tolist()

            self.mainLogger.info('Populating Sorting Table...')
            self.sortFilesController.fill_sortable_table()
        self.sortFilesController.activateWindow()
        self.sortFilesController.show()
        return

    def sorted_files_confirmed(self):
        self.sortedFiles = True
        self.store_sorted_tabled_information()
        self.pushButtonSaveDirs.setEnabled(True)
        self.pushButtonSaveDirs.setStyleSheet(KV_Stylist.TVPushButtonBlue)
        return

    def closeParametersOkay(self):

        self.close_valid = True
        self.close()

        self.addPairSignal.emit(self.addPairDict)

        return

    def closeEvent(self, event):

        self.parent.mainStatusBar.clearMessage()

        # Make sure the settings reflect the latest user inputs.
        self.store_window_settings()
        # Set mesh pairs to process
        self.set_mesh_pairs_info()

        if self.close_valid:
            event.accept()
            return
        if self.parent.editingBool:
            editingMeshCancel = VarianceUtilities.confirm_user_dialog(
                message='Cancel the mesh entry edit?',
                iconPath=self.iconPath48,
                notifyIcon=KV_ALERT)

            if editingMeshCancel:
                event.accept()
                # Analysis Item Reset
                analysisItem = self.addPairDict[self.editingRow]['Analysis Item']
                analysisItemTxt = ''
                analysisToolTip = self.addPairDict[self.editingRow]['Tool Tip Main']
                analysisItem.setText(analysisItemTxt)
                analysisItem.setToolTip(analysisToolTip)

                # Status ITem Reset
                statusItem = self.addPairDict[self.editingRow]['Status Items Text']
                statusItemTxt = self.addPairDict[self.editingRow]['Status']
                statusItem.setText(statusItemTxt)

                # Pixmap Reset
                rowNumber = analysisItem.row()
                analysisItemPixmap = self.addPairDict[self.editingRow]['Analysis Pixmap']
                analysisItemLabel = QLabel()
                analysisItemLabel.setAlignment(Qt.AlignCenter)
                analysisItemLabel.setPixmap(analysisItemPixmap)
                self.parent.tableWidgetMeshQueue.setCellWidget(rowNumber, 4, analysisItemLabel)
            else:
                event.ignore()
        return

    def check_input_files_differ(self):
        if self.testFileSet and self.refFileSet:
            fileMatch = (self.settingsSelectFiles.userTestFile == self.settingsSelectFiles.userRefFile)
            fileMatchMessage = 'Input files match, please select different files.'
            dirMatch = (os.path.dirname(self.settingsSelectFiles.userTestFile) ==
                        os.path.dirname(self.settingsSelectFiles.userRefFile))
            dirSet = None
            if dirMatch:
                dirSet = os.path.dirname(self.settingsSelectFiles.userTestFile)

            return fileMatch, fileMatchMessage, dirMatch, dirSet
        else:
            return False, None, False, None

    def check_input_dirs_differ(self):
        if self.testDirSet and self.refDirSet:
            dirMatch = (self.settingsSelectFiles.usertestDir == self.settingsSelectFiles.userrefDir)
            dirMatchMessage = 'Input directories match, please select different directories.'
            return dirMatch, dirMatchMessage
        else:
            return False, None

    def select_test_file_action(self):
        # Choose starting filePath to browse from.
        # filePath = self.lineEdittestFile.text()
        filePath = None
        if self.settingsSelectFiles.userTestFile is not None:
            filePath = self.settingsSelectFiles.userTestFile
        else:
            filePath = self.settingsSelectFiles.defaultInputDir

        # isBlank = (filePath == '') or (filePath is None) or not os.path.isfile(filePath)
        # if isBlank:
        #     filePath = self.settingsSelectFiles.defaultInputDir

        if self.refFileSet:
            filePath = self.settingsSelectFiles.userRefFile

        # Ask user to select filePath.
        filePath = VarianceUtilities.select_file_dialog(
            filePath, iconPath=self.iconPath32)

        if filePath is None:
            return

        self.testFileSet = True
        self.settingsSelectFiles.userTestFile = filePath
        fileMatch, fileMatchMessage, dirMatch, dirSet = self.check_input_files_differ()
        if fileMatch:
            VarianceUtilities.alert_user_dialog(fileMatchMessage,
                                                parent=self,
                                                iconPath=self.iconPath32, alertIcon=self.alertIcon54)
            self.reset_test_file_widget()
            return

        # Update filePath.
        self.update_test_file_widget(TestFilePath=filePath)
        self.update_test_file()

        if dirMatch and os.path.isdir(dirSet):
            # Update filePath.
            self.settingsSelectFiles.userOutputDirectory = dirSet
            self.lineEditOutputDir.setText(dirSet)
            self.update_output_dir()

        return

    def update_test_file(self):
        # Get the text from the line edit widget.
        strLineEditText = self.lineEdittestFile.text()
        # Remove white space and quotes.
        strLineEditText = strLineEditText.strip()
        strLineEditText = strLineEditText.strip('\"')
        # Update filePath if line edit input is not blank.
        isBlank = (strLineEditText == '') or (strLineEditText is None)
        if not isBlank:
            strLineEditText = os.path.abspath(strLineEditText)
            # Make sure the filePath exists.
            if not os.path.isfile(strLineEditText):
                # Update filePath line edit widget.
                message = 'Please enter existing Test file.'
                self.mainLogger.info(message)
                VarianceUtilities.alert_user_dialog(message, parent=self,
                                                    iconPath=self.iconPath32, alertIcon=self.alertIcon54)
            else:
                # Update filePath line edit widget.
                self.lineEdittestFile.setText(strLineEditText)
                message = 'Test Input file set as:\n%s\n' % (strLineEditText)
                self.mainLogger.info(message)
                self.testFileSet = True
                self.pushButtonSaveFiles.setEnabled(False)
                self.pushButtonSaveFiles.setStyleSheet(KV_Stylist.TVPushButtonBlueDisabled)
                self.check_files_set()
        return

    def select_test_dir_action(self):
        # Choose starting filePath to browse from.
        # filePath = self.lineEdittestDir.text()
        filePath = self.settingsSelectFiles.usertestDir
        isBlank = (filePath == '') or (filePath is None) or not os.path.isdir(filePath)
        if isBlank:
            filePath = self.settingsSelectFiles.defaultInputDir
        # Ask user to select filePath.
        filePath = VarianceUtilities.select_directory_dialog(
            filePath, iconPath=self.iconPath32)

        if filePath is None:
            return

        self.testDirSet = True
        self.settingsSelectFiles.usertestDir = filePath
        dirMatch, dirMatchMessage = self.check_input_dirs_differ()
        if dirMatch:
            VarianceUtilities.alert_user_dialog(dirMatchMessage, parent=self,
                                                iconPath=self.iconPath32, alertIcon=self.alertIcon54)
            self.reset_test_dir_widget()
            return

        # Update filePath.
        filesFound = self.edit_test_dir_selection(filePath)
        if filesFound:
            self.update_test_dir_widget(TestDirPath=filePath)
            self.update_test_dir()
        return

    def update_test_dir(self):
        # Get the text from the line edit widget.
        strLineEditText = self.lineEdittestDir.text()
        # Remove white space and quotes.
        strLineEditText = strLineEditText.strip()
        strLineEditText = strLineEditText.strip('\"')
        # Update filePath if line edit input is not blank.
        isBlank = (strLineEditText == '') or (strLineEditText is None)
        if not isBlank:
            strLineEditText = os.path.abspath(strLineEditText)
            # Make sure the filePath exists.
            if not os.path.isdir(strLineEditText):
                # Update filePath line edit widget.
                message = 'Please enter existing Test directory.'
                self.mainLogger.info(message)
                VarianceUtilities.alert_user_dialog(message, parent=self,
                                                    iconPath=self.iconPath32, alertIcon=self.alertIcon54)
            else:
                # Update filePath line edit widget.
                self.lineEdittestDir.setText(strLineEditText)
                message = 'Test Input Directory set as:\n%s\n' % (strLineEditText)
                self.mainLogger.info(message)
                self.testDirSet = True
                self.sortedFiles = False
                self.pushButtonSaveDirs.setEnabled(False)
                self.pushButtonSaveDirs.setStyleSheet(KV_Stylist.TVPushButtonBlueDisabled)
                self.check_directories_set()

        return

    def select_ref_file_action(self):
        # Choose starting filePath to browse from.
        # filePath = self.lineEditRefFile.text()
        filePath = None
        if self.settingsSelectFiles.userRefFile is not None:
            filePath = self.settingsSelectFiles.userRefFile
        else:
            filePath = self.settingsSelectFiles.defaultInputDir

        # isBlank = (filePath == '') or (filePath is None) or not os.path.isfile(filePath)
        # if isBlank:
        #     filePath = self.settingsSelectFiles.defaultInputDir

        if self.testFileSet:
            filePath = self.settingsSelectFiles.userTestFile

        # Ask user to select filePath.
        filePath = VarianceUtilities.select_file_dialog(
            filePath, iconPath=self.iconPath32, meshOnly=False)

        if filePath is None:
            return

        self.refFileSet = True
        self.settingsSelectFiles.userRefFile = filePath
        fileMatch, fileMatchMessage, dirMatch, dirSet = self.check_input_files_differ()
        if fileMatch:
            VarianceUtilities.alert_user_dialog(fileMatchMessage, parent=self,
                                                iconPath=self.iconPath32, alertIcon=self.alertIcon54)
            self.reset_ref_file_widget()
            return

        # Update filePath.
        self.update_ref_file_widget(RefFilePath=filePath)
        self.update_ref_file()

        if dirMatch and os.path.isdir(dirSet):
            # Update filePath.
            self.settingsSelectFiles.userOutputDirectory = dirSet
            self.lineEditOutputDir.setText(dirSet)
            self.update_output_dir()

        return

    def update_ref_file(self):
        # Get the text from the line edit widget.
        strLineEditText = self.lineEditRefFile.text()
        # Remove white space and quotes.
        strLineEditText = strLineEditText.strip()
        strLineEditText = strLineEditText.strip('\"')
        # Update filePath if line edit input is not blank.
        isBlank = (strLineEditText == '') or (strLineEditText is None)
        if not isBlank:
            strLineEditText = os.path.abspath(strLineEditText)
            # Make sure the filePath exists.
            if not os.path.isfile(strLineEditText):
                # Update filePath line edit widget.
                message = 'Please enter existing Ref file.'
                self.mainLogger.info(message)
                VarianceUtilities.alert_user_dialog(message, parent=self,
                                                    iconPath=self.iconPath32, alertIcon=self.alertIcon54)
            else:
                # Update filePath line edit widget
                self.lineEditRefFile.setText(strLineEditText)
                message = 'Ref Input file set as:\n%s\n' % (strLineEditText)
                self.mainLogger.info(message)
                self.refFileSet = True
                self.pushButtonSaveFiles.setEnabled(False)
                self.pushButtonSaveFiles.setStyleSheet(KV_Stylist.TVPushButtonBlueDisabled)
                self.check_files_set()
                self.set_ref_file_type()
                if self.refFileType == 'cad':
                    self.add_geomagic_units_combobox()
                else:
                    self.remove_geomagic_units_combobox()
        return

    def select_ref_dir_action(self):
        # Choose starting filePath to browse from.
        # filePath = self.lineEditRefDir.text()
        filePath = self.settingsSelectFiles.userrefDir
        isBlank = (filePath == '') or (filePath is None) or not os.path.isdir(filePath)
        if isBlank:
            filePath = self.settingsSelectFiles.defaultInputDir
        # Ask user to select filePath.
        filePath = VarianceUtilities.select_directory_dialog(
            filePath, iconPath=self.iconPath32)

        if filePath is None:
            return

        self.refDirSet = True
        self.settingsSelectFiles.userrefDir = filePath
        dirMatch, dirMatchMessage = self.check_input_dirs_differ()
        if dirMatch:
            VarianceUtilities.alert_user_dialog(dirMatchMessage, parent=self,
                                                iconPath=self.iconPath32, alertIcon=self.alertIcon54)
            self.reset_ref_dir_widget()
            return

        # Update filePath.
        filesFound = self.edit_ref_dir_selection(filePath)
        if filesFound:
            self.update_ref_dir_widget(RefDirPath=filePath)
            self.update_ref_dir()
        return

    def update_ref_dir(self):
        # Get the text from the line edit widget.
        strLineEditText = self.lineEditRefDir.text()
        # Remove white space and quotes.
        strLineEditText = strLineEditText.strip()
        strLineEditText = strLineEditText.strip('\"')
        # Update filePath if line edit input is not blank.
        isBlank = (strLineEditText == '') or (strLineEditText is None)
        if not isBlank:
            strLineEditText = os.path.abspath(strLineEditText)
            # Make sure the filePath exists.
            if not os.path.isdir(strLineEditText):
                # Update filePath line edit widget.
                message = 'Please enter existing Ref directory.'
                self.mainLogger.info(message)
                VarianceUtilities.alert_user_dialog(message, parent=self,
                                                    iconPath=self.iconPath32, alertIcon=self.alertIcon54)
            else:
                # Update filePath line edit widget.
                self.lineEditRefDir.setText(strLineEditText)
                message = 'Ref Input Dir set as:\n%s\n' % (strLineEditText)
                self.mainLogger.info(message)
                self.refDirSet = True
                self.sortedFiles = False
                self.pushButtonSaveDirs.setEnabled(False)
                self.pushButtonSaveDirs.setStyleSheet(KV_Stylist.TVPushButtonBlueDisabled)
                self.check_directories_set()
        return

    def select_output_dir_action(self):
        # Choose starting filePath to browse from.
        # filePath = self.pushButtonClose.text()
        filePath = self.settingsSelectFiles.userOutputDirectory
        isBlank = (filePath == '') or (filePath is None) or not os.path.isdir(filePath)
        if isBlank:
            filePath = self.settingsSelectFiles.defaultOutputDirectory
        # Ask user to select filePath.
        filePath = VarianceUtilities.select_directory_dialog(
            filePath, iconPath=self.iconPath32)

        if filePath is None:
            return

        # Update filePath.
        self.settingsSelectFiles.userOutputDirectory = filePath
        self.lineEditOutputDir.setText(filePath)
        self.update_output_dir()
        return

    def update_output_dir(self):
        # Get the text from the line edit widget.
        strLineEditText = self.lineEditOutputDir.text()
        # Remove white space and quotes.
        strLineEditText = strLineEditText.strip()
        strLineEditText = strLineEditText.strip('\"')
        # Update filePath if line edit input is not blank.
        isBlank = (strLineEditText == '') or (strLineEditText is None)
        if not isBlank:
            strLineEditText = os.path.abspath(strLineEditText)
            # Make sure the filePath exists.
            if not os.path.isdir(strLineEditText):
                # Update filePath line edit widget.
                message = 'Please enter existing Output directory.'
                self.mainLogger.info(message)
                VarianceUtilities.alert_user_dialog(message, parent=self,
                                                    iconPath=self.iconPath32, alertIcon=self.alertIcon54)
            else:
                # Update filePath line edit widget.
                self.lineEditOutputDir.setText(strLineEditText)
                message = 'Input Dir set as:\n%s\n' % (strLineEditText)
                self.mainLogger.info(message)
                self.outputDirSet = True
                self.check_directories_set()
                self.check_files_set()
        return

    def set_point_density(self):
        self.settingsSelectFiles.userPointDensity = self.spinBoxPointDensity.value()
        if self.settingsSelectFiles.userPointDensity <= 1:
            self.spinBoxPointDensity.setValue(1)
            self.settingsSelectFiles.userPointDensity = self.spinBoxPointDensity.value()
        if self.settingsSelectFiles.userPointDensity > 1000000:
            self.spinBoxPointDensity.setValue(1000000)
            self.settingsSelectFiles.userPointDensity = self.spinBoxPointDensity.value()
        return

    def check_directories_set(self):
        if self.outputDirSet and self.testDirSet and self.refDirSet:
            self.pushButtonSortFiles.setEnabled(True)
            self.pushButtonSortFiles.setStyleSheet(KV_Stylist.TVPushButtonBlue)
        return

    def check_files_set(self):
        if self.outputDirSet and self.testFileSet and self.refFileSet:
            self.pushButtonSaveFiles.setEnabled(True)
            self.pushButtonSaveFiles.setStyleSheet(KV_Stylist.TVPushButtonBlue)
        return

    def create_table_items(self):
        # Assign Table Items
        testItem = QTableWidgetItem()
        refItem = QTableWidgetItem()
        outputDir = QTableWidgetItem()
        outputType = QTableWidgetItem()
        analysisItem = QTableWidgetItem()
        pointDenItem = QTableWidgetItem()
        statusItemsText = QTableWidgetItem()

        outputDir.setTextAlignment(Qt.AlignCenter)
        outputType.setTextAlignment(Qt.AlignCenter)
        analysisItem.setTextAlignment(Qt.AlignCenter)
        pointDenItem.setTextAlignment(Qt.AlignCenter)
        statusItemsText.setTextAlignment(Qt.AlignCenter)

        statusItemsBar = VarianceCalculator_Widgets.TVProgressBar(name='statusItemsBar')
        actionItemsWidget = VarianceCalculator_Widgets.TVWidget(name='actionItemsWidget', actionItems=True)

        actionItemsEdit = actionItemsWidget.actionEditButton
        actionItemsOpen = actionItemsWidget.actionOpenButton
        actionItemsDelete = actionItemsWidget.actionDeleteButton
        actionItemsDisplay = actionItemsWidget.actionDisplayButton
        return (testItem, refItem, outputDir, outputType, analysisItem, pointDenItem,
                statusItemsText, statusItemsBar, actionItemsEdit, actionItemsOpen, actionItemsDelete, actionItemsDisplay,
                actionItemsWidget)

    def store_sorted_tabled_information(self):
        self.addPairConfirmed = dict()
        self.refFileExtList = list()
        """ Retrieve the settings from the widgets for the window."""
        for TestFileRow in range(self.sortFilesController.TestModel.rowCount()):
            self.addPairConfirmed[TestFileRow] = dict()
            TestFileRowText = self.sortFilesController.TestModel.item(TestFileRow, 0).text()
            TestFile = self.sortFilesController.TestFileDict[TestFileRowText]
            self.addPairConfirmed[TestFileRow].update({'Test Mesh File': TestFile})

        for RefFileRow in range(self.sortFilesController.RefModel.rowCount()):
            RefFileRowText = self.sortFilesController.RefModel.item(RefFileRow, 0).text()
            RefFile = self.sortFilesController.RefFileDict[RefFileRowText]
            self.set_ref_dir_type(inputFile=RefFile)
            self.addPairConfirmed[RefFileRow].update({'Ref Mesh File': RefFile})
            self.addPairConfirmed[RefFileRow].update({'Reference File Type': self.refFileType})
            self.addPairConfirmed[RefFileRow].update({'Reference File Ext': self.refFileExt})
            self.refFileExtList.append(self.refFileType)

        if 'cad' in self.refFileExtList:
            self.add_geomagic_units_combobox()
        else:
            self.remove_geomagic_units_combobox()

        return

    def set_mesh_pairs_info(self):
        if self.addPairType == 'file':
            self.addPairDict = dict()
            if self.editingData:
                self.addPairDict[self.editingRow] = self.editingData
                self.addPairDict[self.editingRow]['Test Mesh File'] = self.settingsSelectFiles.userTestFile
                self.addPairDict[self.editingRow]['Test Mesh Name'] = os.path.basename(
                    self.settingsSelectFiles.userTestFile)
                self.addPairDict[self.editingRow][
                    'Ref Mesh File'] = self.settingsSelectFiles.userRefFile
                self.addPairDict[self.editingRow]['Ref Mesh Name'] = os.path.basename(
                    self.settingsSelectFiles.userRefFile)
                self.addPairDict[self.editingRow][
                    'Output Dir'] = self.settingsSelectFiles.userOutputDirectory
                self.addPairDict[self.editingRow]['Output Dir Name'] = os.path.basename(
                    self.settingsSelectFiles.userOutputDirectory)
                self.addPairDict[self.editingRow]['Output File Type'] = self.userOutputType
                self.addPairDict[self.editingRow]['Output File Type Idx'] = self.settingsSelectFiles.userOutputTypeIdx
                self.addPairDict[self.editingRow]['Point Density'] = self.settingsSelectFiles.userPointDensity
                self.addPairDict[self.editingRow]['Editing'] = True
                self.addPairDict[self.editingRow]['Entry Type'] = self.addPairType
                self.addPairDict[self.editingRow]['Reference File Type'] = self.refFileType
                self.addPairDict[self.editingRow]['Reference File Ext'] = self.refFileExt
                self.addPairDict[self.editingRow]['Geo Units'] = self.userGeoUnits
                self.addPairDict[self.editingRow]['Geo Units Idx'] = self.settingsSelectFiles.userGeoUnitsIdx

            else:
                testItem, refItem, outputDirItem, outputTypeItem, \
                analysisItem, pointDenItem, statusItemsText, statusItemsBar, \
                actionItemsEdit, actionItemsOpen, actionItemsDelete, actionItemsDisplay, \
                actionItemsWidget = self.create_table_items()

                self.addPairDict[self.emptyRow] = {
                    'Test Mesh File': self.settingsSelectFiles.userTestFile,
                    'Test Mesh Name': os.path.basename(self.settingsSelectFiles.userTestFile),
                    'Ref Mesh File': self.settingsSelectFiles.userRefFile,
                    'Ref Mesh Name': os.path.basename(self.settingsSelectFiles.userRefFile),
                    'Output Dir': self.settingsSelectFiles.userOutputDirectory,
                    'Output Dir Name': os.path.basename(self.settingsSelectFiles.userOutputDirectory),
                    'Output File Type': self.userOutputType,
                    'Output File Type Idx': self.settingsSelectFiles.userOutputTypeIdx,
                    'Point Density': self.settingsSelectFiles.userPointDensity,
                    'Row Number': self.emptyRow,
                    'test File Item': testItem,
                    'ref File Item': refItem,
                    'Output Dir Item': outputDirItem,
                    'Output Type Item': outputTypeItem,
                    'Analysis Item': analysisItem,
                    'Density Item': pointDenItem,
                    'Status Items Text': statusItemsText,
                    'Status Items Bar': statusItemsBar,
                    'Action Items Edit': actionItemsEdit,
                    'Action Items Open': actionItemsOpen,
                    'Action Items Delete': actionItemsDelete,
                    'Action Items Display': actionItemsDisplay,
                    'Action Items Widget': actionItemsWidget,
                    'Editing': False,
                    'Entry Type': self.addPairType,
                    'Reference File Type': self.refFileType,
                    'Reference File Ext': self.refFileExt,
                    'Geo Units': self.userGeoUnits,
                    'Geo Units Idx': self.settingsSelectFiles.userGeoUnitsIdx
                }

        if self.addPairType == 'dir':
            self.addPairDict = dict()
            for itemIdx, meshPairDict in self.addPairConfirmed.items():
                subPairAddNum = self.emptyRow + itemIdx

                testItem, refItem, outputDirItem, outputTypeItem, \
                analysisItem, pointDenItem, statusItemsText, statusItemsBar, \
                actionItemsEdit, actionItemsOpen, actionItemsDelete, actionItemsDisplay, \
                actionItemsWidget = self.create_table_items()

                meshPairDict['Test Mesh Name'] = os.path.basename(meshPairDict['Test Mesh File'])
                meshPairDict['Ref Mesh Name'] = os.path.basename(meshPairDict['Ref Mesh File'])
                meshPairDict['Output Dir'] = self.settingsSelectFiles.userOutputDirectory
                meshPairDict['Output Dir Name'] = os.path.basename(self.settingsSelectFiles.userOutputDirectory)
                meshPairDict['Output File Type'] = self.userOutputType
                meshPairDict['Output File Type Idx'] = self.settingsSelectFiles.userOutputTypeIdx
                meshPairDict['Point Density'] = self.settingsSelectFiles.userPointDensity
                meshPairDict['Row Number'] = subPairAddNum

                meshPairDict['test File Item'] = testItem
                meshPairDict['ref File Item'] = refItem
                meshPairDict['Output Dir Item'] = outputDirItem
                meshPairDict['Output Type Item'] = outputTypeItem
                meshPairDict['Analysis Item'] = analysisItem
                meshPairDict['Density Item'] = pointDenItem
                meshPairDict['Status Items Text'] = statusItemsText
                meshPairDict['Status Items Bar'] = statusItemsBar
                meshPairDict['Action Items Edit'] = actionItemsEdit
                meshPairDict['Action Items Open'] = actionItemsOpen
                meshPairDict['Action Items Delete'] = actionItemsDelete
                meshPairDict['Action Items Display'] = actionItemsDisplay
                meshPairDict['Action Items Widget'] = actionItemsWidget
                meshPairDict['Editing'] = False
                meshPairDict['Entry Type'] = self.addPairType
                meshPairDict['Geo Units'] = self.userGeoUnits
                meshPairDict['Geo Units Idx'] = self.settingsSelectFiles.userGeoUnitsIdx
                # meshPairDict['Reference File Type'] = self.refFileType, gets added when confirming matches
                # meshPairDict['Reference File Ext'] = self.refFileExt, gets added when confirming matches

                self.addPairDict[subPairAddNum] = meshPairDict
        return
